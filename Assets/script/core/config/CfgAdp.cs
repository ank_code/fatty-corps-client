using System;
using System.Collections.Generic;
using kf;

namespace script.core.config
{
    public enum ECfgType
    {
        Json,
        Csv,
    }
    
    public class CfgAdp : Singleton<CfgAdp>
    {
        public List<T> read<T>(string path, ECfgType cfgType = ECfgType.Csv) where T : new()
        {
            switch (cfgType)
            {
                case ECfgType.Json:
                    return new CfgJsonReader().read<T>(path);
                case ECfgType.Csv:
                    return new CfgCsvReader().read<T>(path);
            }

            throw new Exception("暂不支持的文件格式");
        }
        
        public void readAsync<T>(string path, ECfgType cfgType, Action<List<T>> callback) where T : new()
        {
            switch (cfgType)
            {
                case ECfgType.Json:
                    new CfgJsonReader().readAsync<T>(path, callback);
                    break;
                case ECfgType.Csv:
                    new CfgCsvReader().readAsync<T>(path, callback);
                    break;
                default:
                    throw new Exception("暂不支持的文件格式");
            }
        }
    }
}