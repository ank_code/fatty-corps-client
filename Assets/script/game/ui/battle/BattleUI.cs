using System;
using System.Collections;
using System.Collections.Generic;
using ak;
using kf;
using kf.anim;
using script.core.config;
using script.game.enums;
using script.game.mgr;
using script.game.net.http;
using script.game.net.msg.server;
using script.game.net.msg.server.item;
using script.game.ui.battle.effect;
using script.game.ui.battle.record;
using script.game.ui.battle.effect.anim;
using script.game.ui.home;
using script.game.ui.loading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.battle
{
    public class BattleUI : GameUI
    {
        private GameObject m_back;
        private GameObject m_mid;
        private GameObject m_front;

        private TMP_Text m_txtCurRound;
        private int m_curRound = 0;

        private GameObject m_objTeamLeft;
        private GameObject m_objTeamRight;
        
        private GameObject m_objCasterContainer;
        private GameObject m_objTargetContainer;
        private GameObject m_objNumberContainer;
        private GameObject m_objEffectContainer;

        private GameObject m_objSpeed;
        private TMP_Text m_txtSpeed;

        private GameObject m_objBtnStop;
        private GameObject m_objBtnResume;
        
        private GameObject m_objBtnSkip;

        private float m_initSchedule = 0.0f;

        private Coroutine m_showDamageHandler = null;
        private Coroutine m_sleepHandler = null;

        public enum EPLAY_SPEED
        {
            SPEED_ONE,
            SPEED_TWO,
            SPEED_FOUR,
        }
        
        private EPLAY_SPEED m_speed = EPLAY_SPEED.SPEED_ONE;
        private float m_speedVal = 1.0f;

        private List<GameObject> m_objLeftPoses = new List<GameObject>();
        private List<GameObject> m_objRightPoses = new List<GameObject>();
        
        private SBattleRes m_rep;
        private List<DItem> m_awards;

        private List<BattleHero> m_left = new List<BattleHero>();
        private List<BattleHero> m_right = new List<BattleHero>();
        
        private List<BattleHero> m_battleHeroes = new List<BattleHero>();

        private EBattleType m_type = EBattleType.MainGuanqia;

        public const float NUMBER_DIS_Y = 50.0f;
        private const float DOT_DAMAGE_DELAY_TIME = 0.4f;
        
        // 战斗类型
        public enum EBattleType
        {
            MainGuanqia,
        }

        public class UIParam : IUIParam
        {
            public SBattleRes res;
            public EBattleType type;
            public List<DItem> awards;
        }

        public override void onCreate(IUIParam param)
        {
            UIParam p = (UIParam)param;
            m_rep = p.res;
            m_type = p.type;
            m_awards = p.awards;

            m_initSchedule += 0.1f;
            sendSchedule(m_initSchedule);
            
            m_back = KtUnity.findObj(gameObject, "back");
            m_mid = KtUnity.findObj(gameObject, "mid");
            m_front = KtUnity.findObj(gameObject, "front");

            GameObject objRound = KtUnity.findObj(gameObject, "txtCurRound");
            m_txtCurRound = objRound.GetComponent<TMP_Text>();
            
            m_objTeamLeft = KtUnity.findObj(gameObject, "left");
            m_objTeamRight = KtUnity.findObj(gameObject, "right");

            m_objCasterContainer = KtUnity.findObj(gameObject, "casterContainer");
            m_objTargetContainer = KtUnity.findObj(gameObject, "targetContainer");
            
            m_objNumberContainer = KtUnity.findObj(gameObject, "numberContainer");
            m_objEffectContainer = KtUnity.findObj(gameObject, "effectContainer");

            m_objSpeed = KtUnity.findObj(gameObject, "btnSpeed");
            KtUI.addListener(m_objSpeed, () =>
            {
                m_speed++;
                if ((int)m_speed >= Enum.GetValues(typeof(EPLAY_SPEED)).Length)
                {
                    m_speed = EPLAY_SPEED.SPEED_ONE;
                }
                
                // NumMgr.getInstance().showNum(NumMgr.ENumType.NorDamage, 123321, BattlePos.getInstance().getEnemyBack(false));
                if (m_speed == EPLAY_SPEED.SPEED_ONE)
                {
                    m_speedVal = 1.0f;
                    
                } else if (m_speed == EPLAY_SPEED.SPEED_TWO)
                {
                    m_speedVal = 2.0f;
                }
                else
                {
                    m_speedVal = 4.0f;
                }
                
                Time.timeScale = m_speedVal; 
                updateSpeedTxt();
            });
            
            GameObject objTxtSpeed = KtUnity.findObj(gameObject, "txtSpeed");
            m_txtSpeed = objTxtSpeed.GetComponent<TMP_Text>();
            
            m_objBtnStop = KtUnity.findObj(gameObject, "btnStop");
            KtUI.addListener(m_objBtnStop, () =>
            {
                m_objBtnStop.SetActive(false);
                m_objBtnResume.SetActive(true);
                Time.timeScale = 0.0f;
            });
            
            m_objBtnResume = KtUnity.findObj(gameObject, "btnResume");
            KtUI.addListener(m_objBtnResume, () =>
            {
                m_objBtnStop.SetActive(true);
                m_objBtnResume.SetActive(false);
                Time.timeScale = m_speedVal;
            });
            m_objBtnResume.SetActive(false);
            
            GameObject objTxtName = KtUnity.findObj(gameObject, "txtName");
            var txtName = objTxtName.GetComponent<TMP_Text>();
            txtName.text = m_rep.name;
            
            m_objBtnSkip = KtUnity.findObj(gameObject, "btnSkip");
            KtUI.addListener(m_objBtnSkip, () =>
            {
                // close();
                // UIMgr.getInstance().show<HomeUI>();
                onBattleEnd();
            });

            for (int i = 0; i < GameConst.MAX_TEAM_NUM; i++)
            {
                int no = i + 1;
                
                GameObject objPosLeft = KtUnity.findObj(m_objTeamLeft, "pos" + no);
                m_objLeftPoses.Add(objPosLeft);
                
                GameObject objPosRight = KtUnity.findObj(m_objTeamRight, "pos" + no);
                m_objRightPoses.Add(objPosRight);
            }

            initBattlePos();
            // sendMsg();
            NumMgr.getInstance().setParent(m_objNumberContainer);
            EffectAnimFactory.getInstance().setParent(m_objEffectContainer);
            
            m_initSchedule += 0.1f;
            sendSchedule(m_initSchedule);

            StartCoroutine(initHeroes());

#if UNITY_EDITOR
            Time.timeScale = 1;
#endif
        }

        private void sendSchedule(float schedule)
        {
            GameEvent.sendEvent(EEvent.OnLoadingUISchedule, new object[]{schedule});
        }

        private void initBattlePos()
        {
            if (BattlePos.getInstance().init)
            {
                return;
            }

            GameObject posLeft = KtUnity.findObj(gameObject, "posLeft");
            GameObject posRight = KtUnity.findObj(gameObject, "posRight");
            
            GameObject obj = KtUnity.findObj(posLeft, "frontCenter");
            BattlePos.getInstance().setFrontPosLeft(obj.transform.position);
            
            obj = KtUnity.findObj(posLeft, "backCenter");
            BattlePos.getInstance().setBackPosLeft(obj.transform.position);
            
            obj = KtUnity.findObj(posLeft, "center");
            BattlePos.getInstance().setCenterPosLeft(obj.transform.position);
            
            obj = KtUnity.findObj(posRight, "frontCenter");
            BattlePos.getInstance().setFrontPosRight(obj.transform.position);
            
            obj = KtUnity.findObj(posRight, "backCenter");
            BattlePos.getInstance().setBackPosRight(obj.transform.position);          
            
            obj = KtUnity.findObj(posRight, "center");
            BattlePos.getInstance().setCenterPosRight(obj.transform.position);
        }

        public override void onHide()
        {
            Time.timeScale = 1.0f;
        }

        public void OnDestroy()
        {
            NumMgr.getInstance().clear();
            KtUnity.stopCoroutine(this, m_showDamageHandler);
            KtUnity.stopCoroutine(this, m_sleepHandler);
        }

        public void sendMsg()
        {
            GameRequest.getInstance().testBattle((res) =>
            {
                m_rep = res;
                initHeroes();
                RecordInvoker.getInstance().next();
            });
        }

        // 初始化英雄
        public IEnumerator initHeroes()
        {
            yield return null;
            
            // 根据站位初始化所有英雄
            for (var i = 0; i < m_rep.heroes.Count; i++)
            {
                var hero = m_rep.heroes[i];
                int posIndex = hero.pos.standPosNo - 1;

                GameObject objPos = null;
                
                if (hero.pos.leftTeam)
                {
                    objPos = m_objLeftPoses[posIndex];
                }
                else
                {
                    objPos = m_objRightPoses[posIndex];
                }
                
                BattleHero battleHero = new BattleHero();
                battleHero.init(hero.typeId, objPos, hero);

                if (hero.pos.leftTeam)
                {
                    m_left.Add(battleHero);
                }
                else
                {
                    m_right.Add(battleHero);
                }
                
                m_battleHeroes.Add(battleHero);
                
                m_initSchedule += 0.1f;
                sendSchedule(m_initSchedule);
                
                yield return null;
            }
            
            RecordInvoker.getInstance().init(m_rep.records);
            BattleHeroMgr.getInstance().init(m_battleHeroes);
            
            sendSchedule(1.0f);
            UIMgr.getInstance().destroy<SceneLoadingUI>();
            RecordInvoker.getInstance().next();
        }
        
        public override void onEvent(int eId, object[] objs)
        {
            EEvent e = (EEvent)eId;
            switch (e)
            {
                case EEvent.OnBattleBegin:
                    setRound(1);
                    Debug.Log("开始战斗，总出手次数：" + m_rep.records.Count);
                    break;
                case EEvent.OnBattleRoundEnd:
                    setRound(m_curRound + 1);
                    break;
                case EEvent.OnBattleEnd:
                    Debug.Log("战斗结束");
                    onBattleEnd();
                    break;
                case EEvent.OnBattleUICasterToTop:
                {
                    RecElem recElem = (RecElem)objs[0];
                    BattleHero caster = BattleHeroMgr.getInstance().getHeroByPos(recElem.caster);
                    caster.setParent(m_objCasterContainer.transform);
                    
                    // todo: 放在目标动作行为事件中处理，被攻击人高亮等效果
                    // for (var i = 0; i < recElem.effects.Count; i++)
                    // {
                    //     var targetHero = BattleHeroMgr.getInstance().getHeroByPos(recElem.effects[i].targetPos);
                    //     targetHero.setParent(m_objTargetContainer.transform);
                    // }
                    break;
                }
                case EEvent.OnBattleEffectDamage:
                {
                    BattleHero hero = (BattleHero)objs[0];
                    long value = (long)objs[1];
                    string extraStr = (string)objs[2];
                    NumMgr.ENumType type = ((NumMgr.ENumType[])Enum.GetValues(typeof(NumMgr.ENumType)))[int.Parse(extraStr)];
                    m_showDamageHandler = StartCoroutine(showDamageNum(hero, value, type));
                    break;                    
                }
                case EEvent.OnBattleEffectAddEnergy:
                {
                    BattleHero hero = (BattleHero)objs[0];
                    long value = (long)objs[1];
                    bool show = Boolean.Parse((string)objs[2]);
                    hero.onAddEnergy((int)value, show);
                    break;
                }
                case EEvent.OnBattleEffectAddBuff:
                {
                    BattleHero hero = (BattleHero)objs[0];
                    long value = (long)objs[1];
                    hero.getHeroInfoUI().addBuff((int)value);
                    break;
                }
                case EEvent.OnBattleEffectRemoveBuff:
                {
                    BattleHero hero = (BattleHero)objs[0];
                    long value = (long)objs[1];
                    hero.getHeroInfoUI().removeBuff((int)value);
                    break;
                }
                case EEvent.OnBattleSleep:
                {
                    float time = (float)objs[0];
                    m_sleepHandler = StartCoroutine(sleep(time));
                    break;
                }
            }
        }

        IEnumerator sleep(float time)
        {
            yield return new WaitForSeconds(time);
            m_sleepHandler = null;
        }

        IEnumerator showDamageNum(BattleHero hero, long value, NumMgr.ENumType type)
        {
            if (type == NumMgr.ENumType.Dot)
            {
                yield return new WaitForSeconds(DOT_DAMAGE_DELAY_TIME);
            }
            
            hero.onDamage(value, type);
            m_showDamageHandler = null;
        }

        private void setRound(int round)
        {
            m_txtCurRound.text = round.ToString();
            m_curRound = round;
        }

        private void updateSpeedTxt()
        {
            switch (m_speed)
            {
                case EPLAY_SPEED.SPEED_ONE:
                    m_txtSpeed.text = "X1";
                    break;
                case EPLAY_SPEED.SPEED_TWO:
                    m_txtSpeed.text = "X2";
                    break;
                case EPLAY_SPEED.SPEED_FOUR:
                    m_txtSpeed.text = "X4";
                    break;
            }
        }

        private void onBattleEnd()
        {
            this.close();
            
            if (m_rep.br.Equals(EBattleResult.Win))
            {
                // 战斗胜利
                if (m_type.Equals(EBattleType.MainGuanqia))
                {
                    BattleResultUI.Param param = new BattleResultUI.Param();
                    param.awards = m_awards;
                    UIMgr.getInstance().show<BattleResultUI>(param);
                }
            }
            else
            {
                // todo: 失败结算界面
            }
        }
    }
}