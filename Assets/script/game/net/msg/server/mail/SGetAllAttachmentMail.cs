using System.Collections.Generic;
using script.game.net.msg.server.item;

namespace script.game.net.msg.client.hero
{
    public class SGetAllAttachmentMail
    {
        public List<DItem> attachment;
    }
}