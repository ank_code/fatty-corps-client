namespace script.game.enums
{
    // 事件
    public enum EEvent
    {
        // 注册账号成功
        OnRegAccountSuccess,
        // 登陆成功
        OnLoginSuccess,
        // 保存刷新游戏token
        OnRefreshGameToken,
        
        // loading进度变化
        OnLoadingUISchedule,
        
        //////////////////// player
        OnPlayerLvChanged,
        OnPlayerExpChanged,
        OnPlayerNameChanged,
        OnPlayerCoinChanged,
        OnPlayerMoneyChanged,
        OnPlayerCombatChanged,
        
        //////////////////// bag
        OnBagItemClick,
        OnBagSwitchType,
        OnSellItemOk,
        
        //////////////////// chat
        OnChatSwitchChannel,
        
        //////////////////// heroList
        OnHeroItemClick,
        OnHeroSwitchType,
        
        //////////////////// battle
        OnBattleBegin,
        OnBattleEnd,
        OnBattleRoundEnd,
        
        // 停留一段时间
        OnBattleSleep,
        
        //////////////////// lineup
        OnLineupHeroItemClick,
        OnLineupHeroDragEnd,
        // 设置阵容成功
        OnSetLineupSuccess,
        
        //////////////////// mail
        OnMailRead,
        OnMailGetAttachment,
        OnMailDel,
        
        // 英雄出手，设置出手英雄与目标英雄层级，高亮展示
        OnBattleUICasterToTop,
        // 触发效果-伤害
        OnBattleEffectDamage,
        // 触发效果-死亡
        OnBattleEffectDead,
        // 触发效果-复活
        OnBattleEffectRevive,
        // 触发效果-加buff
        OnBattleEffectAddBuff,
        // 触发效果-减buff
        OnBattleEffectRemoveBuff,
        // 触发效果-加能量
        OnBattleEffectAddEnergy,
    }
}