using System.Collections.Generic;
using kf;
using script.game.enums;
using script.game.net.http;
using script.game.net.msg.server;
using script.game.net.msg.server.item;
using UnityEngine;

namespace script.game.mgr
{
    public class ServerCfg : Singleton<ServerCfg>
    {
        public SCfg cfg = new SCfg();
        private bool m_recv = false;
        public void init()
        {
            addSendTime();
        }

        private void getRemoteCfg()
        {
            Debug.Log("aktest getRemoteCfg");
            GameRequest.getInstance().cfg((rep) =>
            {
                Debug.Log("aktest getRemoteCfg ok");
                m_recv = true;
                this.cfg = rep;
            });
        }

        private void addSendTime()
        {
            TimerMgr.getInstance().AddTimerTask(() =>
            {
                if (!m_recv)
                {
                    getRemoteCfg();
                    addSendTime();
                }
            }, 1, ETimeUnit.Second, 1);
        }
    }
}