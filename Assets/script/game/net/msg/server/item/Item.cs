namespace script.game.net.msg.server.item
{
    public class Item
    {
        public int id;
        public int typeId;
        public int num;
        public string name;
    }
}