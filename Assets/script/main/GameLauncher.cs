using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using ak;
using kf;
using kf.hotpUpdate.state;
using UnityEngine;
using YooAsset;

public class GameLauncher : MonoBehaviour
{
    public GameObject m_objReporter;
    private bool m_yooAssetsInit = false;
    private LogoUI m_logoUI = null;
    private bool m_logoComplete = false;
    
    private bool testLogo = true;
    
    void Start()
    {
        Debug.Log("aktest GameLauncher Start");
        StructBuilder.getInstance().init(this.gameObject);
        ResMgr.getInstance().setEditor(GlobalCfg.ASSETS_PLAY_MODE == EPlayMode.EditorSimulateMode);
        UIMgr.getInstance().init(StructBuilder.getInstance().getCanvasObj());
        LaunchUIRegister.getInstance().init();
        HotUpdate.getInstance().init(GlobalCfg.getUpdateUrl(), GlobalCfg.ASSETS_PLAY_MODE,
            GlobalConst.HOTUPDATE_ASSEMBLY_NAME, GlobalConst.HOTUPDATE_DLL_FOLDER,
            GlobalConst.YOOASSET_DEFAULT_PACKAGE_NAME);
        HotUpdate.getInstance().setOnStateChange(onHotUpdateStateChanage);
        
        Application.lowMemory += OnLowMemory;

#if UNITY_EDITOR
        // 编辑器下跳过入场动画
        if (testLogo)
        {
            showLogoUI();
        }
#else
        showLogoUI();
#endif
        
    }

    private void onHotUpdateStateChanage(IFsmState newState)
    {
        // ui依赖yooasset加载，所以必须等yooasset初始化完成后才能展示
        if (newState is InitOver)
        {
#if UNITY_EDITOR
            showUpdateUI();
#else
            Debug.Log("aktest yooasset初始化完成");
            m_yooAssetsInit = true;
            if (m_logoComplete)
            {
                showUpdateUI();
            }
#endif
        }
    }

    private void showLogoUI()
    {
        m_logoUI = new LogoUI();
        m_logoUI.init(StructBuilder.getInstance().getCanvasObj(), () =>
        {
            Debug.Log("aktest Logo播放完成");
            
            m_logoComplete = true;
            if (m_yooAssetsInit)
            {
                showUpdateUI();
            }
        });
    }
    
    private void showUpdateUI()
    {
        if (m_logoUI != null)
        {
            m_logoUI.close();
        }
        
        // 播放完成后进入防沉迷说明，再进入热更新，然后进入游戏程序集中的登陆页（首个页面）
        UIMgr.getInstance().show(typeof(HotUpdateUI));
    }

    private void createReporter()
    {
        if (GlobalCfg.MODE == GlobalCfg.EHostMode.Prod)
        {
            if (m_objReporter != null)
            {
                m_objReporter.SetActive(false);
            }
        }
    }
    
    private void OnDestroy()
    {
        Application.lowMemory -= OnLowMemory;
    }
    
    private void OnLowMemory()
    {
        Debug.Log("收到了低内存警告！");
        Resources.UnloadUnusedAssets();
    }
}
