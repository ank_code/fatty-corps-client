using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace kf
{
    public class Copy
    {
        public static T DeepCopy<T>(T obj)
        {
            if (obj is string || obj.GetType().IsValueType)
                return obj;

            object retVal = Activator.CreateInstance(obj.GetType());
            FieldInfo[] fields = obj.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic |
                                                         BindingFlags.Static | BindingFlags.Instance);
            foreach (var field in fields)
            {
                try
                {
                    field.SetValue(retVal, DeepCopy(field.GetValue(obj)));
                }
                catch (Exception e)
                {
                    Debug.LogError("aktest DeepCopyByReflection e=" + e);
                }
            }

            return (T)retVal;
        }
    }
}