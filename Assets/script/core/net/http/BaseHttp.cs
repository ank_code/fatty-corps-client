using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

namespace kf
{
    public abstract class BaseHttp : MonoBehaviour, IBaseHttp
    {
        private Dictionary<string, string> m_headers = new Dictionary<string, string>();
        protected Action<IBaseHttp.Err> m_failCallback = null;

        // 默认5秒超时
        private const int TIME_OUT = 5;

        public virtual void init()
        {
            
        }

        public void addHeader(string k, string v)
        { 
            m_headers[k] = v;
        }
        
        public void removeHeader(string k)
        {
            m_headers.Remove(k);
        }

        public void setFailCallback(Action<IBaseHttp.Err> callback)
        {
            this.m_failCallback = callback;
        }

        public string get(string path)
        {
            throw new NotImplementedException();
        }

        public string post(string path, string param)
        {
            throw new NotImplementedException();
        }

        public void getAsync(string path, Action<string> success, Action<IBaseHttp.Err> fail)
        {
            StartCoroutine(getAsyncRequest(path, success, fail));
        }

        private IEnumerator getAsyncRequest(string path, Action<string> success, Action<IBaseHttp.Err> fail)
        {
            throw new NotImplementedException();
        }

        public virtual void postAsync(string path, string param, Action<string> success, Action<IBaseHttp.Err> fail = null)
        {
            StartCoroutine(postAsyncRequest(path, param, success, fail));
        }

        private IEnumerator postAsyncRequest(string path, string param, Action<string> success, Action<IBaseHttp.Err> fail = null)
        {
            string url = getBaseUrl() + path;
            Debug.Log("aktest 发送post消息 url：" + url + " param：" + param);
            using (UnityWebRequest webRequest = new UnityWebRequest(url, "POST"))
            {
                foreach (var v in m_headers)
                {
                    webRequest.SetRequestHeader(v.Key, v.Value);
                }

                if (param == null)
                {
                    param = "";
                }
                
                byte[] bodyRaw = Encoding.UTF8.GetBytes(param);
                webRequest.uploadHandler = new UploadHandlerRaw(bodyRaw);
                webRequest.downloadHandler = new DownloadHandlerBuffer();
                webRequest.timeout = TIME_OUT;
            
                yield return webRequest.SendWebRequest();
            
                if (webRequest.result != UnityWebRequest.Result.Success)
                {
                    string err = webRequest.error;
                    if (string.IsNullOrEmpty(webRequest.downloadHandler.text))
                    {
                        err += "\n" + webRequest.downloadHandler.text;
                    }
                
                    Debug.LogError(err);
                
                    IBaseHttp.Err error = new IBaseHttp.Err();
                    error.type = IBaseHttp.EErrType.Unknown;
                    error.msg = err;

                    if (m_failCallback != null)
                    {
                        m_failCallback(error);
                    }
                
                    if (fail != null)
                    {
                        fail(error);
                    }
                }
                else
                {
                    if (success != null)
                    {
                        success(webRequest.downloadHandler.text);
                    }
                }
            }
        }

        public abstract string getBaseUrl();
    }
}