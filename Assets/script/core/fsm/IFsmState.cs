using System;

namespace kf
{
    public interface IFsmState
    {
        public void onCreate(StateMachine machine);
        public void onEnter();
        public void onUpdate();
        public void onExit();
    }
}