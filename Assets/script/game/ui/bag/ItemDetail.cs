using System.Collections;
using ak;
using kf;
using script.game.enums;
using script.game.mgr;
using script.game.net.http;
using script.game.net.msg.client.item;
using script.game.net.msg.server.item;
using script.game.obj.config;
using script.game.ui.bag.type;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.bag
{
    public class ItemDetail : MonoBehaviour
    {
        private GameObject m_objDetail;
        private Image m_imgIcon;
        private Image m_imgQuality;
        private TMP_Text m_txtName;
        private TMP_Text m_txtSubType;
        private TMP_Text m_txtIntro;
        private TMP_Text m_txtSalePrice;

        private GameObject m_objBtnSale;
        private GameObject m_objBtnSource;

        private Item m_item;
        
        private void Awake()
        {
            m_objDetail = KtUnity.findObj(gameObject, "objDetail");
            
            GameObject objImgIcon = KtUnity.findObj(gameObject, "imgIcon");
            m_imgIcon = objImgIcon.GetComponent<Image>();
            
            GameObject objImgQuality = KtUnity.findObj(gameObject, "imgQuality");
            m_imgQuality = objImgQuality.GetComponent<Image>();

            GameObject objTxtName = KtUnity.findObj(gameObject, "txtName");
            m_txtName = objTxtName.GetComponent<TMP_Text>();
            
            GameObject objTxtSubType = KtUnity.findObj(gameObject, "txtSubType");
            m_txtSubType = objTxtSubType.GetComponent<TMP_Text>();
            
            GameObject objTxtIntro = KtUnity.findObj(gameObject, "txtIntro");
            m_txtIntro = objTxtIntro.GetComponent<TMP_Text>();

            m_objBtnSale = KtUnity.findObj(gameObject, "btnSale");
            KtUI.addListener(m_objBtnSale, () =>
            {
                CSaleItem msg = new CSaleItem();
                msg.itemId = m_item.id;
                msg.num = 1;
                
                GameRequest.getInstance().saleItem(msg, item =>
                {
                    GameEvent.sendEvent(EEvent.OnSellItemOk, new object[]{msg.itemId, msg.num});
                    AlertUtil.alertInfo("售卖成功");
                    Player.getInstance().refresh();
                });
            });
            
            GameObject objTxtSalePrice = KtUnity.findObj(gameObject, "txtSalePrice");
            m_txtSalePrice = objTxtSalePrice.GetComponent<TMP_Text>();
            
            m_objBtnSource = KtUnity.findObj(gameObject, "btnSource");
            KtUI.addListener(m_objBtnSource, () =>
            {
                AlertUtil.alertInfo("贱强还不速速起床设计界面！");
            });
            
        }

        public void setVisible(bool b)
        {
            m_objDetail.SetActive(b);
        }

        public void load(Item item)
        {
            this.m_item = item;
            setVisible(true);
            StartCoroutine(loadAsync());
        }

        IEnumerator loadAsync()
        {
            yield return null;
            
            CfgItem cfgItem = CfgMgr.getInstance().getItem(m_item.typeId);
            m_imgIcon.sprite = ResMgr.getInstance().load<Sprite>(ItemUtil.getIconPath(m_item.typeId));
            m_imgQuality.sprite = ResMgr.getInstance().load<Sprite>(ItemUtil.getQualityFramePath(cfgItem.quality));

            m_txtName.text = cfgItem.name;
            m_txtSubType.text = ItemTypeUtil.getTypeName(cfgItem.type);
            m_txtIntro.text = cfgItem.intro;
            m_txtSalePrice.text = cfgItem.sale_money + "";
        }
    }
}