using System.Net;
using kf;
using script.game.mgr;
using script.game.ui.waiting;
using UnityEngine;

namespace script.game.net.http
{
    public class ErrHandler
    {
        public static void onFail(IBaseHttp.Err err)
        {
            if (err.type == IBaseHttp.EErrType.Custom)
            {
                if (err.code == GameStatusCode.TOKEN_ERROR || err.code == GameStatusCode.VERIFY_ACC_TOKEN_ERROR)
                {
                    // todo: 自动重新登陆获取新token，尝试n次后不成功则弹框提示回到登陆界面
                    Debug.Log("token错误：" + err.msg);

                    alertToLogin(err.msg);
                }
                else if (err.code == GameStatusCode.NOT_LOGIN_ERROR)
                {
                    // 网络异常或心跳未续导致掉线
                    // todo: 重新获取gameToken
                    alertToLogin(err.msg);
                }
                else
                {
                    AlertUtil.alertInfo(err.msg);
                }
            }
            else
            {
                AlertUtil.alertInfo(err.msg);
            }
        }

        private static void alertToLogin(string msg)
        {
            StoreMgr.getInstance().clearToken();
            AlertUtil.alertInfo(msg, () =>
            {
                UIMgr.getInstance().show<LoginBgUI>();
                UIMgr.getInstance().show<LoginUI>();
            });
        }
    }
}