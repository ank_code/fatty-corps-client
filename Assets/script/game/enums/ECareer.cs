namespace script.game.enums
{
    // 职业
    public enum ECareer
    {
        // 战士
        Soldier,
        // 刺客
        Assassin,
        // 牧师
        Priest,
        // 游侠
        Ranger
    }
}