using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

namespace kf
{
    public class Singleton<T> where T : class
    {
        private static T instance;

        public static T getInstance()
        {
            if (instance == null)
            {
                Type t = typeof(T);
                instance = (T)Activator.CreateInstance(t, true);
            }

            return instance;
        }
    }
}