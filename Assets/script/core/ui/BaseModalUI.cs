namespace kf
{
    public class BaseModalUI : BaseUI, IBaseModalUI
    {
        public virtual void onShow(IUIParam param)
        {
            // 增加全屏半透明遮罩
        }

        public float modalBgTrans()
        {
            return 0.5f;
        }

        public void onClickModalBg()
        {
            close();
        }
    }
}