namespace script.game.net.msg.server
{
    public class HeroPos
    {
        public bool leftTeam;
        public int standPosNo;

        public override int GetHashCode()
        {
            int left = leftTeam ? 0 : 10;
            return left + standPosNo;
        }

        public override bool Equals(object obj)
        {
            return GetHashCode() == obj.GetHashCode();
        }
    }
}