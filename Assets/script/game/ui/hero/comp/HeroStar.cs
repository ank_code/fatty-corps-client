using System;
using System.Collections.Generic;
using UnityEngine;

namespace script.game.ui.bag.comp
{
    public class HeroStar : MonoBehaviour
    {
        private List<GameObject> m_starList = new List<GameObject>();
        
        private void Awake()
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                m_starList.Add(child.gameObject);
            }
        }

        public void setStar(int star)
        {
            int index = star - 1;
            if (index < 0 || index >= m_starList.Count)
            {
                // throw new Exception("错误的星级:" + star);
                return;
            }

            for (int i = 0; i < m_starList.Count; i++)
            {
                m_starList[i].SetActive(i == index);
            }
        }
    }
}