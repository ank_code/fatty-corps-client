using System;
using System.Collections;
using ak;
using kf;
using script.game.enums;
using script.game.mgr;
using script.game.net.msg.server.item;
using script.game.obj.config;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace script.game.ui.bag
{
    public class BagItem : MonoBehaviour
    {
        private GameObject m_objImgIcon;
        private GameObject m_objImgFrame;
        private GameObject m_objTxtNum;
        
        private Image m_imgIcon;
        private TMP_Text m_txtNum;
        private Image m_imgFrame;
        private CfgItem m_cfg;
        private GameObject m_objChoosed;
        private bool m_init = false;

        private GameObject m_btn;

        private Item m_item;
        private int m_index;
        private Action m_onClick = null;

        private void Awake()
        {
            init();
        }

        public void init()
        {
            if (m_init)
            {
                return;
            }

            m_init = true;
            
            m_objImgIcon = KtUnity.findObj(gameObject, "imgIcon");
            m_imgIcon = m_objImgIcon.GetComponent<Image>();
            KtUnity.addComponent<ScrollElem>(m_objImgIcon);
            KtUI.addListener(m_objImgIcon, onClick);
            
            m_objImgFrame = KtUnity.findObj(gameObject, "imgFrame");
            m_imgFrame = m_objImgFrame.GetComponent<Image>();
            
            m_objTxtNum = KtUnity.findObj(gameObject, "txtNum");
            KtUnity.addComponent<ScrollElem>(m_objTxtNum);
            KtUI.addListener(m_objTxtNum, onClick);
            
            m_txtNum = m_objTxtNum.GetComponent<TMP_Text>();
            m_txtNum.text = "";
            
            m_objChoosed = KtUnity.findObj(gameObject, "imgChoosed");

            setChoosed(false);
            setVisible(false);

            this.m_onClick = onClick;
        }

        public int getId()
        {
            return m_item.id;
        }

        public int getIndex()
        {
            return m_index;
        }

        public void delNum(int dNum)
        {
            m_txtNum.text = Int32.Parse(m_txtNum.text) - dNum + "";
        }

        public void setOnClick(Action onClick)
        {
            this.m_onClick = onClick;
        }

        private void onClick()
        {
            if (m_onClick != null)
            {
                m_onClick();
            }
        }

        public void setVisible(bool b)
        {
            if (!b)
            {
                m_objImgIcon.name = "objImgIcon_null";
                gameObject.name = "item_null";
            }
            else
            {
                m_objImgIcon.name = "objImgIcon_show";
            }
            
            m_objImgIcon.SetActive(b);
            m_objImgFrame.SetActive(b);
            m_objTxtNum.SetActive(b);
        }

        public bool isChoosed()
        {
            return m_objChoosed.activeSelf;
        }

        public void setChoosed(bool b)
        {
            m_objChoosed.SetActive(b);
        }
        
        public void reload(Item item, int index)
        {
            m_item = item;
            m_index = index;
            
            // 必须先设置为展示状态，不能在load后再展示，否则会覆盖需要隐藏的设置
            setVisible(true);
            StartCoroutine(load(item));
        }

        IEnumerator load(Item item)
        {
            yield return null;
            m_init = true;
            
            m_cfg = CfgMgr.getInstance().getItem(item.typeId);
            var sprIcon = ResMgr.getInstance().load<Sprite>(ItemUtil.getIconPath(item.typeId));
            m_imgIcon.sprite = sprIcon;
            
            var sprFrame = ResMgr.getInstance().load<Sprite>(ItemUtil.getQualityFramePath(m_cfg.quality));
            m_imgFrame.sprite = sprFrame;

            m_txtNum.text = item.num.ToString();
        }
    }
}