using ak;
using UnityEngine;
using UnityEngine.UI;

namespace kf.progressBar
{
    public class ProgressBar
    {
        private GameObject m_obj;
        private Image m_imgContent;
        
        public void init(GameObject obj)
        {
            m_obj = obj;

            GameObject objImgContent = KtUnity.findObj(m_obj, "content");
            m_imgContent = objImgContent.GetComponent<Image>();
        }

        public void setVal(float v)
        {
            if (v < 0)
            {
                v = 0;
            }
            else if (v > 1.0f)
            {
                v = 1.0f;
            }

            m_imgContent.fillAmount = v;
        }
    }
}