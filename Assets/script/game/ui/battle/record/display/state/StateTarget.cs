using System;
using kf;
using kf.anim;
using script.game.enums;
using script.game.enums.battle;
using script.game.mgr;
using script.game.net.msg.server;
using script.game.obj.config;
using UnityEngine;

namespace script.game.ui.battle.effect.anim.state
{
    /**
     * step2: 敌方动作阶段
     */
    public class StateTarget : FsmState
    {
        private CfgBehaviorDisplay m_cfg;
        private BattleHero m_caster;
        
        public override void onEnter()
        {
            Debug.Log("aktest StateTarget 1");
            m_cfg = (CfgBehaviorDisplay)getBlackboard(DisplayInvoker.BB_CFG);
            RecElem recElem = (RecElem)getBlackboard(DisplayInvoker.BB_REC_ELEM);

            Debug.Log("aktest StateTarget 2");
            var displayType = m_cfg == null ? EBhvTargetDisplayType.None : m_cfg.target_display_type;
            
            Debug.Log("aktest StateTarget 3");
            switch (displayType)
            {
                case EBhvTargetDisplayType.None:
                    Debug.Log("aktest StateTarget 4");
                    invokeFinalEffect(recElem);
                    Debug.Log("aktest StateTarget 5");
                    break;
                case EBhvTargetDisplayType.Target:
                    throw new Exception("暂不支持的目标特效展示方式");
                case EBhvTargetDisplayType.FullScreen:
                    Debug.Log("aktest StateTarget 6");
                    playFullScreenEffectAnim(recElem);
                    Debug.Log("aktest StateTarget 7");
                    break;
            }
        }

        // 执行最终效果
        private void invokeFinalEffect(RecElem recElem)
        {
            for (var i = 0; i < recElem.effects.Count; i++)
            {
                var recElemEffect = recElem.effects[i];
                targetUnderAtt(recElemEffect);
            }
        }
        
        // 目标受攻击
        private void targetUnderAtt(RecElemEffect effect)
        {
            BattleHero hero = BattleHeroMgr.getInstance().getHeroByPos(effect.targetPos);
            
            // todo： 播放受击动画，目前没有。。。
            
            // 触发效果
            // switch (effect.opportunity)
            // {
            //     case ERecEffectPlayOpportunity.AfterTargetUnderAttack:
            //         
            //         break;
            //     default:
            //         throw new Exception("暂不支持的触发时机");
            //         break;
            // }

            switch (effect.type)
            {
                case ERecEffectType.Damage:
                    // todo: 是否暴击
                    GameEvent.sendEvent(EEvent.OnBattleEffectDamage, new object[]{hero, effect.value, effect.extra});
                    break;
                case ERecEffectType.Dead:
                    break;
                case ERecEffectType.Revive:
                    break;
                case ERecEffectType.AddBuff:
                    GameEvent.sendEvent(EEvent.OnBattleEffectAddBuff, new object[]{hero, effect.value});
                    break;
                case ERecEffectType.RemoveBuff:
                    GameEvent.sendEvent(EEvent.OnBattleEffectRemoveBuff, new object[]{hero, effect.value});
                    break;
                case ERecEffectType.AddEnergy:
                    GameEvent.sendEvent(EEvent.OnBattleEffectAddEnergy, new object[]{hero, effect.value, effect.extra});
                    break;
                default:
                    throw new Exception("未知的效果类型");
            }
        }
        
        private void playFullScreenEffectAnim(RecElem recElem)
        {
            Debug.Log("aktest playFullScreenEffectAnim 1");
            if (string.IsNullOrEmpty(m_cfg.effect_path))
            {
                invokeFinalEffect(recElem);
                return;
            }

            Debug.Log("aktest playFullScreenEffectAnim 2");
            // 获取特效播放位置
            RecElemEffect elem = recElem.effects[0];
            Vector3 pos = BattlePos.getInstance().getEnemyCenter(!elem.targetPos.leftTeam);
            
            Debug.Log("aktest playFullScreenEffectAnim 3");
            EffectAnimFactory.getInstance().showEffectOnce(m_cfg.effect_path, pos, !elem.targetPos.leftTeam, (ce) =>
            {
                Debug.Log("aktest showEffectOnce 1");
                if (ce.e.Equals(SpineAnim.ECallBackEvent.KeyFrame))
                {
                    Debug.Log("aktest showEffectOnce 2");
                    // 关键帧触发最终效果
                    invokeFinalEffect(recElem);
                    Debug.Log("aktest showEffectOnce 3");
                }
                else
                {
                    Debug.Log("aktest showEffectOnce 4");
                    if (!ce.playedKeyFrame)
                    {
                        Debug.Log("aktest showEffectOnce 5");
                        invokeFinalEffect(recElem);
                        Debug.Log("aktest showEffectOnce 6");
                    }
                }
            });
            
            Debug.Log("aktest playFullScreenEffectAnim 4");
        }
    }
}