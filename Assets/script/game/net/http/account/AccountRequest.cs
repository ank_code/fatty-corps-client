using System;
using ak;
using kf;
using script.game.net.msg.client;
using script.game.net.msg.server;
using UnityEngine;

namespace script.game.net.http
{
    public class AccountRequest : Singleton<AccountRequest>
    {
        private bool m_init = false;
        private AccountHttp m_http = null;
        
        public void init()
        {
            if (m_init)
            {
                return;
            }

            m_init = true;

            GameObject obj = new GameObject("accountRequest");
            GameObject.DontDestroyOnLoad(obj);

            m_http = KtUnity.addComponent<AccountHttp>(obj);
            m_http.init();
            m_http.setFailCallback(ErrHandler.onFail);
        }
        
        public void login(string account, string password, Action<SLogin> callback)
        {
            CLogin cLogin = new CLogin();
            cLogin.account = account;
            cLogin.password = password;
            m_http.postAsync<CLogin, SLogin>("/login", cLogin, (rep) =>
            {
                if (callback != null)
                {
                    callback(rep);
                }
            }, false, null);
        }

        public void reg(string account, string password, Action callback)
        {
            CReg cReg = new CReg();
            cReg.account = account;
            cReg.password = password;
            m_http.postAsync<CReg, SReg>("/reg", cReg, (rep) =>
            {
                if (callback != null)
                {
                    callback();
                }
            }, false, null);
        }
    }
}