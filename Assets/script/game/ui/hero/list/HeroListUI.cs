using System.Collections.Generic;
using System.Linq;
using ak;
using kf;
using script.game.enums;
using script.game.enums.item;
using script.game.mgr;
using script.game.net.http;
using script.game.net.msg.client.hero;
using script.game.net.msg.server.hero;
using script.game.ui.bag;
using script.game.ui.bag.type;
using script.game.ui.comm.comp;
using script.game.ui.hero.detail;
using SuperScrollView;
using UnityEngine;

namespace script.game.ui.hero
{
    public class HeroListUI : GameUI
    {
        public class UIParam : IUIParam
        {
            public List<HeroShort> heroList;
        }
        
        private LoopListView2 m_loopListView;
        private GameObject m_objScrollView;
        
        private GameObject m_objTypeTabBar;
        private GameObject m_objType;
        
        private CurrencyInfo m_currencyInfo;

        private List<HeroShort> m_allHeroList;
        private List<HeroShort> m_curHeroList = new List<HeroShort>();
        
        private bool m_initLoopList = false;

        private const int PER_ROW_ITEM_NUM = 8;
        
        public static void open()
        {
            GameRequest.getInstance().getHeroList((rep) =>
            {
                UIParam param = new UIParam();
                param.heroList = rep.heroList;
                UIMgr.getInstance().show<HeroListUI>(param);
            });
        }
        
        public override void onCreate(IUIParam param)
        {
            m_allHeroList = ((UIParam)param).heroList;
            
            GameObject m_objBtnClose = KtUnity.findObj(gameObject, "btnClose");
            KtUI.addListener(m_objBtnClose, () =>
            {
                this.close();
            });
            
            m_objScrollView = KtUnity.findObj(gameObject, "svHeroList");
            m_loopListView = m_objScrollView.GetComponent<LoopListView2>();
            
            // type tabbar
            m_objType = KtUnity.findObj(gameObject, "type");
            m_objTypeTabBar = KtUnity.findObj(gameObject, "typeContent");
            KtUnity.addComponent<HeroTypeTabBar>(m_objTypeTabBar);
            
            // 货币
            // GameObject objCurrency = KtUnity.findObj(gameObject, "currency");
            // m_currencyInfo = new CurrencyInfo();
            // m_currencyInfo.init(objCurrency);
            //
            // m_currencyInfo.add(CurrencyInfo.ECurrencyType.Money);            
            // m_currencyInfo.add(CurrencyInfo.ECurrencyType.Coin);
        }

        public override void onShow()
        {
            // 入场动画
        }

        private void initData(HeroUICampUtil.EUICamp uiCamp)
        {
            m_curHeroList.Clear();
            
            if (uiCamp == HeroUICampUtil.EUICamp.All)
            {
                m_curHeroList.AddRange(m_allHeroList);
            }
            else
            {
                for (var i = 0; i < m_allHeroList.Count; i++)
                {
                    var cfgHero = CfgMgr.getInstance().getHero(m_allHeroList[i].typeId);
                    int campIndex = (int)cfgHero.camp;
                    int showCampIndex = (int)uiCamp - 1;
                    if (campIndex == showCampIndex)
                    {
                        m_curHeroList.Add(m_allHeroList[i]);
                    }
                }
            }
            
            int rows = m_curHeroList.Count / PER_ROW_ITEM_NUM;
            if (m_curHeroList.Count % PER_ROW_ITEM_NUM > 0)
            {
                rows++;
            }

            if (m_initLoopList)
            {
                m_loopListView.SetListItemCount(rows, false);
                m_loopListView.RefreshAllShownItem();
            }
            else
            {
                m_loopListView.InitListView(rows, onGetItem);
                m_initLoopList = true;
            }
        }
        
        private LoopListViewItem2 onGetItem(LoopListView2 view2, int rowIndex)
        {
            if (rowIndex < 0)
            {
                return null;
            }

            var row = view2.NewListViewItem("heroElemRow");
            row.gameObject.name = "row_" + rowIndex;
            
            for (int i = 0; i < row.transform.childCount; i++)
            {
                GameObject objItem = row.transform.GetChild(i).gameObject;
                var item = KtUnity.addComponent<HeroItem>(objItem);
                int index = rowIndex * PER_ROW_ITEM_NUM + i;
                
                if (index >= m_curHeroList.Count)
                {
                    item.setVisible(false);
                    continue;
                }
                
                objItem.name = "hero_" + index;
                HeroShort heroShort = m_curHeroList[index];
                item.reload(heroShort, index);
            }
            
            return row;
        }
        
        public override void onEvent(int eId, object[] objs)
        {
            EEvent e = (EEvent)eId;
            switch (e)
            {
                case EEvent.OnHeroItemClick:
                {
                    int heroId = (int)objs[0];
                    int bagIndex = (int)objs[1];
                    
                    HeroDetailUI.UIParam param = new HeroDetailUI.UIParam();
                    param.allHeroShorts = this.m_allHeroList;
                    param.bagIndex = bagIndex;
                    UIMgr.getInstance().show<HeroDetailUI>(param);
                    
                    this.close();
                    break;
                }
                case EEvent.OnHeroSwitchType:
                {
                    HeroUICampUtil.EUICamp type = (HeroUICampUtil.EUICamp)objs[0];
                    initData(type);
                    break;
                }
            }
        }
    }
}