using System.Collections.Generic;
using ak;
using kf;
using script.game.net.msg.server.item;
using script.game.ui.comm.comp;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.battle
{
    public class BattleResultUI : GameUI
    {
        public class Param : IUIParam
        {
            public List<DItem> awards;
        }

        public override void onCreate(IUIParam param)
        {
            base.onCreate(param);
            Param p = (Param)param;
            
            KtUI.addListenerAutoScale(KtUnity.findObj(gameObject, "btnOk"), () =>
            {
                this.close();
            });
            
            var objAwards = KtUnity.findObj(gameObject, "awards");
            
            for (var i = 0; i < p.awards.Count; i++)
            {
                ItemAward award = new ItemAward();
                award.init(objAwards, p.awards[i].typeId, p.awards[i].num, false);
            }
            
            GameObject objSvItems = KtUnity.findObj(gameObject, "svAwards");
            GameObject objContent = KtUnity.findObj(objSvItems, "Content");
            var contentRT = objContent.GetComponent<RectTransform>();
            var awardsRT = objAwards.GetComponent<RectTransform>();
            
            LayoutRebuilder.ForceRebuildLayoutImmediate(awardsRT);
            
            contentRT.sizeDelta = new Vector2(awardsRT.rect.width, awardsRT.rect.height);
            contentRT.localPosition = Vector3.zero;
            
            LayoutRebuilder.ForceRebuildLayoutImmediate(contentRT);
        }
    }
}