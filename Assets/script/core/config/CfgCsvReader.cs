using System;
using System.Collections.Generic;
using System.Text;
using kf;
using UnityEngine;

namespace script.core.config
{
    
    public class CfgCsvReader : ICfgReader
    {
        public List<T> read<T>(string path) where T : new()
        {
            try
            {
                return CsvUtility.Read<T>(ResMgr.getInstance().loadRawFile(path));
            }
            catch (Exception e)
            {
                Debug.LogError("读取配置文件失败：" + path);
                throw e;
            }
        }

        public void readAsync<T>(string path, Action<List<T>> callback) where T : new()
        {
            ResMgr.getInstance().loadRawFile(path, true, (rawFile) =>
            {
                callback(CsvUtility.Read<T>((byte[])rawFile));
            });
        }
    }
}