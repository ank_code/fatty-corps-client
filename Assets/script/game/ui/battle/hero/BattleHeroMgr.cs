using System.Collections.Generic;
using kf;
using script.game.net.msg.server;
using UnityEngine;

namespace script.game.ui.battle
{
    public class BattleHeroMgr : Singleton<BattleHeroMgr>
    {
        private Dictionary<HeroPos, BattleHero> m_heroDic = new Dictionary<HeroPos, BattleHero>();
        
        public void init(List<BattleHero> heroes)
        {
            if (m_heroDic.Count > 0)
            {
                m_heroDic.Clear();
            }

            for (var i = 0; i < heroes.Count; i++)
            {
                m_heroDic.Add(heroes[i].getPos(), heroes[i]);
            }
        }

        public BattleHero getHeroByPos(HeroPos pos)
        {
            return m_heroDic[pos];
        }
    }
}