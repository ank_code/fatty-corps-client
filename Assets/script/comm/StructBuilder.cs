using ak;
using kf;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class StructBuilder : Singleton<StructBuilder>
{
    private GameObject m_objUI;
    private GameObject m_objLogic;

    private GameObject m_objCanvas;
    private GameObject m_objEventSystem;

    private bool m_init = false;
    
    // 构造游戏结构
    public void init(GameObject root)
    {
        if (m_init)
        {
            return;
        }

        m_init = true;

        // system
        buildSys(root);
        
        // ui
        buildUI(root);

        // nor
        buildLogic(root);
    }

    public GameObject getUIObj()
    {
        return m_objUI;
    }

    public GameObject getLogicObj()
    {
        return m_objLogic;
    }

    public GameObject getCanvasObj()
    {
        return m_objCanvas;
    }

    public GameObject getEventSystemObj()
    {
        return m_objEventSystem;
    }

    private void buildSys(GameObject root)
    {
        // eventSystem
        m_objEventSystem = new GameObject("eventSystem");
        m_objEventSystem.transform.SetParent(root.transform);

        KtUnity.addComponent<EventSystem>(m_objEventSystem);
        KtUnity.addComponent<StandaloneInputModule>(m_objEventSystem);
        
        // etc...
    }

    // ui层
    private void buildUI(GameObject root)
    {
        // ui根结点
        m_objUI = new GameObject("ui");
        m_objUI.transform.SetParent(root.transform);
        
        // canvas
        m_objCanvas = new GameObject("canvas");
        m_objCanvas.transform.SetParent(m_objUI.transform);
        Canvas canvas = KtUnity.addComponent<Canvas>(m_objCanvas);
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;

        CanvasScaler scaler = KtUnity.addComponent<CanvasScaler>(m_objCanvas);
        scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        scaler.referenceResolution = new Vector2(GlobalConst.DES_SCREEN_WIDTH, GlobalConst.DES_SCREEN_HEIGTH);
        scaler.screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
        
        KtUnity.addComponent<GraphicRaycaster>(m_objCanvas);
    }

    // 游戏逻辑层
    private void buildLogic(GameObject root)
    {
        // 游戏逻辑根结点
        m_objLogic = new GameObject("logic");
        m_objLogic.transform.SetParent(root.transform);
    }
    
    
}