using System;
using kf;
using script.game.enums;

namespace script.game.ui
{
    public class GameUI : BaseUI
    {
        public void sendEvent(EEvent eId, Object[] objs, EventMgr.scope onBefore = null, EventMgr.scope onComplete = null)
        {
            EventMgr.getInstance().sendEvent((int)eId, objs, onBefore, onComplete);
        }
    }
}