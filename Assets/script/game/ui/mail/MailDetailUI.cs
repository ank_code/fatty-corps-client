using System.Collections.Generic;
using ak;
using kf;
using script.game.enums;
using script.game.net.http;
using script.game.net.msg.client.hero;
using script.game.net.msg.server.item;
using script.game.ui.bag;
using script.game.ui.comm.comp;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.mail
{
    public class MailDetailUI : GameUI
    {
        private DMail m_mail;
        private int m_index;

        private List<ItemAward> m_itemAwards = new List<ItemAward>();
        private GameObject m_objBtnAward;
        
        private const string SENDER_PREFIX = "发件人：";

        public class Param : IUIParam
        {
            public DMail mail;
            public int index;
        }

        public override void onCreate(IUIParam param)
        {
            base.onCreate(param);

            m_mail = ((Param)param).mail;
            m_index = ((Param)param).index;

            var btnClose = KtUnity.findObj(gameObject, "btnClose");
            KtUI.addListenerAutoScale(btnClose, () =>
            {
                this.close();
            });

            initLeft();
            initRight();
        }

        private void initLeft()
        {
            var objSender = KtUnity.findObj(gameObject, "txtSender");
            var txtSender = objSender.GetComponent<TMP_Text>();
            txtSender.text = SENDER_PREFIX + "系统邮件";
            
            var objContent = KtUnity.findObj(gameObject, "txtContent");
            var txtContent = objContent.GetComponent<TMP_Text>();
            txtContent.text = m_mail.content;
        }

        private void initRight()
        {
            List<DItem> itemList = ItemUtil.strToAwardList(m_mail.attachment);

            GameObject objAwards = KtUnity.findObj(gameObject, "awards");
            for (var i = 0; i < itemList.Count; i++)
            {
                ItemAward award = new ItemAward();
                award.init(objAwards, itemList[i].typeId, itemList[i].num, m_mail.getAttachment);
                m_itemAwards.Add(award);
            }

            GameObject objSvItems = KtUnity.findObj(gameObject, "svItems");
            GameObject objContent = KtUnity.findObj(objSvItems, "Content");
            var contentRT = objContent.GetComponent<RectTransform>();
            var awardsRT = objAwards.GetComponent<RectTransform>();
            
            LayoutRebuilder.ForceRebuildLayoutImmediate(awardsRT);
            
            contentRT.sizeDelta = new Vector2(awardsRT.rect.width, awardsRT.rect.height);
            contentRT.localPosition = Vector3.zero;

            m_objBtnAward = KtUnity.findObj(gameObject, "btnGetAward");
            GameObject objBtnAwardClickArea = KtUnity.findObj(m_objBtnAward, "btnClickArea");
            KtUI.addListener(m_objBtnAward, objBtnAwardClickArea, () =>
            {
                GameRequest.getInstance().getAttachment(m_mail.id, (rep) =>
                {
                    EventMgr.getInstance().sendEvent((int)EEvent.OnMailGetAttachment, new object[]{m_mail.id, m_index});
                });
            });

            if (string.IsNullOrEmpty(m_mail.attachment) || m_mail.getAttachment)
            {
                m_objBtnAward.SetActive(false);
            }
        }

        private void onGetAttachment()
        {
            for (var i = 0; i < m_itemAwards.Count; i++)
            {
                m_itemAwards[i].setAward(true);
            }
            
            m_objBtnAward.SetActive(false);
        }
        
        public override void onEvent(int eId, object[] objs)
        {
            base.onEvent(eId, objs);
            EEvent e = (EEvent)eId;
            switch (e)
            {
                case EEvent.OnMailGetAttachment:
                {
                    int mailId = (int)objs[0];
                    int idx = (int)objs[1];
                    if (mailId == this.m_mail.id)
                    {
                        onGetAttachment();
                    }
                    break;
                }
            }
        }
    }
}