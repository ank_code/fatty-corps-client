using System;

namespace kf
{
    public class KtTime
    {
        public static String FORMAT_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
        public static String FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
        public static String FORMAT_HH_MM_SS = "HH:mm:ss";
        public static String FORMAT_HH_MM_MM_DD = "HH:mm MM-dd";
        
        public static long getTimestampToSeconds()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds);
        }

        /// <summary>获取毫秒级别时间戳（13位）</summary>
        public static long getTimeStampToMilliseconds()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalMilliseconds);
        }

        public static int getTimpTimeFromStr(string time)
        {
            int index = time.IndexOf(":");
            int h = int.Parse(time.Substring(0, index));
            int m = int.Parse(time.Substring(index + 1, time.Length - index - 1));

            return h * 3600 + m * 60;
        }

        public static string formatTimpTime(long timestamp, string format)
        {
            DateTime baseTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            DateTime date = baseTime.AddSeconds(timestamp).ToLocalTime();
            return date.ToString(format);
        }

        public static string formatDTime(int dTime)
        {
            int h = dTime / 3600;
            int m = dTime / 60 - h * 60;
            int s = dTime % 60;
            return formatFillZero(h) + ":" + formatFillZero(m) + ":" + formatFillZero(s);
        }

        private static string formatFillZero(int v)
        {
            return v < 10 ? "0" + v : v + "";
        }
    }
}