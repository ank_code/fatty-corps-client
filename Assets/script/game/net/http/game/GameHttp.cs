using System;
using kf;
using script.game.mgr;
using script.game.ui.waiting;
using UnityEngine;

namespace script.game.net.http
{
    public class GameHttp : MsgHttp
    {
        public const string HEADER_TOKEN = "token";
        
        public override string getToken()
        {
            return (string)StoreMgr.getInstance().get(StoreMgr.KEY_GAME_TOKEN);
        }

        public override void postAsync<Req, RepMsg>(string path, Req param, Action<RepMsg> success, bool useToken = true, Action<IBaseHttp.Err> fail = null)
        {
            Action<RepMsg> lSuccess = (rep) =>
            {
                UIMgr.getInstance().hide<LoadingUI>();
                success(rep);
            };

            Action<IBaseHttp.Err> lFail = (err) =>
            {
                UIMgr.getInstance().hide<LoadingUI>();
                if (fail != null)
                {
                    fail(err);
                }
            };
            
            UIMgr.getInstance().show<LoadingUI>(null, (ui) =>
            {
                base.postAsync(path, param, lSuccess, useToken, lFail);
            });
        }

        public override string getBaseUrl()
        {
            return GlobalCfg.getGameHost();
        }
    }
}