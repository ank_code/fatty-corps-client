using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kf
{
    public class KtHtml
    {
        public static string richTextToHtml(string richText)
        {
            string baseHtml = "<div style=\"width: 100%; WORD-WRAP: break-word; \">";
            richText = baseHtml + richText;
            richText += "</div>";
            return richText;
        }
    }

}