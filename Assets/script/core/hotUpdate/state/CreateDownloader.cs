using System.Collections;
using UnityEngine;
using YooAsset;

namespace kf.hotpUpdate.state
{
    public class CreateDownloader : FsmState
    {
        public override void onEnter()
        {
            HotUpdate.HotUpdateMono mono = (HotUpdate.HotUpdateMono)getMachine().getBlackboard(HotUpdate.BBK_MONOBEHAVIOUR);
            mono.startCoroutine(createDownloader());
        }
        
        IEnumerator createDownloader()
        {
            yield return null;

            var packageName = (string)getMachine().getBlackboard(HotUpdate.BBK_YOOASSET_PACKAGE_NAME);
            var package = YooAssets.GetPackage(packageName);
            int downloadingMaxNum = 10;
            int failedTryAgain = 3;
            var downloader = package.CreateResourceDownloader(downloadingMaxNum, failedTryAgain);
            getMachine().setBlackboard(HotUpdate.BBK_DOWNLOADER, downloader);

            if (downloader.TotalDownloadCount == 0)
            {
                Debug.Log("Not found any download files !");
                getMachine().setState<InitHybrid>();
            }
            else
            {
                // 发现新更新文件后，挂起流程系统
                // 注意：开发者需要在下载前检测磁盘空间不足
                getMachine().setBlackboard(HotUpdate.BBK_TOTAL_DOWNLOAD_COUNT, downloader.TotalDownloadCount);
                getMachine().setBlackboard(HotUpdate.BBK_TOTAL_DOWNLOAD_BYTES, downloader.TotalDownloadBytes);
                getMachine().setState<WaitForConfirm>();
            }
        }
    }
}