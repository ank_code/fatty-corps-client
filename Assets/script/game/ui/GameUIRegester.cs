

using kf;
using script.game.ui.award;
using script.game.ui.bag;
using script.game.ui.bag.prop;
using script.game.ui.bag.skill;
using script.game.ui.battle;
using script.game.ui.chat;
using script.game.ui.hero;
using script.game.ui.hero.detail;
using script.game.ui.home;
using script.game.ui.lineup;
using script.game.ui.loading;
using script.game.ui.mail;
using script.game.ui.waiting;

public class GameUIRegester : Singleton<GameUIRegester>
{
    public void init()
    {
        UIMgr.getInstance().reg<LoginBgUI>("Assets/res/prefab/loginBgUI.prefab");
        UIMgr.getInstance().reg<LoginUI>("Assets/res/prefab/loginUI.prefab");
        UIMgr.getInstance().reg<RegUI>("Assets/res/prefab/regUI.prefab");
        UIMgr.getInstance().reg<HomeUI>("Assets/res/prefab/home/homeUI.prefab");
        UIMgr.getInstance().reg<BagUI>("Assets/res/prefab/bag/bagUI.prefab");
        UIMgr.getInstance().reg<BattleUI>("Assets/res/prefab/battle/battleUI.prefab");
        UIMgr.getInstance().reg<SceneLoadingUI>("Assets/res/prefab/loading/sceneLoadingUI.prefab");
        UIMgr.getInstance().reg<LoadingUI>("Assets/res/prefab/loading/loadingUI.prefab");
        UIMgr.getInstance().reg<HeroListUI>("Assets/res/prefab/hero/list/heroListUI.prefab");
        UIMgr.getInstance().reg<HeroDetailUI>("Assets/res/prefab/hero/detail/heroDetailUI.prefab");
        UIMgr.getInstance().reg<ChatUI>("Assets/res/prefab/chat/chatUI.prefab");
        UIMgr.getInstance().reg<LineupUI>("Assets/res/prefab/lineup/lineupUI.prefab");
        
        UIMgr.getInstance().reg<MailUI>("Assets/res/prefab/mail/mailUI.prefab");
        UIMgr.getInstance().reg<MailDetailUI>("Assets/res/prefab/mail/mailDetail.prefab");
        UIMgr.getInstance().reg<AwardUI>("Assets/res/prefab/award/awardUI.prefab");
        UIMgr.getInstance().reg<HangAwardUI>("Assets/res/prefab/award/hangAwardUI.prefab");
        UIMgr.getInstance().reg<BattleResultUI>("Assets/res/prefab/battle/battleResultUI.prefab");
        UIMgr.getInstance().reg<SkillUI>("Assets/res/prefab/hero/skill/skillUI.prefab");
        UIMgr.getInstance().reg<PropUI>("Assets/res/prefab/hero/detail/prop/propUI.prefab");
    }
}