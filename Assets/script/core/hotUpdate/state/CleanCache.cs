using YooAsset;

namespace kf.hotpUpdate.state
{
    public class CleanCache : FsmState
    {
        public override void onEnter()
        {
            var packageName = (string)getMachine().getBlackboard(HotUpdate.BBK_YOOASSET_PACKAGE_NAME);
            var package = YooAssets.GetPackage(packageName);
            var operation = package.ClearUnusedCacheFilesAsync();
            operation.Completed += Operation_Completed;
        }
        
        private void Operation_Completed(YooAsset.AsyncOperationBase obj)
        {
            getMachine().setState<HotUpdateOver>();
        }
    }
}