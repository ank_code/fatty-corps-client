using System;
using System.Collections.Generic;
using ak;
using kf;
using kf.hotpUpdate.state;
using script.game.ui.battle.effect.number;
using UnityEditor;
using UnityEngine;
using Object = System.Object;

namespace script.game.ui.battle.effect
{
    public class NumMgr : Singleton<NumMgr>
    {
        private Dictionary<ENumType, Font> m_fonts = new Dictionary<ENumType, Font>();
        private bool m_init = false;
        private GameObject m_parent;

        private Queue<BattleNum> m_numCache = new Queue<BattleNum>();
        
        public enum ENumType
        {
            // 白色数字，字号小一些，普通伤害，无需加减号
            NorDamage,
            // 红色数字，字号通用，暴击伤害，无需加减号
            Critical,
            // 绿色数字，字号通用，治疗或复活换血等增加血量，无需加减号
            Treat,
            // 紫色数字，字号通用，debuff伤血，无需加减号
            Dot,
            // 青色数字，字号通用，能量变化，带加减号
            Energy
        }
        
        // 加载所有font
        public void init()
        {
            if (m_init)
            {
                return;
            }

            m_init = true;
            
            foreach (ENumType value in Enum.GetValues(typeof(ENumType)))
            {
                var fontPath = getFontPath(value);
                ResMgr.getInstance().load<Font>(fontPath, false, true, o =>
                {
                    m_fonts.Add(value, (Font)o);
                });
            }
        }

        public void setParent(GameObject parent)
        {
            m_parent = parent;
        }

        public void clear()
        {
            while (m_numCache.Count > 0)
            {
                var battleNum = m_numCache.Dequeue();
                KtUnity.destory(battleNum.obj()); 
            }
        }
        
        private string getFontPath(ENumType type)
        {
            switch (type)
            {
                case ENumType.NorDamage:
                    return "Assets/res/font/baishuzi.fontsettings";
                case ENumType.Critical:
                    return "Assets/res/font/hongshuzi.fontsettings";
                case ENumType.Treat:
                    return "Assets/res/font/lvshuzi.fontsettings";
                case ENumType.Dot:
                    return "Assets/res/font/qinshuzi.fontsettings";
                case ENumType.Energy:
                    return "Assets/res/font/zishuzi.fontsettings";
                default:
                    throw new Exception("暂不支持的字体类型");
            }
        }

        private bool showSymbol(ENumType type)
        {
            switch (type)
            {
                case ENumType.NorDamage:
                case ENumType.Critical:
                case ENumType.Treat:
                case ENumType.Dot:
                    return false;
                case ENumType.Energy:
                    return true;
            }

            return false;
        }

        public Font getFont(ENumType type)
        {
            return m_fonts[type];
        }

        public void onNumPlayComplete(BattleNum num)
        {
            num.setActive(false);
            m_numCache.Enqueue(num);
        }
        

        public void showNum(ENumType type, long num, Vector3 pos)
        {
            bool ss = showSymbol(type);
            string numStr = "";
            if (ss)
            {
                numStr = num > 0 ? "+" + num : "-" + (-num);
            }
            else
            {
                numStr = num > 0 ? num + "" : "" + (-num);
            }

            BattleNum battleNum = null;
            if (m_numCache.Count > 0)
            {
                battleNum = m_numCache.Dequeue();
            }
            else
            {
                battleNum = new BattleNum();
                battleNum.init(m_parent);
            }

            battleNum.setType(type);
            battleNum.obj().name = "num_" + num;
            battleNum.obj().transform.position = pos;
            battleNum.obj().transform.SetAsLastSibling();
            battleNum.show(numStr, type);
        }
    }
}