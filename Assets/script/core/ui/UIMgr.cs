using System;
using System.Collections.Generic;
using ak;
using UnityEngine;

namespace kf
{
    /**
     * 使用流程
     * 1、注册UI：UI类,
     * 2、显示/隐藏UI
     * 3、销毁UI
     */
    public class UIMgr : Singleton<UIMgr>
    {
        // 层对象
        private List<GameObject> m_layers = new List<GameObject>();
        // ui配置信息
        private Dictionary<Type, UICfg> m_uiCfgs = new Dictionary<Type, UICfg>();
        // 正在展示/隐藏的Ui
        private Dictionary<Type, BaseUI> m_uis = new Dictionary<Type, BaseUI>();

        private class UICfg
        {
            public string prefabPath = "";
        }

        // 初始化，传入挂载canvas相关脚本的游戏物体
        public void init(GameObject objCanvas)
        {
            foreach (EUILayer e in Enum.GetValues(typeof(EUILayer)))
            {
                GameObject layer = new GameObject(e.ToString().ToLower());
                layer.transform.SetParent(objCanvas.transform);
                // 撑满父节点，也就是和canvas一样大小
                KtUnity.stretchFull(layer.AddComponent<RectTransform>());
                m_layers.Add(layer);
            }
        }

        // 注册界面，绑定类->prefab的关系
        public void reg<T>(string prefabPath)
        {
            reg(typeof(T), prefabPath);
        }

        public void reg(Type uiCls, string prefabPath)
        {
            if (m_uiCfgs.ContainsKey(uiCls))
            {
                throw new Exception("名为[" + uiCls.Name + "]的ui重复注册注册");
            }

            var uiCfg = new UICfg();
            uiCfg.prefabPath = prefabPath;
            
            m_uiCfgs.Add(uiCls, uiCfg);
        }

        // 展示界面，并将该界面置顶当前层级
        public void show<TUI>(IUIParam param = null, Action<IBaseUI> onShow = null) where TUI : IBaseUI
        {
            var type = typeof(TUI);
            show(type, param, onShow);
        }

        public void show(Type uiCls, IUIParam param = null, Action<IBaseUI> onShow = null)
        {
            Debug.Log("aktest 显示UI：" + uiCls.ToString());
            BaseUI baseUI = null;
            if (m_uis.ContainsKey(uiCls))
            {
                baseUI = m_uis[uiCls];

                if (baseUI == null)
                {
                    Debug.Log("aktest show " + uiCls.Name + " == null");
                    return;
                }
                
                if (!baseUI.gameObject.activeSelf)
                {
                    baseUI.gameObject.SetActive(true);
                    baseUI.onShow();
                }

                // 置顶
                baseUI.transform.SetAsLastSibling();
                
                if (onShow != null)
                {
                    onShow(baseUI);
                }
            }
            else
            {
                UICfg uiCfg = getUICfg(uiCls);
                if (uiCfg == null)
                {
                    throw new Exception("名为[" + uiCls.Name + "]的ui未注册");
                }
                
                // 占位
                m_uis.Add(uiCls, null);

                // 加载prefab
                ResMgr.getInstance().load<GameObject>(uiCfg.prefabPath, true, true, (obj) =>
                {
                    GameObject objUI = (GameObject)obj;
                    if (objUI == null)
                    {
                        throw new Exception("UI创建失败");
                    }
                
                    objUI.name = uiCls.Name;
            
                    // 如果没有挂载脚本，就自动挂载
                    baseUI = (BaseUI)KtUnity.addComponent(objUI, uiCls);
            
                    // 根据窗口所在层级展示
                    GameObject parentLayer = m_layers[(int)(baseUI.getUILayer())];
                    objUI.transform.SetParent(parentLayer.transform);
                    
                    // 窗口根节点默认撑满
                    KtUnity.stretchFull(objUI.GetComponent<RectTransform>());
                
                    EventMgr.getInstance().addEventListener(baseUI);
                    try
                    {
                        baseUI.onCreate(param);
                        baseUI.onShow();
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("界面创建失败[" + uiCls.Name + "]:" + e.Message);
                        Debug.LogError("atkest stack=>" + e.StackTrace);
                        throw e;
                    }
                    
                    Debug.Log("创建界面[" + uiCls.Name + "]成功");
                    m_uis[uiCls] = baseUI;

                    if (onShow != null)
                    {
                        onShow(baseUI);
                    }
                });
            }
        }
        
        // 隐藏界面（还在内存中）
        public void hide<T>()
        {
            hide(typeof(T));
        }

        public void hide(Type uiCls)
        {
            if (!m_uis.ContainsKey(uiCls) || m_uis[uiCls] == null)
            {
                return;
            }
            
            m_uis[uiCls].onHide();
            m_uis[uiCls].gameObject.SetActive(false);
        }

        // 销毁界面（不在内存中）
        public void destroy<T>()
        {
            destroy(typeof(T));
        }
        
        public void destroy(Type uiCls)
        {
            if (!m_uis.ContainsKey(uiCls) || m_uis[uiCls] == null)
            {
                return;
            }
            
            m_uis[uiCls].onHide();
            EventMgr.getInstance().removeEventListener(m_uis[uiCls]);
            KtUnity.destory(m_uis[uiCls].gameObject);
            m_uis.Remove(uiCls);
        }

        // 获取界面关联脚本（尽量使用事件系统代替该方法）
        public T getUI<T>() where T : BaseUI
        {
            return (T)getUI(typeof(T));
        }

        public BaseUI getUI(Type uiCls)
        {
            if (m_uis.ContainsKey(uiCls))
            {
                return m_uis[uiCls];
            }

            return null;
        }
        
        private UICfg getUICfg(Type uiCls)
        {
            if (!m_uiCfgs.ContainsKey(uiCls))
            {
                throw new Exception(uiCls.ToString() + " 界面没有注册");
            }
            
            return m_uiCfgs[uiCls];
        }
    }
}
