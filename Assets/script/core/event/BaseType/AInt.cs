﻿using UnityEngine;
using System.Collections;

namespace kf
{
    public class AInt : UnityEngine.Object
    {
        public int value;

        public static AInt makeAInt(int v)
        {
            AInt i = new AInt();
            i.value = v;
            return i;
        }
    }

}