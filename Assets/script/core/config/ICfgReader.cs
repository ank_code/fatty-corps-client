using System;
using System.Collections.Generic;
using kf;
using Newtonsoft.Json;

namespace script.core.config
{
    public interface ICfgReader
    {
        public List<T> read<T>(string path) where T : new();
        public void readAsync<T>(string path, Action<List<T>> callback) where T : new();
    }
}