namespace script.game.enums.item
{
    public enum EItemType
    {
        // 普通道具
        Nor,
        // 装备
        Equipment,
        // 碎片
        Fragment
    }
}