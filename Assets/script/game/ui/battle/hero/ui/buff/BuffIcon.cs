using System;
using ak;
using kf;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.battle
{
    // buff图标
    public class BuffIcon
    {
        private GameObject m_obj;
        private Image m_imgIcon;
        private int m_num;
        private TMP_Text m_txtNum;

        private const string PREFAB_PATH = "Assets/res/prefab/battle/buff/buffIcon.prefab";

        public void init(GameObject parent, Action callback)
        {
            ResMgr.getInstance().load<GameObject>(PREFAB_PATH, true, true, o =>
            {
                m_obj = (GameObject)o;
                m_obj.transform.SetParent(parent.transform);

                GameObject objImgIcon = KtUnity.findObj(m_obj, "imgIcon");
                m_imgIcon = objImgIcon.GetComponent<Image>();

                GameObject objTxtNum = KtUnity.findObj(m_obj, "txtNum");
                m_txtNum = objTxtNum.GetComponent<TMP_Text>();
                m_num = 0;
            });
        }

        public GameObject getObj()
        {
            return m_obj;
        }

        public void setBuff(int typeId)
        {
            string path = BuffUtil.getIconPath(typeId);
            ResMgr.getInstance().load<Sprite>(path, false, true, o =>
            {
                var sprite = (Sprite)o;
                m_imgIcon.sprite = sprite;
            });
            
            m_num = 1;
            m_obj.SetActive(true);
            m_obj.name = "buff_" + typeId;
            updateInfo();
        }

        public void add()
        {
            m_num++;
            updateInfo();
        }

        public void remove()
        {
            m_num--;
            updateInfo();
        }

        public int getNum()
        {
            return m_num;
        }

        private void updateInfo()
        {
            updateTxtNum();

            if (m_num <= 0)
            {
                m_obj.SetActive(false);
            }
        }

        private void updateTxtNum()
        {
            m_txtNum.text = m_num.ToString();
        }
    }
}