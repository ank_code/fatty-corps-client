using System;
using ak;
using kf;
using script.core.config;
using script.core.store;
using script.game.enums;
using script.game.mgr;
using script.game.net.http;
using script.game.net.msg.client.item;
using script.game.obj.config;
using script.game.ui;
using script.game.ui.comm;
using script.game.ui.home;
using TMPro;
using UnityEngine;
using Object = UnityEngine.Object;

public class LoginUI : GameUI
{
    public class Param : IUIParam
    {
        public string account;
        public string password;
    }

    private TMP_InputField m_inputAcc = null;
    private TMP_InputField m_inputPwd = null;
    
    public override void onCreate(IUIParam param)
    {
        var objBtnLogin = KtUnity.findObj(gameObject, "btnLogin");
        var objInputAcc = KtUnity.findObj(gameObject, "inputAcc");
        var objInputPwd = KtUnity.findObj(gameObject, "inputPwd");

        var inputAcc = objInputAcc.GetComponent<TMP_InputField>();
        var inputPwd = objInputPwd.GetComponent<TMP_InputField>();
        
#if UNITY_WEBGL
        KtUnity.addComponent<WXTMPInputHandler>(objInputAcc);
        KtUnity.addComponent<WXTMPInputHandler>(objInputPwd);
#endif

        m_inputAcc = inputAcc;
        m_inputPwd = inputPwd;

        if (param != null)
        {
            Param p = (Param)param;
            inputAcc.text = p.account;
            inputPwd.text = p.password;
            Debug.Log("aktest LoginUI acc=>" + p.account);
        }
        else
        {
            inputAcc.text = (string)StoreMgr.getInstance().get(StoreMgr.KEY_ACC);
            inputPwd.text = (string)StoreMgr.getInstance().get(StoreMgr.KEY_PWD);
        }

        var objImgBtnLoginBg = KtUnity.findObj(gameObject, "imgBtnLoginBg");
        KtUI.addListener(objImgBtnLoginBg, objBtnLogin, () =>
        {
            // enterHome();
            // UIMgr.getInstance().show<HomeUI>();
            // return;
            
            if (!UIUtil.verifyInputAndAlert(inputAcc.text, "账号不能为空"))
            {
                return;
            }
            
            if (!UIUtil.verifyInputAndAlert(inputPwd.text, "密码不能为空"))
            {
                return;
            }
            
            if (!UIUtil.accountRegexMatch(inputAcc.text))
            {
                AlertUtil.alertInfo("账号格式错误：只能输入a-z,0-9的字符，6-20位");
                return;
            }
            
            if (!UIUtil.passwordRegexMatch(inputPwd.text))
            {
                AlertUtil.alertInfo("密码格式错误：8-20位，至少包含一个字母&一个数字&一个特殊字符（@、$、!、%、*、? 或 &）");
                return;
            }
            
            AccountRequest.getInstance().login(inputAcc.text, inputPwd.text, (rep) =>
            {
                // AlertUtil.alertInfo("登陆成功");

                Debug.Log("登陆成功，token=>" + rep.token);
                sendEvent(EEvent.OnLoginSuccess, new []{rep.token, inputAcc.text, inputPwd.text});

                HomeUI.open();
                
                UIMgr.getInstance().destroy<LoginUI>();
                UIMgr.getInstance().destroy<LoginBgUI>();
            });
        });
        
        var objBtnReg = KtUnity.findObj(gameObject, "btnReg");
        KtUI.addListener(objBtnReg, () =>
        {
            UIMgr.getInstance().show<RegUI>();
        });
    }

    // public void reSet(string account, string password)
    // {
    //     m_inputAcc.text = account;
    //     m_inputPwd.text = password;
    // }

    public override void onEvent(int eId, object[] objs)
    {
        if ((EEvent)eId == EEvent.OnRegAccountSuccess)
        {
            m_inputAcc.text = (string)objs[0];
            m_inputPwd.text = (string)objs[1];
        } 
    }
}