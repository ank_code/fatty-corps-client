using System;
using System.Collections.Generic;
using kf;
using Newtonsoft.Json;
using script.game.enums;
using script.game.mgr;
using script.game.net.msg.server;
using script.game.obj.config;
using script.game.ui.battle.effect.anim;
using UnityEngine;

namespace script.game.ui.battle.record
{
    /**
     * 录像执行器
     */
    public class RecordInvoker : Singleton<RecordInvoker>
    {
        private List<RecElem> m_records = new List<RecElem>();
        private int m_curIndex = 0;

        public void init(List<RecElem> records)
        {
            if (m_records.Count > 0)
            {
                m_records.Clear();
            }
            
            m_records.AddRange(records);
            m_curIndex = 0;
        }

        public void next()
        {
            // 结束
            if (m_curIndex >= m_records.Count)
            {
                GameEvent.sendEvent(EEvent.OnBattleEnd);
                return;
            }

            if (m_curIndex == 0)
            {
                GameEvent.sendEvent(EEvent.OnBattleBegin);
            }

            RecElem recElem = m_records[m_curIndex];
            Debug.Log("播放录像索引：" + m_curIndex);
            Debug.Log("录像内容：" + JsonConvert.SerializeObject(recElem));
            m_curIndex++;

            switch (recElem.cnd)
            {
                case EBehaviorCondition.AfterRoundEnd:
                case EBehaviorCondition.SelfNorAtt:
                case EBehaviorCondition.SelfSkillAtt:
                case EBehaviorCondition.AfterSelfUnderAtt:
                case EBehaviorCondition.AfterSelfSkillAtt:
                case EBehaviorCondition.AfterSelfActive:
                    DisplayInvoker.getInstance().play(recElem);
                    break;
                default:
                    throw new Exception("暂时不支持的时机");
            }
            
            if (recElem.cnd.Equals(EBehaviorCondition.AfterRoundEnd))
            {
                GameEvent.sendEvent(EEvent.OnBattleRoundEnd);
            } 
            
            sleep(recElem.cnd);
        }

        private void sleep(EBehaviorCondition cnd)
        {
            // 英雄出手后、回合结束时，放缓节奏
            float sleep = 0.1f;
            switch (cnd)
            {
                case EBehaviorCondition.AfterSelfActive:
                    sleep = 0.5f;
                    break;
                case EBehaviorCondition.AfterRoundEnd:
                    sleep = 1f;
                    break;
                default:
                    return;
            }
            
            GameEvent.sendEvent(EEvent.OnBattleSleep, new object[]{sleep});
        }

        private void action(RecElem elem)
        {
            DisplayInvoker.getInstance().play(elem);
        }
    }
}