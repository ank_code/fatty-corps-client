using System;
using System.Collections.Generic;
using System.Data.Common;
using kf;
using script.core.config;
using script.game.obj.config;
using UnityEngine;


namespace script.game.mgr
{
    /**
     * 配置文件管理
     */
    public class CfgMgr : Singleton<CfgMgr>
    {
        private Dictionary<int, CfgBehaviorDisplay> m_behaviorDisplays = new Dictionary<int, CfgBehaviorDisplay>();
        private Dictionary<int, CfgHero> m_heroes = new Dictionary<int, CfgHero>();
        private Dictionary<int, CfgItem> m_items = new Dictionary<int, CfgItem>();
        
        private Dictionary<int, CfgSkillGroup> m_skillGroups = new Dictionary<int, CfgSkillGroup>();
        private Dictionary<int, CfgSkill> m_skills = new Dictionary<int, CfgSkill>();
        private Dictionary<int, CfgStar> m_stars = new Dictionary<int, CfgStar>();
        private Dictionary<int, CfgMainGuanqia> m_mainGQs = new Dictionary<int, CfgMainGuanqia>();
        private Dictionary<int, CfgPlayerExp> m_playerExps = new Dictionary<int, CfgPlayerExp>();
        private Dictionary<int, CfgHeroExp> m_heroExps = new Dictionary<int, CfgHeroExp>();

        private bool m_init = false;
        private Action m_complete = null;
        private int m_cfgNum = 0;
        private bool m_loadCfgFinish = false;
        
        public void init(Action complete = null)
        {
            if (m_init)
            {
                if (complete != null)
                {
                    complete();
                }
                
                return;
            }

            m_complete = complete;
            
            initCfg<CfgBehaviorDisplay>("Assets/res/config/config_behavior_display.csv", m_behaviorDisplays);
            initCfg<CfgHero>("Assets/res/config/config_hero.csv", m_heroes);
            initCfg<CfgItem>("Assets/res/config/config_item.csv", m_items);
            
            initCfg<CfgSkillGroup>("Assets/res/config/config_skill_group.csv", m_skillGroups);
            initCfg<CfgSkill>("Assets/res/config/config_skill.csv", m_skills);
            initCfg<CfgStar>("Assets/res/config/config_star.csv", m_stars);
            initCfg<CfgMainGuanqia>("Assets/res/config/config_main_guanqia.csv", m_mainGQs);
            initCfg<CfgPlayerExp>("Assets/res/config/config_player_exp.csv", m_playerExps);
            initCfg<CfgHeroExp>("Assets/res/config/config_hero_exp.csv", m_heroExps);

            m_init = true;
        }

        private void initCfg<T>(string path, Dictionary<int, T> dic) where T : BaseCfg, new()
        {
            m_cfgNum++;
            CfgAdp.getInstance().readAsync<T>(path, ECfgType.Csv, (list) =>
            {
                dic.Clear();
                
                for (var i = 0; i < list.Count; i++)
                {
                    dic.Add(list[i].id, list[i]);
                }
                
                Debug.Log("aktest load cfg " + path + " data size=>" + dic.Count);

                m_cfgNum--;
                if (m_cfgNum == 0)
                {
                    if (m_complete != null)
                    {
                        m_loadCfgFinish = true;
                        m_complete();
                    }
                }
            });
        }

        public CfgBehaviorDisplay getBehaviorDisplay(int id)
        {
            if (m_behaviorDisplays.ContainsKey(id))
            {
                return m_behaviorDisplays[id];
            }

            return null;
        }

        public CfgHero getHero(int id)
        {
            if (!m_heroes.ContainsKey(id))
            {
                throw new Exception("无法找到hero配置 id=>" + id);
            }
            
            return m_heroes[id];
        }

        public CfgItem getItem(int id)
        {
            return m_items[id];
        }
        
        public CfgSkillGroup getSkillGroup(int id)
        {
            return m_skillGroups[id];
        }
        
        public CfgSkill getSkill(int id)
        {
            return m_skills[id];
        }
        
        public CfgStar getStar(int star)
        {
            foreach (var keyValuePair in m_stars)
            {
                if (keyValuePair.Value.star == star)
                {
                    return keyValuePair.Value;
                }
            }

            return null;
        }
        
        public CfgMainGuanqia getMainGQ(int id)
        {
            return m_mainGQs[id];
        }

        public int getMainGQSize()
        {
            return m_mainGQs.Count;
        }
        
        public CfgPlayerExp getPlayerExp(int lv)
        {
            if (m_playerExps == null)
            {
                Debug.Log("aktest m_playerExps == null");
            }

            Debug.Log("aktest m_playerExps.count=>" + m_playerExps.Count + " lv=>" + lv);
            return m_playerExps[lv];
        }
        
        public CfgHeroExp getHeroExp(int lv)
        {
            return m_heroExps[lv];
        }
    }
}