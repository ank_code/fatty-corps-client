using System.Reflection;
using ak;
using kf;
using kf.hotpUpdate.state;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using YooAsset;

public class HotUpdateUI : BaseUI
{
    private Slider m_sliderProgress;
    private TMP_Text m_txtInfo;
    private TMP_Text m_txtDetail;
    
    void Start()
    {
        var objProgress = KtUnity.findObj(gameObject, "progress");
        var objTxtInfo = KtUnity.findObj(gameObject, "txtInfo");
        var objTxtDetail = KtUnity.findObj(gameObject, "txtDetail");
        
        m_sliderProgress = objProgress.GetComponent<Slider>();
        m_sliderProgress.value = 1.0f;
        m_sliderProgress.interactable = false;
        
        m_txtInfo = objTxtInfo.GetComponent<TMP_Text>();
        m_txtInfo.text = "-/-";

        m_txtDetail = objTxtDetail.GetComponent<TMP_Text>();
        m_txtDetail.text = "正在检查版本信息...";
        
        HotUpdate.getInstance().setOnStateChange(onHotUpdateStateChg);
        HotUpdate.getInstance().setOnDownloadProgress(onDownloadProgress);
        HotUpdate.getInstance().next();
    }

    private void onHotUpdateStateChg(IFsmState newState)
    {
        if (newState is UpdateVersion)
        {
            m_txtDetail.text = "正在更新版本文件...";
        } else if (newState is UpdateManifest)
        {
            m_txtDetail.text = "正在更新资源信息文件...";
        } else if (newState is CreateDownloader)
        {
            m_txtDetail.text = "正在比对版本...";
        } else if (newState is WaitForConfirm)
        {
            // 弹框告知用户有多少资源要下载，用户确认后开始下载
            m_txtDetail.text = "";
            m_txtInfo.text = "等待玩家确认...";
            AlertWnd.Param param = new AlertWnd.Param();
            var totalBytes = HotUpdate.getInstance().getDownloadTotalBytes();
            param.info = "发现资源更新，大小：" + KtByteConverter.convertBytesToMB(totalBytes) + "MB，是否下载？";
            param.type = AlertWnd.EType.Ok;
            param.okCallback = () =>
            {
                HotUpdate.getInstance().next();
            };
            
            // UIMgr.getInstance().show<AlertWnd>(param);
            HotUpdate.getInstance().next();
        } else if (newState is DownloadFiles)
        {
            m_txtDetail.text = "正在下载文件...";
            m_txtInfo.text = "0%";
            m_sliderProgress.value = 0.0f;
        } else if (newState is InitHybrid)
        {
            m_txtDetail.text = "正在加载脚本...";
        } else if (newState is CleanCache)
        {
            m_txtDetail.text = "正在清理无效文件...";
        } else if (newState is Error)
        {
            var err = HotUpdate.getInstance().getErr();
            // 弹框报错
            Debug.LogError("hotUpdate error:" + err);
            
            AlertWnd.Param param = new AlertWnd.Param();
            param.info = "错误：" + err;
            UIMgr.getInstance().show<AlertWnd>(param);
        } else if (newState is HotUpdateOver)
        {
            m_txtDetail.text = "更新完成";
            m_sliderProgress.value = 1.0f;
            
            // AlertWnd.Param param = new AlertWnd.Param();
            // param.info = "更新完成";
            // param.okCallback = () =>
            // {
            //     enterGame();
            //     UIMgr.getInstance().destroy<HotUpdateUI>();
            // };
            //
            // UIMgr.getInstance().show<AlertWnd>(param);
            
            enterGame();
            UIMgr.getInstance().destroy<HotUpdateUI>();
            HotUpdate.getInstance().Dispose();
        }
        
        Debug.Log("hotUpdate info:" + m_txtInfo.text);
    }

    private void onDownloadProgress(int totalDownloadCount, int currentDownloadCount, long totalDownloadBytes, long currentDownloadBytes)
    {
        Debug.Log("hotUpdate totalDownloadCount:" + totalDownloadCount + ",currentDownloadCount:" + currentDownloadCount +
                  "，totalDownloadBytes:" + totalDownloadBytes + ",currentDownloadBytes:" + currentDownloadBytes);
        m_sliderProgress.value = (float)((decimal)currentDownloadBytes/totalDownloadBytes);
        int per = Mathf.RoundToInt(m_sliderProgress.value * 10000) / 100;
        m_txtInfo.text = per + "%";
        // Debug.Log("aktest m_sliderProgress.value=>" + m_sliderProgress.value);
    }
    
    private void enterGame()
    {
        Assembly ass = HotUpdate.getInstance().getHotUpdateAssembly();
        var type = ass.GetType(GlobalConst.HOTUPDATE_ENTRY_CLASS);
        if (type == null)
        {
            Debug.LogError("无法找到热更新入口类：" + GlobalConst.HOTUPDATE_ENTRY_CLASS);
            return;
        }
        
        StructBuilder.getInstance().getLogicObj().AddComponent(type);
    }
}