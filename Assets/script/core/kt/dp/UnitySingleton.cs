using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kf
{
    public class UnitySingleton<T> : MonoBehaviour where T : UnitySingleton<T>
    {
        public static T instance;

        public static T getInstance()
        {
            return instance;
        }

        protected void Awake()
        {
            if (instance == null)
            {
                instance = (T)this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}