using System;
using kf;
using script.game.enums;

namespace script.game.mgr
{
    public class GameEvent
    {
        public static void sendEvent(EEvent eId, Object[] objs = null, EventMgr.scope onBefore = null, EventMgr.scope onComplete = null)
        {
            EventMgr.getInstance().sendEvent((int)eId, objs, onBefore, onComplete);
        }
    }
}