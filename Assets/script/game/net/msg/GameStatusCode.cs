﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kf
{
    public class GameStatusCode
    {
        public const int OK = 200;
        // 游戏token错误
        public const int TOKEN_ERROR = 202;
        // 未登陆
        public const int NOT_LOGIN_ERROR = 204;
        // 账号token错误
        public const int VERIFY_ACC_TOKEN_ERROR = 1100;
        public const int PRODUCT_LIMIT_ERROR = 2002;
    }
}