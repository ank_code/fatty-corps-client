using System;
using System.Collections;
using System.IO;
using UnityEngine;
using YooAsset;

namespace kf.hotpUpdate.state
{
    // 初始化yooAsset
    public class Init : FsmState
    {
        public override void onEnter()
        {
            HotUpdate.HotUpdateMono mono = (HotUpdate.HotUpdateMono)getMachine().getBlackboard(HotUpdate.BBK_MONOBEHAVIOUR);
            mono.startCoroutine(init());
        }

        IEnumerator init()
        {
	        yield return null;
	        
            var playMode = (EPlayMode)getMachine().getBlackboard(HotUpdate.BBK_PLAY_MODE);
            var packageName = (string)getMachine().getBlackboard(HotUpdate.BBK_YOOASSET_PACKAGE_NAME);
            var host = (string)getMachine().getBlackboard(HotUpdate.BBK_HOST);
            
            // 初始化资源系统
            YooAssets.Initialize();
            
            // 创建资源包裹类
            var package = YooAssets.TryGetPackage(packageName);
            if (package == null)
            {
                package = YooAssets.CreatePackage(packageName);
            }
            
            YooAssets.SetDefaultPackage(package);

            // 编辑器下的模拟模式
			InitializationOperation initializationOperation = null;
			if (playMode == EPlayMode.EditorSimulateMode)
			{
				var createParameters = new EditorSimulateModeParameters();
				createParameters.SimulateManifestFilePath = EditorSimulateModeHelper.SimulateBuild(packageName);
				initializationOperation = package.InitializeAsync(createParameters);
			}

			// 单机运行模式
			if (playMode == EPlayMode.OfflinePlayMode)
			{
				var createParameters = new OfflinePlayModeParameters();
				createParameters.DecryptionServices = new GameDecryptionServices();
				initializationOperation = package.InitializeAsync(createParameters);
			}

			// 联机运行模式
			if (playMode == EPlayMode.HostPlayMode)
			{
				string defaultHostServer = host;
				string fallbackHostServer = host;
				var createParameters = new HostPlayModeParameters();
				createParameters.DecryptionServices = new GameDecryptionServices();
				createParameters.BuildinQueryServices = new GameQueryServices();
				createParameters.DeliveryQueryServices = new DefaultDeliveryQueryServices();
				createParameters.RemoteServices = new RemoteServices(defaultHostServer, fallbackHostServer);
				initializationOperation = package.InitializeAsync(createParameters);
			}

			// WebGL运行模式
			if (playMode == EPlayMode.WebPlayMode)
			{
				string defaultHostServer = host;
				string fallbackHostServer = host;
				var createParameters = new WebPlayModeParameters();
				createParameters.DecryptionServices = new GameDecryptionServices();
				createParameters.BuildinQueryServices = new GameQueryServices();
				createParameters.RemoteServices = new RemoteServices(defaultHostServer, fallbackHostServer);
				initializationOperation = package.InitializeAsync(createParameters);
				YooAssets.SetCacheSystemDisableCacheOnWebGL();
			}

			yield return initializationOperation;

			// 如果初始化失败弹出提示界面
			if (initializationOperation.Status != EOperationStatus.Succeed)
			{
				getMachine().setBlackboard(HotUpdate.BBK_ERR, initializationOperation.Error);
				getMachine().setState<Error>();
			}
			else
			{
				var version = initializationOperation.PackageVersion;
				Debug.Log($"Init resource package version : {version}");
				getMachine().setState<InitOver>();
			}
        }
        
        
        /// <summary>
        /// 远端资源地址查询服务类
        /// </summary>
        private class RemoteServices : IRemoteServices
        {
            private readonly string _defaultHostServer;
            private readonly string _fallbackHostServer;

            public RemoteServices(string defaultHostServer, string fallbackHostServer)
            {
                _defaultHostServer = defaultHostServer;
                _fallbackHostServer = fallbackHostServer;
            }
            string IRemoteServices.GetRemoteMainURL(string fileName)
            {
                return $"{_defaultHostServer}/{fileName}";
            }
            string IRemoteServices.GetRemoteFallbackURL(string fileName)
            {
                return $"{_fallbackHostServer}/{fileName}";
            }
        }

        /// <summary>
        /// 资源文件解密服务类
        /// </summary>
        private class GameDecryptionServices : IDecryptionServices
        {
            public ulong LoadFromFileOffset(DecryptFileInfo fileInfo)
            {
                return 32;
            }

            public byte[] LoadFromMemory(DecryptFileInfo fileInfo)
            {
                throw new NotImplementedException();
            }

            public Stream LoadFromStream(DecryptFileInfo fileInfo)
            {
                BundleStream bundleStream = new BundleStream(fileInfo.FilePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                return bundleStream;
            }

            public uint GetManagedReadBufferSize()
            {
                return 1024;
            }
        }

        /// <summary>
        /// 默认的分发资源查询服务类
        /// </summary>
        private class DefaultDeliveryQueryServices : IDeliveryQueryServices
        {
	        public DeliveryFileInfo GetDeliveryFileInfo(string packageName, string fileName)
	        {
		        throw new NotImplementedException();
	        }
	        public bool QueryDeliveryFiles(string packageName, string fileName)
	        {
		        return false;
	        }
        }
        
        public class BundleStream : FileStream
        {
	        public const byte KEY = 64;
    
	        public BundleStream(string path, FileMode mode, FileAccess access, FileShare share) : base(path, mode, access, share)
	        {
	        }
	        public BundleStream(string path, FileMode mode) : base(path, mode)
	        {
	        }

	        public override int Read(byte[] array, int offset, int count)
	        {
		        var index = base.Read(array, offset, count);
		        for (int i = 0; i < array.Length; i++)
		        {
			        array[i] ^= KEY;
		        }
		        return index;
	        }
        }
    }
}