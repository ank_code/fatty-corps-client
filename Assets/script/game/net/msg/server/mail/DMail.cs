namespace script.game.net.msg.client.hero
{
    public class DMail
    {
        public int id;
        public string title;
        public string content;
        public string attachment;
        public bool read;
        public bool getAttachment;
        public int time;
    }
}