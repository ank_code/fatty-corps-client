using System.Text.RegularExpressions;

namespace script.game.ui.comm
{
    public class UIUtil
    {
        public static bool verifyInputAndAlert(string input, string info)
        {
            if (string.IsNullOrEmpty(input))
            {
                AlertUtil.alertInfo(info);
                return false;
            }

            return true;
        }

        public static bool accountRegexMatch(string input)
        {
            string pattern = @"^[A-Za-z0-9]{5,20}$";
            return Regex.IsMatch(input, pattern);
        }

        public static bool passwordRegexMatch(string input)
        {
            string pattern = @"^(?=.*[a-zA-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,16}$";
            return Regex.IsMatch(input, pattern);
        }
    }
}