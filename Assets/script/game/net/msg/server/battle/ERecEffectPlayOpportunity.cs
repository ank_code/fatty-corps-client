namespace script.game.net.msg.server
{
    public enum ERecEffectPlayOpportunity
    {
        // 无
        None,
        // 目标播放受击动画前
        BeforeTargetUnderAttack,
        // 目标播放受击动画后
        AfterTargetUnderAttack
    }
}