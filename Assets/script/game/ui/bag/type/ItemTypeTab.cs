using System;
using ak;
using kf;
using script.game.enums;
using script.game.enums.item;
using script.game.mgr;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.bag.type
{
    public class ItemTypeTab : ABaseTab
    {
        private GameObject m_obj;
        private GameObject m_objImgChoosed;
        private GameObject m_objTxtName;
        private TMP_Text m_txtName;

        private EItemType m_type;
        
        public void init(GameObject obj, EItemType type)
        {
            m_obj = obj;
            m_type = type;

            m_objImgChoosed = KtUnity.findObj(m_obj, "imgChoosed");
            KtUI.addListener(m_obj, () =>
            {
                base.onClickTab();
            });

            m_objTxtName = KtUnity.findObj(m_obj, "txtName");
            m_txtName = m_objTxtName.GetComponent<TMP_Text>();
            m_txtName.text = ItemTypeUtil.getTypeName(m_type);

            this.refresh();
        }
        
        public override void onFocus()
        {
            m_objImgChoosed.SetActive(true);
            m_txtName.color = Color.white;
            
            // todo: 分类刷新物品内容
            GameEvent.sendEvent(EEvent.OnBagSwitchType, new object[]{m_type});
        }

        public override void onLoseFocus()
        {
            m_objImgChoosed.SetActive(false);
            m_txtName.color = Color.gray;
        }
    }
}