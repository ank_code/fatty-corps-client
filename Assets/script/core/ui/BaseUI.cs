using UnityEngine;

namespace kf
{
    public abstract class BaseUI : MonoBehaviour, IBaseUI, IEventListener
    {
        public virtual void onCreate(IUIParam param)
        {
        }

        public virtual void onShow()
        {
        }

        public virtual void onHide()
        {
        }

        public virtual void close()
        {
            UIMgr.getInstance().destroy(GetType());
        }

        public virtual EUILayer getUILayer()
        {
            return EUILayer.Nor;
        }

        public virtual void onEvent(int eId, object[] objs)
        {
        }
    }
}