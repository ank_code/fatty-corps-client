using script.game.enums.item;

namespace script.game.obj.config
{
    public class CfgItem : BaseCfg
    {
        public string name;
        public string intro;
        public EItemType type;
        public EItemSubType sub_type;
        public int quality;
        public int sale_money;
        public string prop;
    }
}