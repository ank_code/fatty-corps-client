using System;
using System.Collections.Generic;
using ak;
using script.game.enums;
using script.game.mgr;
using script.game.net.msg.server.hero;
using UnityEngine;

namespace script.game.ui.lineup
{
    public class LineupTeam
    {
        private GameObject m_obj;
        private GameObject m_dragFront;

        private List<LineupPos> m_poses = new List<LineupPos>();
        
        public void init(GameObject obj, List<HeroShort> heroList, bool left)
        {
            this.m_obj = obj;
            m_dragFront = KtUnity.findObj(obj, "dragFront");
            
            for (int i = 0; i < GameConst.MAX_TEAM_NUM; i++)
            {
                int no = i + 1;
                string objPosName = "pos" + no;
                GameObject objPos = KtUnity.findObj(obj, objPosName);
                LineupPos pos = new LineupPos();
                pos.init(no, objPos, left);
                pos.setDragFront(m_dragFront);

                if (heroList != null && i >= 0 && i < heroList.Count && heroList[i]!=null && !heroList[i].isEmpty())
                {
                    pos.setHero(heroList[i]);
                }
                else
                {
                    pos.clear();
                }
                
                m_poses.Add(pos);
            }
        }

        public List<int> getLineup()
        {
            List<int> lineup = new List<int>();
            for (var i = 0; i < m_poses.Count; i++)
            {
                lineup.Add(m_poses[i].getHeroShort().id);
            }

            return lineup;
        }

        public bool isFull()
        {
            for (var i = 0; i < m_poses.Count; i++)
            {
                if (m_poses[i].isEmpty())
                {
                    return false;
                }
            }

            return true;
        }

        public bool add(HeroShort heroShort)
        {
            // 找一个空的位置放新的英雄
            for (var i = 0; i < m_poses.Count; i++)
            {
                if (m_poses[i].isEmpty())
                {
                    m_poses[i].setHero(heroShort);
                    return true;
                }
            }
            
            return false;
        }

        public void remove(int heroId)
        {
            for (var i = 0; i < m_poses.Count; i++)
            {
                if (m_poses[i].getHeroShort().id == heroId)
                {
                    m_poses[i].clear();
                }
            }
        }

        public void clear()
        {
            for (var i = 0; i < m_poses.Count; i++)
            {
                if (!m_poses[i].isEmpty())
                {
                    GameEvent.sendEvent(EEvent.OnLineupHeroItemClick, new object[] {m_poses[i].getHeroShort(), false});
                }
            }
        }

        public void onHeroDragEnd(int fromNo, Vector3 pos)
        {
            int fromIdx = fromNo - 1;
            var destPos = getLPosByPos(pos, fromNo);
            
            // 无论是否成功，拖拽后的层级关系先还原
            m_poses[fromIdx].toOrg();
            
            if (destPos == null || destPos.getNo() == fromNo)
            {
                return;
            }
            
            // 交换/换位置
            
            LineupPos fromPos = m_poses[fromIdx];
            HeroShort fromHeroShort = fromPos.getHeroShort();
            fromPos.setHero(destPos.getHeroShort());
            destPos.setHero(fromHeroShort);
        }

        public LineupPos getLPosByPos(Vector3 pos, int withoutNo = -1)
        {
            for (var i = 0; i < m_poses.Count; i++)
            {
                int no = i + 1;
                if (withoutNo == no)
                {
                    continue;
                }
                
                if (m_poses[i].isPosInSwitchArea(pos))
                {
                    return m_poses[i];
                }
            }

            return null;
        }
    }
}