namespace script.game.net.msg.server
{
    public enum EBattleResult
    {
        Win,
        Fail,
        Draw
    }
}