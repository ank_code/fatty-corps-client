

using System;
using System.Linq.Expressions;
using kf;

public class AlertUtil
{
    public static void alertInfo(string info, Action okCallback = null)
    {
        AlertWnd.Param param = new AlertWnd.Param();
        param.type = AlertWnd.EType.Ok;
        param.info = info;
        param.okCallback = okCallback;
        UIMgr.getInstance().show<AlertWnd>(param);
    }    
 
    public static void alertOkCancel(string info, Action okCallback = null)
    {
        AlertWnd.Param param = new AlertWnd.Param();
        param.type = AlertWnd.EType.OkCancel;
        param.info = info;
        param.okCallback = okCallback;
        UIMgr.getInstance().show<AlertWnd>(param);
    }   
}