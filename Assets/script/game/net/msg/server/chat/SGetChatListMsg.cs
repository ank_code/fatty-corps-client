using System.Collections.Generic;

namespace script.game.net.msg.server.chat
{
    public class SGetChatListMsg
    {
        public List<ChatInfo> chatInfoList;
    }
}