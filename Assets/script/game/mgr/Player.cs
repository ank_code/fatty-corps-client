using System.Collections.Generic;
using kf;
using script.game.enums;
using script.game.net.http;
using script.game.net.msg.server;

namespace script.game.mgr
{
    public class Player : Singleton<Player>, IEventListener
    {
        public SGetInfo info;

        public void init()
        {
            EventMgr.getInstance().addEventListener(this);
        }

        public void onEvent(int eId, object[] objs)
        {
            EEvent e = (EEvent)eId;
            switch (e)
            {
                case EEvent.OnSetLineupSuccess:
                {
                    info.defLineup = (List<int>)objs[0];
                    break;
                }
            }
        }

        public void setLv(int newLv)
        {
            if (newLv == this.info.lv)
            {
                return;
            }
            
            this.info.lv = newLv;
            EventMgr.getInstance().sendEvent((int)EEvent.OnPlayerLvChanged, new object[]{newLv});
        }
        
        public void setExp(int newExp)
        {
            if (newExp == this.info.exp)
            {
                return;
            }
            
            this.info.exp = newExp;
            EventMgr.getInstance().sendEvent((int)EEvent.OnPlayerExpChanged, new object[]{newExp});
        }
        
        public void passGuanqia()
        {
            info.guanqia += 1;
            int maxGQ = CfgMgr.getInstance().getMainGQSize();
            
            if (info.guanqia > maxGQ)
            {
                info.guanqia = maxGQ + 1;
            }
        }

        public bool clearance()
        {
            return info.guanqia > CfgMgr.getInstance().getMainGQSize();
        }

        public void refresh()
        {
            GameRequest.getInstance().getCacheInfo((rep) =>
            {
                info = rep;
                
                EventMgr.getInstance().sendEvent((int)EEvent.OnPlayerLvChanged, new object[]{info.lv});
                EventMgr.getInstance().sendEvent((int)EEvent.OnPlayerExpChanged, new object[]{info.exp});
                EventMgr.getInstance().sendEvent((int)EEvent.OnPlayerNameChanged, new object[]{info.nickname});
                EventMgr.getInstance().sendEvent((int)EEvent.OnPlayerCoinChanged, new object[]{info.coin});
                EventMgr.getInstance().sendEvent((int)EEvent.OnPlayerMoneyChanged, new object[]{info.money});
                EventMgr.getInstance().sendEvent((int)EEvent.OnPlayerCombatChanged, new object[]{info.combatVal});
            });
        }
    }
}