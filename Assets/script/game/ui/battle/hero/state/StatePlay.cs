using kf;
using kf.anim;

namespace script.game.ui.battle.state
{
    public class StatePlay : FsmState
    {
        public override void onEnter()
        {
            // 队列完成后再执行下一次的队列
            next();
        }

        public void next()
        {
            PlayCmdQueue queue = (PlayCmdQueue)getBlackboard(BattleHero.BB_CMD_QUEUE);
            SpineAnim sa = (SpineAnim)getBlackboard(BattleHero.BB_SPINE_ANIM);
            BattleHero hero = (BattleHero)getBlackboard(BattleHero.BB_BATTLE_HERO);
            
            if (queue.empty())
            {
                hero.onStatePlayComplete();
                return;
            }
            
            var playCmd = queue.pop();
            if (playCmd is PlayCmdQueue.PlayAnimCmd)
            {
                PlayCmdQueue.PlayAnimCmd cmd = (PlayCmdQueue.PlayAnimCmd)playCmd;
                sa.playOnce(cmd.animName, (SpineAnim.CallbackEvent ce)  =>
                {
                    if (ce.e.Equals(SpineAnim.ECallBackEvent.Complete))
                    {
                        // 没有关键帧，播放结束当做关键帧
                        if (!ce.playedKeyFrame)
                        {
                            if (cmd.keyFrameCallback != null)
                            {
                                cmd.keyFrameCallback();
                            }
                        }
                        
                        // 执行下个动作
                        next();
                    }
                    else
                    {
                        if (cmd.keyFrameCallback != null)
                        {
                            cmd.keyFrameCallback();
                        }
                    }
                });
            } else if (playCmd is PlayCmdQueue.PlayIdleCmd)
            {
                sa.playLoop(BattleHero.ANIM_NAME_IDLE);
                next();
            }
            else if (playCmd is PlayCmdQueue.PlayMoveToCmd)
            {
                PlayCmdQueue.PlayMoveToCmd cmd = (PlayCmdQueue.PlayMoveToCmd)playCmd;
                hero.moveTo(cmd.pos, () =>
                {
                    next();
                });
                
            } else if (playCmd is PlayCmdQueue.PlayMoveToOrgCmd)
            {
                hero.reset(() =>
                {
                    next();
                });
            }
        }
    }
}