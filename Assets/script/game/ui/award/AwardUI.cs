using System.Collections.Generic;
using ak;
using kf;
using script.game.net.msg.server.item;
using script.game.ui.comm.comp;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.award
{
    public class AwardUI : GameUI
    {
        public class Param : IUIParam
        {
            public List<DItem> items;
        }
        
        public override void onCreate(IUIParam param)
        {
            base.onCreate(param);
            var objClickArea = KtUnity.findObj(gameObject, "clickArea");
            KtUI.addListener(objClickArea, () =>
            {
                this.close();
            });
            
            Param p = (Param)param;
            if (p.items == null || p.items.Count == 0)
            {
                Debug.Log("aktest error AwardUI item 为空");
                return;
            }
            
            var objAwards = KtUnity.findObj(gameObject, "awards");
            for (var i = 0; i < p.items.Count; i++)
            {
                ItemAward award = new ItemAward();
                award.init(objAwards, p.items[i].typeId, p.items[i].num, false);
            }
            
            GameObject objSvItems = KtUnity.findObj(gameObject, "svItems");
            GameObject objContent = KtUnity.findObj(objSvItems, "Content");
            var contentRT = objContent.GetComponent<RectTransform>();
            var awardsRT = objAwards.GetComponent<RectTransform>();
            
            LayoutRebuilder.ForceRebuildLayoutImmediate(awardsRT);
            
            contentRT.sizeDelta = new Vector2(awardsRT.rect.width, awardsRT.rect.height);
            contentRT.localPosition = Vector3.zero;
        }
    }
}