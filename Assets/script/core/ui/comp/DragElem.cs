using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace kf
{
    /**
     * 可拖拽移动的控件，将该脚本挂载到控件上
     */
    public class DragElem : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        Vector3 m_offsetPos; //存储按下鼠标时的图片-鼠标位置差
        private Image m_imgDrag;

        private onEvent m_onBeginDrag;
        private onEvent m_onDrag;
        private onEvent m_onEndDrag;

        // private GameObject m_objOrgParent;
        // private GameObject m_objDragFront;

        public delegate void onEvent(PointerEventData eventData);

        public void init()
        {
            m_imgDrag = gameObject.GetComponent<Image>();

            // m_objDragFront = objDragFront;
            // m_objOrgParent = gameObject.transform.parent.gameObject;
        }

        public void addListener(onEvent onBeginDrag = null, onEvent onDrag = null, onEvent onEndDrag = null)
        {
            m_onBeginDrag = onBeginDrag;
            m_onDrag = onDrag;
            m_onEndDrag = onEndDrag;
        }

        /// <summary>
        /// 开始拖拽的时候
        /// </summary>
        /// <param name="eventData"></param>
        public void OnBeginDrag(PointerEventData eventData)
        {
            m_imgDrag.raycastTarget = false;
            // m_offsetPos = Camera.main.WorldToScreenPoint(gameObject.GetComponent<RectTransform>().position) - Input.mousePosition;
            m_offsetPos = Camera.main.WorldToScreenPoint(gameObject.transform.position) - Input.mousePosition;
            
            if (m_onBeginDrag != null) {
                m_onBeginDrag(eventData);
            }
        }
        /// <summary>
        /// 拖拽中
        /// </summary>
        /// <param name="eventData"></param>
        public void OnDrag(PointerEventData eventData)
        {
            updatePos();

            if (m_onDrag != null)
            {
                m_onDrag(eventData);
            }
        }
        /// <summary>
        /// 拖拽结束
        /// </summary>
        /// <param name="eventData"></param>
        public void OnEndDrag(PointerEventData eventData)
        {
            updatePos();
            m_imgDrag.raycastTarget = true;
            
            if (m_onEndDrag != null)
            {
                m_onEndDrag(eventData);
            }
        }

        private void updatePos()
        {
            transform.position = Camera.main.ScreenToWorldPoint(
                new Vector3(Mathf.Clamp(Input.mousePosition.x, 0, Screen.width),
                    Mathf.Clamp(Input.mousePosition.y, 0, Screen.height), 0) + m_offsetPos);
        }
    }
}