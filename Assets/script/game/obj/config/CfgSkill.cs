using System;
using script.game.enums;

namespace script.game.obj.config
{
    public class CfgSkill : BaseCfg
    {
        public string name;
        public string intro;
        public string icon;
        public int lv;
        public ESkillType type;
        public string targetType;
        private string behaviorIds; // 技能行为列表（behavior表中id） 包含触发时机，触发结果，不可为空 可包含多种行为，依次执行
        private int unlockStar; // 解锁星级
        private int unlockLv;
    }
}