

using System;
using ak;
using DG.Tweening;
using kf;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

// 由于该界面要在YooAsset初始化完成前展示，所以修改为不依赖UIMgr也就是不使用yooAsset加载资源的方式
public class LogoUI
{
    private GameObject m_obj;
    private VideoPlayer m_videoPlayer;
    private Action m_callback;
    
    private const string PREFAB_PATH = "prefab/logoUI";
    private const float FADE_IN_TIME = 1.0f;
    private const float FADE_OUT_TIME = 1.0f;
    
    public void init(GameObject parent, Action callback)
    {
        string mp4FilePath = Application.streamingAssetsPath + "/logo.mp4";
        
        // Handheld.PlayFullScreenMovie("logo.mp4", Color.black, FullScreenMovieControlMode.Hidden);
        
        Debug.Log("aktest 显示logoUI");
        m_obj = ResMgr.getInstance().loadFromResources<GameObject>(PREFAB_PATH, true);
        m_obj.transform.SetParent(parent.transform);
        KtUnity.stretchFull(m_obj.GetComponent<RectTransform>());

        Camera.main.backgroundColor = Color.white;

        var objImgLogo = KtUnity.findObj(m_obj, "imgLogo");
        var imgLogo = objImgLogo.GetComponent<Image>();
        imgLogo.color = new Color(imgLogo.color.r, imgLogo.color.g, imgLogo.color.b, 0.0f);
        fadeFun(imgLogo, FADE_IN_TIME, 1.0f, () =>
        {
            fadeFun(imgLogo, FADE_OUT_TIME, 0.0f, () =>
            {
                callback();
            });
        });

        // videoPlayer在android环境播放异常
        // var objVideo = KtUnity.findObj(m_obj, "video");
        // m_videoPlayer = objVideo.GetComponent<VideoPlayer>();
        // m_videoPlayer.source = VideoSource.Url;
        // m_videoPlayer.url = mp4FilePath;
        //
        // m_videoPlayer.loopPointReached += onVideoCompleteCore;
        // m_callback = callback;

        // 播放视频，todo: Resources下的mp4文件，可加载，但play时报错Cannot read file.
        // var objVideo = KtUnity.findObj(m_obj, "video");
        // var compVideo = KtUnity.addComponent<Video>(objVideo);
        // var videoClip = ResMgr.getInstance().loadFromResources<VideoClip>("video/logo");
        // compVideo.init(GlobalConst.DES_SCREEN_WIDTH, GlobalConst.DES_SCREEN_HEIGTH, videoClip);
        // compVideo.addCompleteListener(() =>
        // {
        //     callback();
        // });
        //
        // compVideo.play();
    }

    private void fadeFun(Image img, float time, float endA, Action onComplete)
    {
        DOTween.To(() => img.color, value => img.color = value,
            new Color(img.color.r, img.color.g, img.color.b, endA), time).OnComplete(() =>
        {
            
            onComplete();
        });
    }
    
    private void onVideoCompleteCore(VideoPlayer source)
    {
        if (m_callback != null)
        {
            m_callback();
        }
    }

    public void close()
    {
        if (m_videoPlayer != null && m_videoPlayer.targetTexture != null)
        {
            m_videoPlayer.targetTexture.Release();
        }
        
        KtUnity.destory(this.m_obj);
        Camera.main.backgroundColor = Color.black;
    }
}