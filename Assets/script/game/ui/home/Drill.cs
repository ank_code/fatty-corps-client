using System.Collections.Generic;
using ak;
using kf.anim;
using script.game.mgr;
using UnityEngine;

namespace script.game.ui.home
{
    // 战斗演练
    public class Drill
    {
        private GameObject m_parent;
        private GameObject m_objLeftTeam;
        private GameObject m_objRightTeam;
        
        private List<SpineAnim> m_saLeftTeam;
        
        public void init(GameObject parent)
        {
            m_parent = parent;

            m_objLeftTeam = KtUnity.findObj(parent, "leftTeam");
            
            m_objRightTeam = KtUnity.findObj(parent, "rightTeam");
            
        }

        // 根据当前战役刷新
        public void refresh()
        {
            var cfg = CfgMgr.getInstance().getMainGQ(Player.getInstance().info.guanqia);
            // 关卡怪
            var enemyTypeIds = cfg.getEnemyTypeIds();
            
        }
    }
}