using System;
using kf;
using kf.anim;
using script.game.ui.battle.effect.anim.state;
using UnityEngine;

namespace script.game.ui.battle.effect.anim
{
    public class EffectAnimFactory : Singleton<EffectAnimFactory>
    {
        private GameObject m_parent;
        
        public void setParent(GameObject parent)
        {
            m_parent = parent;
        }

        public EffectAnim showEffectOnce(string animPath, Vector3 pos, bool flip, Action<SpineAnim.CallbackEvent> callback)
        {
            EffectAnim ea = new EffectAnim();
            ea.init(m_parent, animPath, pos, flip, () =>
            {
                ea.playOnce(callback);
            });

            return ea;
        }
    }
}