using System.Collections;
using ak;
using kf;
using script.game.enums;
using script.game.mgr;
using script.game.net.msg.server.hero;
using script.game.obj.config;
using script.game.ui.bag;
using script.game.ui.bag.comp;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.hero
{
    public class HeroItem : MonoBehaviour
    {
        protected GameObject m_objImgBg;
        protected GameObject m_objImgAvatar;
        protected GameObject m_objImgCamp;
        protected GameObject m_objTxtLv;
        protected GameObject m_objTxtName;
        protected GameObject m_objHeroStar;
        
        protected Image m_imgBg;
        protected Image m_imgAvatar;
        protected Image m_imgCamp;
        protected TMP_Text m_txtLv;
        protected TMP_Text m_txtName;
        protected HeroStar m_heroStar;

        protected HeroShort m_heroShort;
        protected int m_index;

        protected CfgHero m_cfg;

        protected bool m_showName = true;

        private void Awake()
        {
            init();
        }

        protected virtual void init()
        {
            m_objImgBg = KtUnity.findObj(gameObject, "imgBg");
            m_imgBg = m_objImgBg.GetComponent<Image>();
            dragAndClickElem(m_objImgBg);
            
            m_objImgAvatar = KtUnity.findObj(gameObject, "imgAvatar");
            m_imgAvatar = m_objImgAvatar.GetComponent<Image>();
            dragAndClickElem(m_objImgAvatar);
            
            m_objImgCamp = KtUnity.findObj(gameObject, "imgCamp");
            m_imgCamp = m_objImgCamp.GetComponent<Image>();
            dragAndClickElem(m_objImgCamp);
            
            m_objTxtLv = KtUnity.findObj(gameObject, "txtLv");
            m_txtLv = m_objTxtLv.GetComponent<TMP_Text>();
            dragAndClickElem(m_objTxtLv);
            
            m_objTxtName = KtUnity.findObj(gameObject, "txtName");
            m_txtName = m_objTxtName.GetComponent<TMP_Text>();
            // dragAndClickElem(m_objTxtName);

            m_objHeroStar = KtUnity.findObj(gameObject, "starContainer");
            m_heroStar = KtUnity.addComponent<HeroStar>(m_objHeroStar);

            setVisible(false);
        }

        protected void dragAndClickElem(GameObject obj)
        {
            KtUnity.addComponent<ScrollElem>(obj);
            KtUI.addListener(obj, onClick);
        }
        
        public int getId()
        {
            return m_heroShort.id;
        }

        public int getIndex()
        {
            return m_index;
        }

        protected virtual void onClick()
        {
            // 选中
            GameEvent.sendEvent(EEvent.OnHeroItemClick, new object[] {m_heroShort.id, m_index});
        }

        public void setVisible(bool b)
        {
            if (!b)
            {
                gameObject.name = "item_null";
            }
        
            m_objImgBg.SetActive(b);
            m_objImgAvatar.SetActive(b);
            m_objImgCamp.SetActive(b);
            m_objTxtLv.SetActive(b);

            if (m_showName)
            {
                m_objTxtName.SetActive(b);
            }
            else
            {
                m_objTxtName.SetActive(false);
            }
           
            m_objHeroStar.SetActive(b);
        }

        public void showName(bool b)
        {
            m_objTxtName.SetActive(false);
        }

        public void reload(HeroShort heroShort, int index)
        {
            m_heroShort = heroShort;
            m_index = index;
            
            setVisible(false);
            // 必须先设置为展示状态，不能在load后再展示，否则会覆盖需要隐藏的设置
            // setVisible(true);
            StartCoroutine(load(heroShort));
        }

        IEnumerator load(HeroShort heroShort)
        {
            yield return null;
            m_cfg = CfgMgr.getInstance().getHero(heroShort.typeId);
            ResMgr.getInstance().load<Sprite>(HeroUtil.getIconPath(heroShort.typeId), false, true, o =>
            {
                m_imgAvatar.sprite = (Sprite)o;
            });
            
            ResMgr.getInstance().load<Sprite>(HeroUtil.getCampBgPath(m_cfg.camp), false, true, o =>
            {
                m_imgBg.sprite = (Sprite)o;
            });
            
            ResMgr.getInstance().load<Sprite>(HeroUtil.getCampIconPath(m_cfg.camp), false, true, o =>
            {
                m_imgCamp.sprite = (Sprite)o;
            });
            
            m_txtLv.text = m_heroShort.lv.ToString();
            m_txtName.text = m_cfg.name;
            m_heroStar.setStar(m_heroShort.star);
            
            // 尝试加载成功后再展示
            setVisible(true);
        }
    }
}