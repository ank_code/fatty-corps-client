using YooAsset;

/**
 * 全局配置
 */
public class GlobalCfg
{
    // 服务器模式
    public enum EHostMode
    {
        // 本地服
        Local,
        // 开发测试服
        Dev,
        // 正式服
        Prod,
    }

    // 服务器地址
    // public const EHostMode MODE = EHostMode.Local;
    public const EHostMode MODE = EHostMode.Prod;
    
    // 资源加载模式
    // public const EPlayMode ASSETS_PLAY_MODE = EPlayMode.EditorSimulateMode;
    // public const EPlayMode ASSETS_PLAY_MODE = EPlayMode.HostPlayMode;
    public const EPlayMode ASSETS_PLAY_MODE = EPlayMode.WebPlayMode;
    
    
    public static string getGameHost()
    {
        switch (MODE)
        {
            case EHostMode.Dev:
                return "http://49.235.114.30:30300";
            case EHostMode.Prod:
                return "https://game.artistschool.xyz/game";
            default:
                return "http://127.0.0.1:8080";
        }
    }
    
    public static string getAccHost()
    {
        switch (MODE)
        {
            case EHostMode.Dev:
                return "http://49.235.114.30:30301";
            case EHostMode.Prod:
                return "https://game.artistschool.xyz/account";
            default:
                return "http://127.0.0.1:8081";
        }
    }

    public static string getUpdateUrl()
    {
        switch (MODE)
        {
            case EHostMode.Prod:
                string url = "https://game.artistschool.xyz/fattyCorps/hotUpdate/";
#if UNITY_WEBGL
                url += "wx";
#elif UNITY_ANDROID
                url += "android";
#elif UNITY_IOS
                url += "ios";
#endif
                return url;
            default:
                return "http://49.235.114.30:30800/fattyCorps";
                // return "http://49.235.114.30:30800/fattyCorps";
                // return "https://edu-public-1309163888.cos.ap-nanjing.myqcloud.com/fattyCorps";
        }
    }
}