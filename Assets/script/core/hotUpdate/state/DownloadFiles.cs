using System.Collections;
using YooAsset;

namespace kf.hotpUpdate.state
{
    // 下载文件并检查磁盘空间
    public class DownloadFiles : FsmState
    {
        public override void onEnter()
        {
            // todo: 判断磁盘空间是否足够
            
            HotUpdate.HotUpdateMono mono = (HotUpdate.HotUpdateMono)getMachine().getBlackboard(HotUpdate.BBK_MONOBEHAVIOUR);
            mono.startCoroutine(beginDownload());
        }
        
        private IEnumerator beginDownload()
        {
            var downloader = (ResourceDownloaderOperation)getMachine().getBlackboard(HotUpdate.BBK_DOWNLOADER);
            downloader.BeginDownload();
            yield return downloader;

            // 检测下载结果
            if (downloader.Status != EOperationStatus.Succeed)
            {
                getMachine().setBlackboard(HotUpdate.BBK_ERR, downloader.Error);
                yield break;
            }
            else
            {
                getMachine().setState<InitHybrid>();
            }
        }
    }
}