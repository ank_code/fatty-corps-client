using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = System.Object;

namespace kf
{
    public class KtDelegateMethod
    {
        public delegate void vv();
        public delegate void vi(int v1);

        public delegate void vii(int v1, int v2);

        public delegate int iv();

        public delegate int ii(int v1);

        public delegate int iii(int v1, int v2);

        public delegate void vb(bool v1);

        public delegate void vs(string v1);
        
        public delegate void vo(Object v1);
        
        public delegate void voo(Object v1, Object v2);
    }
}