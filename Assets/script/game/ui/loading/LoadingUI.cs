using System.Collections;
using ak;
using kf;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.waiting
{
    /**
     * 网络请求时，如果请求时间过长，需要展示加载中动画
     * 同时一次网络请求发起后，用户不能进行其它操作，直到结果返回
     */
    public class LoadingUI : GameUI
    {
        private GameObject m_objAnimLoading;
        private Image m_imgBg;
        private Coroutine m_handle;
        
        // 延迟出现加载动画
        private const float SHOW_LOADING_ANIM_DELAY = 0.3f;
        
        public override void onCreate(IUIParam param)
        {
            m_objAnimLoading = KtUnity.findObj(gameObject, "animLoading");
            m_imgBg = gameObject.GetComponent<Image>();
        }

        public override void onShow()
        {
            m_imgBg.color = new Color(0, 0, 0, 0);
            m_objAnimLoading.SetActive(false);
            m_handle = StartCoroutine(delayShow(SHOW_LOADING_ANIM_DELAY));
        }

        public override void onHide()
        {
            StopCoroutine(m_handle);
        }

        IEnumerator delayShow(float time)
        {
            yield return new WaitForSeconds(time);
            m_imgBg.color = new Color(0, 0, 0, 0.4f);
            m_objAnimLoading.SetActive(true);
        }

        public override EUILayer getUILayer()
        {
            return EUILayer.Loading;
        }
    }
}