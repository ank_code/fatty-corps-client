using System;
using kf;
using script.core.store;
using script.game.mgr;
using script.game.net.http;
using script.game.ui.battle;
using script.game.ui.battle.effect;
using script.game.ui.battle.effect.anim;
using script.game.ui.home;
using script.game.ui.loading;
using UnityEngine;
using Store = script.core.store.Store;

public class Entry : MonoBehaviour
{
    private void Start()
    {
        Debug.Log("aktest Entry start");
        init();
        showUI();
    }

    private void init()
    {
        StoreMgr.getInstance().init();
        GameUIRegester.getInstance().init();
        AccountRequest.getInstance().init();
        GameRequest.getInstance().init();
        DisplayInvoker.getInstance().init();
        NumMgr.getInstance().init();
        Player.getInstance().init();
        ServerCfg.getInstance().init();
    }

    private void Update()
    {
        TimerMgr.getInstance().Update();
    }

    private void showUI()
    {
        // todo: 异步加载主场景资源与配置信息
        CfgMgr.getInstance().init(() =>
        {
            // 有token直接进入主场景
            string token = (string)StoreMgr.getInstance().get(StoreMgr.KEY_ACC_TOKEN);
            // todo: token有效性判断
            if (string.IsNullOrEmpty(token))
            {
                UIMgr.getInstance().show<LoginBgUI>();

                LoginUI.Param param = new LoginUI.Param();
                param.account = "bbbbbbb2";
                param.password = "bbbbbbbb@123";
                UIMgr.getInstance().show<LoginUI>(param);
                
            }
            else
            {
                HomeUI.open();
            }
        
            // testBattle();
        });
    }
    
    public void testBattle()
    {
        UIMgr.getInstance().show<SceneLoadingUI>();
        
        GameRequest.getInstance().testBattle((res) =>
        {
            BattleUI.UIParam param = new BattleUI.UIParam();
            param.res = res;
            
            UIMgr.getInstance().show<BattleUI>(param);
            // initHeroes();
            // RecordInvoker.getInstance().next();
            
        });
    }
}