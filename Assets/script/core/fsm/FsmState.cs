using System;

namespace kf
{
    public abstract class FsmState : IFsmState
    {
        protected StateMachine m_machine;

        public StateMachine getMachine()
        {
            return m_machine;
        }

        public Object getBlackboard(string key)
        {
            return m_machine.getBlackboard(key);
        }

        public void onCreate(StateMachine machine)
        {
            this.m_machine = machine;
        }

        public abstract void onEnter();

        public void onUpdate()
        {
        }

        public void onExit()
        {
        }
    }
}