namespace script.game.enums
{
    public enum ESkillType
    {
        // 主动
        Active,
        // 普通（会替代默认普通攻击）
        Normal,
        // 被动
        Passive,
        // 属性（增加属性面板中的属性）
        Prop,
    }
}