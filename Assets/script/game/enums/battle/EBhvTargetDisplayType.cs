namespace script.game.enums.battle
{
    public enum EBhvTargetDisplayType
    {
        // 无（直接触发受击/死亡动画）
        None,
        // 目标身上出现特效（在关键帧时触发受击/死亡动画）
        Target,
        // 全屏出现特效（在关键帧时触发受击/死亡动画）
        FullScreen,
    }
}