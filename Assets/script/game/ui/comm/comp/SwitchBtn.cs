using ak;
using kf;
using UnityEngine;

namespace script.game.ui.comm.comp
{
    public class SwitchBtn
    {
        private GameObject m_objStateA;
        private GameObject m_objStateB;
        
        public enum EState
        {
            A,
            B
        }

        private EState m_state = EState.A;
        public delegate bool OnClick(EState v1);

        private OnClick m_onClick = null;

        private const string DEF_CLICK_AREA_A_OBJ_NAME = "objStateA";
        private const string DEF_CLICK_AREA_B_OBJ_NAME = "objStateB";
        
        public void init(GameObject obj, OnClick callback)
        {
            init(obj, DEF_CLICK_AREA_A_OBJ_NAME, DEF_CLICK_AREA_B_OBJ_NAME, callback);
        }

        public void init(GameObject obj, string clickAraeObjNameA, string clickAraeObjNameB, OnClick callback)
        {
            m_onClick = callback;
            
            GameObject objClickAreaA = KtUnity.findObj(obj, clickAraeObjNameA);
            GameObject objClickAreaB = KtUnity.findObj(obj, clickAraeObjNameB);
            
            m_objStateA = KtUnity.findObj(obj, DEF_CLICK_AREA_A_OBJ_NAME);
            m_objStateB = KtUnity.findObj(obj, DEF_CLICK_AREA_B_OBJ_NAME);
            
            KtUI.addListener(objClickAreaA, onClick);
            KtUI.addListener(objClickAreaB, onClick);
            
            setState(EState.A);
        }

        private void onClick()
        {
            if (m_onClick != null)
            {
                bool autoSwitch = m_onClick(m_state);

                if (autoSwitch)
                {
                    EState newState = EState.A;
                    if (m_state.Equals(EState.A))
                    {
                        newState = EState.B;
                    }
                    
                    setState(newState);
                }
            }
        }

        public void setState(EState state)
        {
            m_state = state;
            
            m_objStateA.SetActive(m_state.Equals(EState.A));
            m_objStateB.SetActive(m_state.Equals(EState.B));
        }

        public EState getState()
        {
            return m_state;
        }
    }
}