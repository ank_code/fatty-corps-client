using System;
using ak;
using kf;
using script.game.enums;
using script.game.enums.item;
using script.game.mgr;
using script.game.ui.bag.type;
using TMPro;
using UnityEngine;

namespace script.game.ui.comm.comp
{
    public class TxtTab : ABaseTab
    {
        private GameObject m_obj;
        private GameObject m_objImgChoosed;
        private GameObject m_objTxtName;
        private TMP_Text m_txtName;

        private EItemType m_type;
        private Action m_onFocus;
    
        public void init(GameObject obj, Action onFocus)
        {
            m_obj = obj;
            m_onFocus = onFocus;

            m_objImgChoosed = KtUnity.findObj(m_obj, "imgChoosed");
            KtUI.addListener(m_obj, () =>
            {
                base.onClickTab();
            });

            m_objTxtName = KtUnity.findObj(m_obj, "txtName");
            m_txtName = m_objTxtName.GetComponent<TMP_Text>();

            this.refresh();
        }
    
        public override void onFocus()
        {
            m_objImgChoosed.SetActive(true);
            m_txtName.color = Color.white;

            if (m_onFocus != null)
            {
                m_onFocus();
            }
        }

        public override void onLoseFocus()
        {
            m_objImgChoosed.SetActive(false);
            m_txtName.color = Color.gray;
        }
    }
}