using System.Collections;
using UnityEngine;
using YooAsset;

namespace kf.hotpUpdate.state
{
    public class UpdateManifest : FsmState
    {
        public override void onEnter()
        {
            HotUpdate.HotUpdateMono mono = (HotUpdate.HotUpdateMono)getMachine().getBlackboard(HotUpdate.BBK_MONOBEHAVIOUR);
            mono.startCoroutine(updateManifest());
        }
        
        private IEnumerator updateManifest()
        {
            // yield return new WaitForSecondsRealtime(0.5f);

            var packageName = (string)getMachine().getBlackboard(HotUpdate.BBK_YOOASSET_PACKAGE_NAME);
            var packageVersion = (string)getMachine().getBlackboard(HotUpdate.BBK_PACKAGE_VERSION);
            var package = YooAssets.GetPackage(packageName);
            bool savePackageVersion = true;
            var operation = package.UpdatePackageManifestAsync(packageVersion, savePackageVersion);
            yield return operation;

            if (operation.Status != EOperationStatus.Succeed)
            {
                Debug.LogWarning(operation.Error);
                getMachine().setBlackboard(HotUpdate.BBK_ERR, operation.Error);
                getMachine().setState<Error>();
                yield break;
            }
            else
            {
                getMachine().setState<CreateDownloader>();
            }
        }
    }
}