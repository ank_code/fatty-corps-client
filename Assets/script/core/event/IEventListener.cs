﻿
namespace kf
{
    public interface IEventListener
    {
        void onEvent(int eId, object[] objs);
    }
}