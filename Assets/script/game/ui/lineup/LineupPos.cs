using ak;
using DG.Tweening;
using kf;
using kf.anim;
using script.game.enums;
using script.game.mgr;
using script.game.net.msg.server.hero;
using script.game.ui.bag;
using script.game.ui.battle;
using UnityEngine;
using UnityEngine.EventSystems;

namespace script.game.ui.lineup
{
    public class LineupPos
    {
        private int m_no = 0;
        private GameObject m_obj;

        private HeroShort m_heroShort = new HeroShort();
        
        private bool m_left = true;
        private GameObject m_objAnim;
        private GameObject m_objHaloLeft;
        private GameObject m_objHaloRight;
        private GameObject m_objHaloEmpty;
        private GameObject m_objBtnArea;
        private GameObject m_objSwitchArea;

        private SpineAnim m_sa;
        private DragElem m_de;

        private GameObject m_objDragFront = null;
        private Vector3 m_orgAnimPos;
        private Vector3 m_orgBtnAreaPos;
        private Vector3 m_dragMoveDis;

        public void init(int no, GameObject obj, bool left)
        {
            this.m_no = no;
            this.m_obj = obj;
            this.m_left = left;

            m_objAnim = KtUnity.findObj(obj, "anim");
            m_objHaloEmpty = KtUnity.findObj(obj, "imgHaloEmpty");
            m_objHaloLeft = KtUnity.findObj(obj, "imgHaloLeft");
            m_objHaloRight = KtUnity.findObj(obj, "imgHaloRight");

            m_objBtnArea = KtUnity.findObj(obj, "btnArea");
            KtUI.addListener(m_objBtnArea, () =>
            {
                // setHero(0);
                if (m_heroShort.typeId > 0)
                {
                    EventMgr.getInstance().sendEvent((int)EEvent.OnLineupHeroItemClick, new object[] {m_heroShort, false});
                }
            });

            m_objSwitchArea = KtUnity.findObj(obj, "switchArea");

            m_de = KtUnity.addComponent<DragElem>(m_objBtnArea);
            m_de.init();
            m_de.addListener(onBeginDrag, onDrag, onEndDrag);
            
            m_orgAnimPos = m_objAnim.transform.localPosition;
            m_orgBtnAreaPos = m_objBtnArea.transform.localPosition;

            clear();
        }

        public bool isEmpty()
        {
            return !m_objAnim.activeSelf;
        }

        public HeroShort getHeroShort()
        {
            return m_heroShort;
        }

        public int getNo()
        {
            return m_no;
        }

        public bool isPosInSwitchArea(Vector3 pos)
        {
            return KtUI.isPosInObj(pos, m_objSwitchArea);
        }

        // heroTypeId=0 表示清空
        public void setHero(HeroShort heroShort)
        {
            clear();
            m_heroShort = heroShort;
            
            if (m_heroShort.typeId > 0)
            {
                m_objAnim.SetActive(true);
                var cfgHero = CfgMgr.getInstance().getHero(m_heroShort.typeId);
                new SpineAnim(false, cfgHero.anim_path, m_objAnim, SpineAnim.EType.UI, (sa) =>
                {
                    m_sa = sa;
                    m_sa.playLoop(BattleHero.ANIM_NAME_IDLE);
                    m_sa.setScale(BattleHero.DEFAULT_ANIM_SCALE);
                });

                update();
            }
        }

        public void clear()
        {
            m_objAnim.SetActive(false);
            if (m_sa != null)
            {
                KtUnity.destory(m_sa.getObj());
                m_sa = null;
            }
            
            m_heroShort = HeroShort.empty();
            
            update();
        }

        public void setDragFront(GameObject objDragFront)
        {
            m_objDragFront = objDragFront;
        }

        private void update()
        {
            if (m_objAnim.activeSelf)
            {
                m_objHaloEmpty.SetActive(false);

                if (m_left)
                {
                    m_objHaloLeft.SetActive(true);
                    m_objHaloRight.SetActive(false);
                    
                    m_objBtnArea.SetActive(true);
                }
                else
                {
                    m_objHaloLeft.SetActive(false);
                    m_objHaloRight.SetActive(true);
                    m_objBtnArea.SetActive(false);
                }
            }
            else
            {
                m_objHaloEmpty.SetActive(true);

                m_objHaloLeft.SetActive(false);
                m_objHaloRight.SetActive(false);
                
                m_objBtnArea.SetActive(false);
            }
        }
        
        public void onBeginDrag(PointerEventData eventData) {
            if (m_objDragFront == null)
            {
                return;
            }

            m_dragMoveDis = m_objAnim.transform.position - Input.mousePosition;

            // 动画放到最前方显示
            m_objAnim.transform.SetParent(m_objDragFront.transform);
        }

        public void onDrag(PointerEventData eventData)
        {
            if (m_objDragFront == null)
            {
                return;
            }

            Vector3 dest = Input.mousePosition + m_dragMoveDis;
            
            // 动画移动
            m_objAnim.transform.DOMove(dest, 0);
        }

        public void onEndDrag(PointerEventData eventData)
        {
            if (m_objDragFront == null)
            {
                return;
            }

            EventMgr.getInstance().sendEvent((int)EEvent.OnLineupHeroDragEnd, new object[]{m_no, Input.mousePosition});
            // toOrg();
        }
        
        public void toOrg()
        {
            m_objAnim.transform.SetParent(m_obj.transform);
            m_objBtnArea.transform.SetAsLastSibling();

            m_objAnim.transform.localPosition = m_orgAnimPos;
            m_objBtnArea.transform.localPosition = m_orgBtnAreaPos;
        }
    }
}