using System;
using ak;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace kf
{
    public class KtUI
    {
        private const float SCALE_TIME = 0.1f;

        public static void addListener(GameObject obj, GameObject clickObj, UnityAction action)
        {
            addListener(obj, action, EventTriggerType.PointerUp, true, true, 0.8f, 1.1f, clickObj);
        }

        public static void addListenerAutoScale(GameObject obj, UnityAction action)
        {
            addListener(obj, action, EventTriggerType.PointerUp, true, true, 0.8f, 1.1f, obj);
        }

        public static void addLintenerNoScale(GameObject obj, GameObject clickObj, UnityAction action)
        {
            addListener(obj, action, EventTriggerType.PointerUp, true, false, 0.8f, 1.1f, clickObj);
        }
        

        public static void addListener(GameObject obj, UnityAction action,
            EventTriggerType eventTriggerType = EventTriggerType.PointerUp, bool sound = true, bool autoScale = false,
            float minScale = 0.8f, float maxScale = 1.1f, GameObject clickObj = null)
        {
            if (obj == null || action == null)
            {
                return;
            }

            if (clickObj == null)
            {
                clickObj = obj;
            }

            // 判断是否是精灵,精灵需要射线碰撞检测组建
            // SpriteRenderer sr = obj.GetComponent<SpriteRenderer>();
            // SkeletonGraphic sg = obj.GetComponent<SkeletonGraphic>();

            // if (sr != null || sg != null) {
            //     UnityAdp.addComponent<BoxCollider2D>(clickObj);
            // }

            // if (sr != null) {
            //     KtUnity.addComponent<BoxCollider2D>(clickObj);
            // }

            EventTrigger trigger = KtUnity.addComponent<EventTrigger>(clickObj);

            if (sound)
            {
                action += () =>
                {
                    // todo: 增加通用点击声音
                };
            }

            Action doAction = () =>
            {
                Image img = clickObj.GetComponent<Image>();
                if (img)
                {
                    // 没有响应条件则不响应事件，兼容滑动view内元素控件 ScrollElem.cs
                    if (!img.raycastTarget)
                    {
                        return;
                    }
                }

                action();
            };

            addEvent(trigger, EventTriggerType.PointerUp, () =>
            {
                var scrollElem = clickObj.GetComponent<ScrollElem>();
                // scrollElem不支持缩放，不然会使scrollElem原来的特性失效
                if (autoScale && scrollElem == null)
                {
                    var seq = DOTween.Sequence();
                    seq.Append(obj.transform.DOScale(maxScale, SCALE_TIME));
                    seq.Append(obj.transform.DOScale(1, SCALE_TIME));
                    // seq.Append(obj.transform.DOScale(maxScale, SCALE_TIME));
                    // seq.Append(obj.transform.DOScale(1, SCALE_TIME));

                    seq.OnComplete(() =>
                    {
                        if (eventTriggerType == EventTriggerType.PointerUp)
                        {
                            doAction();
                        }
                    });
                }
                else
                {
                    if (eventTriggerType == EventTriggerType.PointerUp)
                    {
                        doAction();
                    }
                }
            });

            addEvent(trigger, EventTriggerType.PointerDown, () =>
            {
                var scrollElem = clickObj.GetComponent<ScrollElem>();
                if (autoScale && scrollElem == null)
                {
                    obj.transform.DOScale(minScale, SCALE_TIME).OnComplete(() =>
                    {
                        if (eventTriggerType == EventTriggerType.PointerDown)
                        {
                            doAction();
                        }
                    });
                }
                else
                {
                    if (eventTriggerType == EventTriggerType.PointerDown)
                    {
                        doAction();
                    }
                }
            });
        }
        
        private static void addEvent(EventTrigger trigger, EventTriggerType type, Action action)
        {
            EventTrigger.Entry eventUp = new EventTrigger.Entry();
            eventUp.eventID = type;
            eventUp.callback.AddListener((BaseEventData arg0) =>
            {
                if (Input.touchCount > 1)
                {
                    return;
                }

                action();
            });

            // 同一类型事件重复添加过滤
            for (var i = 0; i < trigger.triggers.Count; i++)
            {
                if (trigger.triggers[i].eventID.Equals(eventUp.eventID))
                {
                    trigger.triggers.RemoveAt(i);
                }
            }
            
            trigger.triggers.Add(eventUp);
        }

        public static UnityAction addDOScale(GameObject obj, UnityAction action, float minScale, float maxScale)
        {
            UnityAction DOaction = delegate
            {
                obj.transform.DOScale(maxScale, SCALE_TIME).OnComplete(() => obj.transform.DOScale(1, SCALE_TIME)
                    .OnComplete(delegate
                    {
                        if (action != null)
                            action();
                    }));
            };
            return DOaction;
        }

        public static float getTxtWidth(Text txt)
        {
            return getTxtWidth(txt.font, txt.text, txt.fontSize, txt.fontStyle);
        }

        public static float getTxtWidth(Font font, string txt, int fontSize, FontStyle fontStyle)
        {
            string text = txt;
            font.RequestCharactersInTexture(text, fontSize, fontStyle);
            CharacterInfo characterInfo;
            float width = 0f;
            for (int i = 0; i < text.Length; i++)
            {
                font.GetCharacterInfo(text[i], out characterInfo, fontSize);
                width += characterInfo.advance;
            }

            return width;
        }

        public static bool isPosInObj(Vector3 pos, GameObject obj)
        {
            var rt = obj.GetComponent<RectTransform>();
            var objPos = obj.transform.position;
            return pos.x >= objPos.x && pos.x <= objPos.x + rt.rect.width &&
                   pos.y >= objPos.y && pos.y <= objPos.y + rt.rect.height;
        }
    }
}