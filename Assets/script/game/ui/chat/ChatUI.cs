using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ak;
using DG.Tweening;
using kf;
using script.core.store;
using script.game.mgr;
using script.game.net.http;
using script.game.net.msg.client.chat;
using script.game.net.msg.server.chat;
using SuperScrollView;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace script.game.ui.chat
{
    public class ChatUI : GameUI
    {
        private GameObject m_objContent;
        private GameObject m_objBg;

        private LoopListView2 m_loopListView;
        private List<ChatInfo> m_chatInfo = new List<ChatInfo>();
        private TMP_InputField m_input;

        private bool m_initLoopList = false;
        private int m_chatId = 0;

        private int m_tickTimeCur = 0;
        private bool m_recvNewChat = false;
        private Coroutine m_tickHandler = null;
        
        private const int MIN_TICK_TIME = 1;
        private const int MAX_TICK_TIME = 30;
        private const int STEP_TICK_TIME = 2;

        private const float ENTER_ANIM_TIME = 0.3f;
        private const float ENTER_ANIM_DIS = 2100f;

        public enum EChatChannelType
        {
            World,
            Guild,
            Recruit
        }

        public override void onCreate(IUIParam param)
        {
            m_objBg = KtUnity.findObj(gameObject, "bg");
            m_objContent = KtUnity.findObj(gameObject, "content");

            GameObject objTabBarChannel = KtUnity.findObj(gameObject, "tabBarChannel");
            KtUnity.addComponent<ChatTypeBar>(objTabBarChannel);

            GameObject objLoopListView = KtUnity.findObj(gameObject, "svChat");
            m_loopListView = objLoopListView.GetComponent<LoopListView2>();

            GameObject objBtnClose = KtUnity.findObj(gameObject, "btnClose");
            KtUI.addListener(objBtnClose,
                () =>
                {
                    m_objBg.transform.DOLocalMoveX(-ENTER_ANIM_DIS, ENTER_ANIM_TIME).OnComplete(() => { close(); });
                });

            GameObject objBtnSend = KtUnity.findObj(gameObject, "btnSend");
            KtUI.addListener(objBtnSend, () => { send(); });

            GameObject objInput = KtUnity.findObj(gameObject, "input");
            m_input = objInput.GetComponent<TMP_InputField>();

#if UNITY_WEBGL
            KtUnity.addComponent<WXTMPInputHandler>(objInput);
#endif

            GameObject to = null;
            var transformPosition = to.transform.position;
        }

        public override void onHide()
        {
            removeTick();
        }

        public override void onShow()
        {
            m_objBg.transform.DOLocalMoveX(-ENTER_ANIM_DIS, ENTER_ANIM_TIME).From();
            addTick();
        }

        private void addTick()
        {
            // 立即获取一次聊天消息，然后每过1s获取一次，如果获取到则保持每1s获取，如果没有获取到则变为5s一次，10s一次20s一次，30s一次，60s一次
            delayGetNewChat(0);
        }
        
        private void delayGetNewChat(float delay)
        {
            if (m_tickHandler == null)
            {
                m_tickHandler = StartCoroutine(getNewChat(delay));
            }
        }

        IEnumerator getNewChat(float delay)
        {
            Debug.Log("请求获取聊天信息" + " 下次获取时间：" + m_tickTimeCur);
            yield return new WaitForSeconds(delay);
            req();
            Debug.Log("结束获取聊天信息");
            m_tickHandler = null;
        }

        private int getNextTickTime()
        {
            int step = m_recvNewChat ? -STEP_TICK_TIME : STEP_TICK_TIME;
            int tickTime = m_tickTimeCur;
            tickTime += step;

            if (tickTime < MIN_TICK_TIME)
            {
                tickTime = MIN_TICK_TIME;
            }

            if (tickTime > MAX_TICK_TIME)
            {
                tickTime = MAX_TICK_TIME;
            }

            return tickTime;
        }

        private void removeTick()
        {
            if (m_tickHandler != null)
            {
                StopCoroutine(m_tickHandler);
                m_tickHandler = null;
            }
        }

        private void send()
        {
            CChat req = new CChat();
            req.content = m_input.text;
            // req.chatId = StoreMgr.getInstance().getInt(StoreMgr.KEY_CHAT_ID, 0);
            req.chatId = m_chatId;
            GameRequest.getInstance().chat(req, req =>
            {
                onRecvChatList(req.chatInfoList);
                m_tickTimeCur = getNextTickTime();
            });
            m_input.text = "";
        }

        private void req()
        {
            CGetChatListMsg req = new CGetChatListMsg();
            req.chatId = m_chatId;
            GameRequest.getInstance().getChatList(req, (rep) => { onRecvChatList(rep.chatInfoList); });
        }

        private void onRecvChatList(List<ChatInfo> list)
        {
            m_recvNewChat = false;
            if (list != null && list.Count > 0)
            {
                m_chatInfo = m_chatInfo.Concat(list).ToList();
                m_chatInfo.Sort();
                m_chatId = m_chatInfo[m_chatInfo.Count - 1].id;

                m_recvNewChat = true;
                initData();
            }

            // 设置下次获取聊天信息时间
            m_tickTimeCur = getNextTickTime();
            delayGetNewChat(m_tickTimeCur);
        }

        private void initData()
        {
            // todo: 插入时间条目
            if (m_initLoopList)
            {
                m_loopListView.SetListItemCount(m_chatInfo.Count, false);
                m_loopListView.RefreshAllShownItem();
            }
            else
            {
                m_loopListView.InitListView(m_chatInfo.Count, initLoopListItem);
                m_initLoopList = true;
            }

            m_loopListView.MovePanelToItemIndex(m_chatInfo.Count - 1, 0);
        }

        private LoopListViewItem2 initLoopListItem(LoopListView2 view2, int rowIndex)
        {
            if (rowIndex < 0)
            {
                return null;
            }

            LoopListViewItem2 item = view2.NewListViewItem("chatElemRowTest");
            // 根据chatData加载数据
            var chatElem = KtUnity.addComponent<ChatElem>(item.gameObject);
            chatElem.init(m_chatInfo[rowIndex]);

            // 重新计算高度
            // chatElem.refreash();

            return item;
        }
    }
}