using System;
using System.Collections.Generic;
using kf;
using Newtonsoft.Json;
using Object = UnityEngine.Object;

namespace script.core.config
{
    public class CfgJsonReader : ICfgReader
    {
        public List<T> read<T>(string path) where T : new()
        {
            var loadRawFile = ResMgr.getInstance().loadRawFile(path);
            string data = System.Text.Encoding.UTF8.GetString(loadRawFile); 
            return JsonConvert.DeserializeObject<List<T>>(data, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }

        public void readAsync<T>(string path, Action<List<T>> callback) where T : new()
        {
            ResMgr.getInstance().loadRawFile(path, true, (rawFile) =>
            {
                callback(analysisCfg<T>(rawFile));
            });
        }

        private List<T> analysisCfg<T>(object rewFile)
        {
            string data = System.Text.Encoding.UTF8.GetString((byte[])rewFile); 
            List<T> retList = JsonConvert.DeserializeObject<List<T>>(data, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });

            return retList;
        }
    }
}