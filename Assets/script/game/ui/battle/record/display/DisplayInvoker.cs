using kf;
using script.game.enums.battle;
using script.game.mgr;
using script.game.net.msg.server;
using script.game.obj.config;
using script.game.ui.battle.effect.anim.state;
using UnityEngine;

namespace script.game.ui.battle.effect.anim
{
    public class DisplayInvoker : Singleton<DisplayInvoker>
    {
        private StateMachine m_mach;

        public const string BB_CFG = "cfg";
        public const string BB_REC_ELEM = "rec_elem";
        
        public void init()
        {
            m_mach = new StateMachine(this);
            initState();
        }

        private void initState()
        {
            m_mach.addState<StateSelf>();
            m_mach.addState<StateTarget>();
        }

        public void play(RecElem recElem)
        {
            CfgBehaviorDisplay cfg = CfgMgr.getInstance().getBehaviorDisplay(recElem.displayId);
            
            m_mach.setBlackboard(BB_CFG, cfg);
            m_mach.setBlackboard(BB_REC_ELEM, recElem);
            
            Debug.Log("aktest play StateSelf 1 cnd=>" + recElem.cnd);
            m_mach.setState<StateSelf>();
            Debug.Log("aktest play StateSelf 2 cnd=>" + recElem.cnd);
        }
    }
}