using System;
using UnityEngine.Events;

namespace kf
{
    public interface IBaseHttp
    {
        public enum EErrType
        {
            Http,
            Custom,
            Unknown
        }

        public class Err
        {
            public EErrType type;
            public int code;
            public string msg;
        }
        
        // Init
        public void init();
        // 设置头
        public void addHeader(string k, string v);
        // 统一响应错误信息
        public void setFailCallback(Action<Err> callback);
        
        // get sync
        public string get(string path);
        // post sync
        public string post(string path, string param);
        
        // get async
        public void getAsync(string path, Action<string> success, Action<Err> fail);
        // post async
        public void postAsync(string path, string param, Action<string> success, Action<Err> fail);
        
        // 获取服务器访问地址
        public string getBaseUrl();
    }
}