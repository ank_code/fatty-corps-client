using System;
using System.Collections.Generic;
using UnityEngine;
using Object = System.Object;

namespace kf
{
    public class StateMachine
    {
        private Dictionary<string, Object> m_blackboard = new Dictionary<string, Object>();
        private Dictionary<string, IFsmState> m_states = new Dictionary<string, IFsmState>();
        private IFsmState m_curState = null;
        private IFsmState m_preState = null;
        private Object m_owner = null;
        
        public delegate void onStateChange(IFsmState newState);
        private onStateChange m_onStateChgCallback = null;

        public StateMachine(Object owner)
        {
            this.m_owner = owner;
        }

        public void setOnStateChgCallback(onStateChange callback)
        {
            m_onStateChgCallback = callback;
        }

        public bool empty()
        {
            return m_states.Count <= 0;
        }

        public void addState<TState>() where TState : IFsmState
        {
            var stateType = typeof(TState);
            var state = Activator.CreateInstance(stateType) as IFsmState;
            addState(state);
        }

        public void addState(IFsmState state)
        {
            if (state == null)
            {
                return;
            }
            
            var stateName = state.GetType().FullName;
            if (m_states.ContainsKey(stateName))
            {
                Debug.Log("[" + stateName + "]state already exist");
                return;
            }
            
            state.onCreate(this);
            m_states.Add(stateName, state);
        }

        public void setState<TState>() where TState : IFsmState
        {
            var stateName = typeof(TState).FullName;
            setState(stateName);
        }

        public void setState(string stateName)
        {
            if (!m_states.ContainsKey(stateName))
            {
                Debug.Log("[" + stateName + "] state not exist");
                return;
            }

            if (m_curState != null)
            {
                m_curState.onExit();
                m_preState = m_curState;
            }
            
            m_curState = m_states[stateName];
            m_curState.onEnter();

            if (m_onStateChgCallback != null)
            {
                m_onStateChgCallback(m_curState);
            }
        }
        
        public void update()
        {
            m_curState.onUpdate();
        }

        public IFsmState getCurState()
        {
            return m_curState;
        }

        public IFsmState getPreState()
        {
            return m_preState;
        }

        public Object getOwner()
        {
            return m_owner;
        }

        //// 黑板数据
        public void setBlackboard(string k, Object v)
        {
            if (m_blackboard.ContainsKey(k))
            {
                m_blackboard[k] = v;
            }
            else
            {
                m_blackboard.Add(k, v);
            }
        }

        public Object getBlackboard(string k)
        {
            return m_blackboard[k];
        }
    }
}