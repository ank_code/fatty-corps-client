using System.Collections.Generic;
using script.game.net.msg.server.item;

namespace script.game.net.msg.client.hero
{
    public class SGetAttachmentMail
    {
        public int mailId;
        public List<DItem> attachment;
    }
}