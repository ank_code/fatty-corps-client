using System.Collections.Generic;
using System.IO;
using kf;
using Newtonsoft.Json;
using UnityEngine;

#if UNITY_WEBGL
using WeChatWASM;
#endif

namespace script.core.store
{
    /*
     * 本地存档
     */
    public class Store
    {
        public class StoreData
        {
            public Dictionary<string, object> data = new Dictionary<string, object>();
        }

        private StoreData m_storeData = new StoreData();
        private string m_filePath = "";

        public void init(string fileName = "")
        {
            m_filePath = getPath(fileName);
            
#if UNITY_WEBGL
            WXFileSystemManager fs = WX.GetFileSystemManager();
        
            // 判断是否存在目录
            if (fs.AccessSync(m_filePath).Equals("access:ok"))
            {
                // 读取内容
                string json = fs.ReadFileSync(m_filePath, "utf-8");
                m_storeData = JsonConvert.DeserializeObject<StoreData>(json);
            }

            Debug.Log($" 读取文件 ：{m_filePath} ");
#else
            if (File.Exists(m_filePath))
            {
                string json = File.ReadAllText(m_filePath);
                m_storeData = JsonConvert.DeserializeObject<StoreData>(json);
            }
#endif

        }

        private void save()
        {
            var json = JsonConvert.SerializeObject(m_storeData);
            
#if UNITY_WEBGL
            WXFileSystemManager fs = WX.GetFileSystemManager();
            fs.WriteFileSync(m_filePath, json, "utf-8");
#else
            File.WriteAllText(m_filePath, json);
#endif
 
        }

        private string getPath(string fileName)
        {
            string name = string.IsNullOrEmpty(fileName) ? "UserData" : fileName;
#if UNITY_WEBGL
            // webgl/微信 不支持Application.persistentDataPath
            Debug.Log("aktest wx USER_DATA_PATH=>" + WX.env.USER_DATA_PATH);
            string path = Path.Combine(WX.env.USER_DATA_PATH, name + ".json");
            return path;
#elif UNITY_EDITOR
            return Application.streamingAssetsPath + "/" + name + ".json";
#else
            return Application.persistentDataPath + "/" + name + ".json";
#endif
        }
        
        public object get(string key)
        {
            if (m_storeData.data.ContainsKey(key))
            {
                return m_storeData.data[key];
            }

            return null;
        }

        public void set(string key, object val)
        {
            m_storeData.data[key] = val;
            save();
        }
    }
}