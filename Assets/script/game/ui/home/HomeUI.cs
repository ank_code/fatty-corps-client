using System;
using System.Collections;
using ak;
using kf;
using script.game.enums;
using script.game.mgr;
using script.game.net.http;
using script.game.net.msg.client.item;
using script.game.net.msg.server;
using script.game.obj.config;
using script.game.ui.award;
using script.game.ui.bag;
using script.game.ui.battle;
using script.game.ui.chat;
using script.game.ui.comm;
using script.game.ui.comm.comp;
using script.game.ui.hero;
using script.game.ui.lineup;
using script.game.ui.loading;
using script.game.ui.mail;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.home
{
    public class HomeUI : GameUI
    {
        private CurrencyInfo m_currencyInfo;

        public enum EState
        {
            Home,
            Battle,
        }

        private EState m_state;

        private SwitchBtn m_sbBattle;
        private SwitchBtn m_sbHome;

        private GameObject m_objBtnBounty;
        private GameObject m_objBtnChallenge;
        private GameObject m_objBtnMopUp;

        private TMP_Text m_txtGQChapter;
        private TMP_Text m_txtGQIndex;

        private GameObject m_objHangAward;
        private TMP_Text m_txtTime;
        private Image m_imgTimePro;

        private TMP_Text m_txtUserName;
        private TMP_Text m_txtUserLv;
        private Image m_imgExp;
        private TMP_Text m_txtUserExp;
        private Text m_txtCombat;

        private Coroutine m_hangTick;
        private const int MIN_HANG_TIME_AWARD = 5;

        public static void open()
        {
            CGetInfo req = new CGetInfo();
            req.token = (string)StoreMgr.getInstance().get(StoreMgr.KEY_ACC_TOKEN);
            
            if (string.IsNullOrEmpty(req.token))
            {
                throw new Exception("请先获取账号token");
            }
            
            // 换取游戏服务器token
            GameRequest.getInstance().getInfo(req, (rep) =>
            {
                // 保存token
                EventMgr.getInstance().sendEvent((int)EEvent.OnRefreshGameToken, new[] { rep.newToken });
                
                // 保存用户信息
                Player.getInstance().info = rep;
                
                CfgMgr.getInstance().init(() =>
                {
                    // 配置加载完成进入游戏
                    UIMgr.getInstance().show<HomeUI>();
                });
            });
        }

        public override void onCreate(IUIParam param)
        {
            // 货币
            GameObject objCurrency = KtUnity.findObj(gameObject, "currency");
            m_currencyInfo = new CurrencyInfo();
            m_currencyInfo.init(objCurrency, () =>
            {
                m_currencyInfo.add(CurrencyInfo.ECurrencyType.Money);
                m_currencyInfo.add(CurrencyInfo.ECurrencyType.Coin);
            });
            
            initLeftTop();
            initLeft();
            initRight();
            initRightBottom();
            // initDevBtn();

            setState(EState.Home);
        }

        public void OnDestroy()
        {
            StopCoroutine(hangTick());
        }

        private void initLeftTop()
        {
            GameObject objUserInfo = KtUnity.findObj(gameObject, "userInfo");
            m_txtUserName = KtUnity.findObj(objUserInfo, "txtName").GetComponent<TMP_Text>();
            refreshPlayerName();
            
            m_txtUserLv = KtUnity.findObj(objUserInfo, "txtLv").GetComponent<TMP_Text>();
            refreshPlayerLv();
            
            m_imgExp = KtUnity.findObj(objUserInfo, "imgExpProgress").GetComponent<Image>();
            m_txtUserExp = KtUnity.findObj(objUserInfo, "txtExp").GetComponent<TMP_Text>();
            refreshPlayerExp();
            
            m_txtCombat = KtUnity.findObj(objUserInfo, "txtCombatVal").GetComponent<Text>();
            refreshCombat();
        }

        private void refreshPlayerName()
        {
            m_txtUserName.text = Player.getInstance().info.nickname;
        }

            private void refreshPlayerLv()
        {
            m_txtUserLv.text = Player.getInstance().info.lv + "";
        }

        private void refreshPlayerExp()
        {
            var cfgPlayerExp = CfgMgr.getInstance().getPlayerExp(Player.getInstance().info.lv);
            m_imgExp.fillAmount = (float)Player.getInstance().info.exp / cfgPlayerExp.exp;
            m_txtUserExp.text = Player.getInstance().info.exp + "/" + cfgPlayerExp.exp;
        }

        private void refreshGQ()
        {
            var cfgGQ = CfgMgr.getInstance().getMainGQ(Player.getInstance().info.guanqia);

            if (Player.getInstance().clearance())
            {
                m_txtGQChapter.text = "第" + cfgGQ.chapter + "章";
                m_txtGQIndex.text = "通关了";
            }
            else
            {
                m_txtGQChapter.text = "第" + cfgGQ.chapter + "章";
                m_txtGQIndex.text = "第" + cfgGQ.no + "关";
            }
        }

        private void refreshCombat()
        {
            m_txtCombat.text = Player.getInstance().info.combatVal + "";
        }

        private void initLeft()
        {
            m_txtGQChapter = KtUnity.findObj(gameObject, "txtChapter").GetComponent<TMP_Text>();
            m_txtGQIndex = KtUnity.findObj(gameObject, "txtIndex").GetComponent<TMP_Text>();

            m_objHangAward = KtUnity.findObj(gameObject, "hangAward");
            m_txtTime = KtUnity.findObj(gameObject, "txtTime").GetComponent<TMP_Text>();
            m_imgTimePro = KtUnity.findObj(gameObject, "imgTimePro").GetComponent<Image>();

            KtUI.addListener(KtUnity.findObj(gameObject, "btnAward"), () =>
            {
                int dTime = (int)KtTime.getTimestampToSeconds() - Player.getInstance().info.lastHangTime;
                if (dTime <= MIN_HANG_TIME_AWARD)
                {
                    AlertUtil.alertInfo("木有奖励，等等再来吧");
                    return;
                }

                // 领取挂机奖励
                GameRequest.getInstance().getHangAward((rep) =>
                {
                    Player.getInstance().setLv(rep.newLv);
                    Player.getInstance().setExp(rep.newExp);

                    if (rep.awards.Count > 0)
                    {
                        HangAwardUI.Param p = new HangAwardUI.Param();
                        p.items = rep.awards;
                        p.hangTime = getHangTime();
                        UIMgr.getInstance().show<HangAwardUI>(p);
                    }

                    // 重置领取时间
                    Player.getInstance().info.lastHangTime = (int)KtTime.getTimestampToSeconds();
                });
            });

            refreshHangUI();

            // 开启挂机累计时间定时器
            addHangTimer();
        }

        private void refreshHangUI()
        {
            // 关卡名
            refreshGQ();

            int dTime = (int)KtTime.getTimestampToSeconds() - Player.getInstance().info.lastHangTime;
            dTime = correctHangTime(dTime);

            // 进度
            float pro = (float)dTime / ServerCfg.getInstance().cfg.hangTimeMax;
            m_imgTimePro.fillAmount = pro;
        }

        private void addHangTimer()
        {
            m_hangTick = StartCoroutine(hangTick());
        }

        IEnumerator hangTick()
        {
            while (true)
            {
                yield return new WaitForSeconds(1.0f);
                m_txtTime.text = getHangTime();
            }
        }

        private string getHangTime()
        {
            int dTime = (int)KtTime.getTimestampToSeconds() - Player.getInstance().info.lastHangTime;
            dTime = correctHangTime(dTime);
            return KtTime.formatDTime(dTime);
        }

        private int correctHangTime(int hangTime)
        {
            if (hangTime > ServerCfg.getInstance().cfg.hangTimeMax)
            {
                hangTime = ServerCfg.getInstance().cfg.hangTimeMax;
            }

            return hangTime;
        }
        
        private void initRight()
        {
            GameObject objRight = KtUnity.findObj(gameObject, "right");

            KtUI.addListenerAutoScale(KtUnity.findObj(objRight, "btnChat"),
                () => { UIMgr.getInstance().show<ChatUI>(); });

            KtUI.addListenerAutoScale(KtUnity.findObj(objRight, "btnFriend"),
                () => { Debug.Log("aktest click btnFriend"); });

            KtUI.addListenerAutoScale(KtUnity.findObj(objRight, "btnMail"), () =>
            {
                Debug.Log("aktest click btnMail");
                UIMgr.getInstance().show<MailUI>();
            });

            KtUI.addListenerAutoScale(KtUnity.findObj(objRight, "btnTask"),
                () => { Debug.Log("aktest click btnTask"); });

            KtUI.addListenerAutoScale(KtUnity.findObj(objRight, "btnRank"),
                () => { Debug.Log("aktest click btnRank"); });

            m_objBtnBounty = KtUnity.findObj(objRight, "btnBounty");
            KtUI.addListenerAutoScale(m_objBtnBounty, () => { Debug.Log("aktest click btnBounty"); });

            m_objBtnChallenge = KtUnity.findObj(objRight, "btnChallenge");
            KtUI.addListenerAutoScale(m_objBtnChallenge, () =>
            {
                Debug.Log("aktest click btnChallenge");
                // 打开布阵界面
                var cfgGQ = CfgMgr.getInstance().getMainGQ(Player.getInstance().info.guanqia);
                string title = string.IsNullOrEmpty(cfgGQ.name) ? "第" + cfgGQ.no + "关" : cfgGQ.name;
                
                LineupUI.open(() =>
                    {
                        // 发关卡战斗消息
                        GameRequest.getInstance().mainGuanqia((rep) =>
                        {
                            BattleUI.UIParam param = new BattleUI.UIParam();
                            param.res = rep.battleResult;
                            param.type = BattleUI.EBattleType.MainGuanqia;
                            param.awards = rep.awards;
                            
                            UIMgr.getInstance().show<BattleUI>(param);

                            if (rep.battleResult.br.Equals(EBattleResult.Win))
                            {
                                Player.getInstance().passGuanqia();
                                refreshGQ();
                            }
                        });

                    }, title, cfgGQ.getEnemyTypeIds()
                );
            });

            m_objBtnMopUp = KtUnity.findObj(objRight, "btnMopUp");
            KtUI.addListenerAutoScale(m_objBtnMopUp, () => { Debug.Log("aktest click btnMopUp"); });
        }

        private void initRightBottom()
        {
            GameObject objBtnBag = KtUnity.findObj(gameObject, "btnBag");
            KtUI.addListener(objBtnBag, objBtnBag, () => { BagUI.open(); });

            GameObject objBtnHero = KtUnity.findObj(gameObject, "btnHero");
            KtUI.addListener(objBtnHero, objBtnHero, () => { HeroListUI.open(); });

            GameObject objBtnBattle = KtUnity.findObj(gameObject, "btnBattle");
            m_sbBattle = new SwitchBtn();

            GameObject objBtnHome = KtUnity.findObj(gameObject, "btnHome");
            m_sbHome = new SwitchBtn();

            m_sbHome.init(objBtnHome, (SwitchBtn.EState state) =>
            {
                if (state.Equals(SwitchBtn.EState.A))
                {
                    setState(EState.Home);
                }

                return false;
            });

            m_sbBattle.init(objBtnBattle, (SwitchBtn.EState state) =>
            {
                if (state.Equals(SwitchBtn.EState.A))
                {
                    setState(EState.Battle);
                }

                return false;
            });
        }

        private void setState(EState state)
        {
            m_state = state;
            bool home = m_state.Equals(EState.Home);

            if (home)
            {
                m_sbHome.setState(SwitchBtn.EState.B);
                m_sbBattle.setState(SwitchBtn.EState.A);
            }
            else
            {
                m_sbBattle.setState(SwitchBtn.EState.B);
                m_sbHome.setState(SwitchBtn.EState.A);
            }

            m_objBtnBounty.SetActive(!home);
            m_objBtnChallenge.SetActive(!home);
            m_objBtnMopUp.SetActive(!home);
            m_objHangAward.SetActive(!home);
        }

        private void initDevBtn()
        {
            GameObject objBtnBag = KtUnity.findObj(gameObject, "btnBagDev");
            KtUI.addListener(objBtnBag, () => { BagUI.open(); });

            GameObject objBtnHero = KtUnity.findObj(gameObject, "btnHeroDev");
            KtUI.addListener(objBtnHero, () => { HeroListUI.open(); });

            GameObject objBtnChat = KtUnity.findObj(gameObject, "btnChatDev");
            KtUI.addListener(objBtnChat, () => { UIMgr.getInstance().show<ChatUI>(); });

            GameObject objBtnTestLineup = KtUnity.findObj(gameObject, "btnTestLineupDev");
            KtUI.addListener(objBtnTestLineup, () => { LineupUI.open(() => { }, "设置阵容"); });

            GameObject objBtnTestBattle = KtUnity.findObj(gameObject, "btnTestBattleDev");
            KtUI.addListener(objBtnTestBattle, () =>
            {
                UIMgr.getInstance().show<SceneLoadingUI>();

                GameRequest.getInstance().testBattle((res) =>
                {
                    BattleUI.UIParam param = new BattleUI.UIParam();
                    param.res = res;

                    UIMgr.getInstance().show<BattleUI>(param);
                });
            });

            GameObject objBtnTestAddItem = KtUnity.findObj(gameObject, "btnTestAddItemDev");
            KtUI.addListener(objBtnTestAddItem, () =>
            {
                CAddItem msg = new CAddItem();
                msg.itemTypeId = 100002;
                msg.num = 999999;

                GameRequest.getInstance().addItem(msg, (res) => { });
            });
        }

        public override void onEvent(int eId, object[] objs)
        {
            EEvent e = (EEvent)eId;
            switch (e)
            {
                case EEvent.OnPlayerLvChanged:
                {
                    refreshPlayerLv();
                    break;
                }
                case EEvent.OnPlayerExpChanged:
                {
                    refreshPlayerExp();
                    break;
                }
                case EEvent.OnPlayerNameChanged:
                {
                    refreshPlayerName();
                    break;
                }
                case EEvent.OnPlayerCombatChanged:
                {
                    refreshCombat();
                    break;
                }
            }
        }
    }
}