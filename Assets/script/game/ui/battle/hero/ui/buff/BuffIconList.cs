using System.Collections.Generic;
using ak;
using kf;
using UnityEngine;

namespace script.game.ui.battle
{
    /**
     * buff icon列表
     */
    public class BuffIconList
    {
        private GameObject m_obj;
        private Dictionary<int, BuffIcon> m_icons = new Dictionary<int, BuffIcon>();

        private const string PREFAB_PATH = "Assets/res/prefab/battle/buff/buffIconList.prefab";
        
        public void init(GameObject parent, Vector3 localPos)
        {
            ResMgr.getInstance().load<GameObject>(PREFAB_PATH, true, true, o =>
            {
                m_obj = (GameObject)o;
                m_obj.transform.SetParent(parent.transform);
                m_obj.transform.localPosition = localPos;
                m_obj.name = "buffIconList";
            });
        }

        public GameObject getObj()
        {
            return m_obj;
        }

        public void addBuff(int typeId)
        {
            if (!m_icons.ContainsKey(typeId))
            {
                BuffIcon icon = new BuffIcon();
                icon.init(m_obj, () =>
                {
                    icon.setBuff(typeId);
                });
                
                m_icons.Add(typeId, icon);
            }
            else
            {
                m_icons[typeId].add();
            }
        }

        public void removeBuff(int typeId)
        {
            if (!m_icons.ContainsKey(typeId))
            {
                return;
            }
            
            m_icons[typeId].remove();
            if (m_icons[typeId].getNum() <= 0)
            {
                KtUnity.destory(m_icons[typeId].getObj());
                m_icons.Remove(typeId);
            }
        }   
    }
}