using System;
using System.Collections;
using ak;
using kf;
using kf.progressBar;
using script.game.enums;
using script.game.ui.comm;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.battle
{
    public class HeroInfoUI
    {
        private GameObject m_obj;
        // private Slider m_sliderHp;
        // private Slider m_sliderEnergy;

        private ProgressBar m_pbHp = new ProgressBar();
        private ProgressBar m_pbEnergy = new ProgressBar();
        
        private Text m_txtLv;
        private Image m_imgCamp;

        private BuffIconList m_buffIconList;
        
        private long m_maxHp;
        private long m_curHp;
        private int m_maxEnergy;
        private int m_curEnergy;
        private int m_lv;

        private const string PREFAB_PATH = "Assets/res/prefab/battle/heroInfo.prefab";
        
        public void init(GameObject parent, Vector3 localPos, Action callback)
        {
            ResMgr.getInstance().load<GameObject>(PREFAB_PATH, true, true, o =>
            {
                m_obj = (GameObject)o;
                m_obj.transform.SetParent(parent.transform);
                m_obj.transform.localPosition = localPos;
                m_obj.name = "heroInfo";

                // var objSliderHp = KtUnity.findObj(m_obj, "sliderHp");
                // m_sliderHp = objSliderHp.GetComponent<Slider>();
                //
                // var objSliderEnergy = KtUnity.findObj(m_obj, "sliderEnergy");
                // m_sliderEnergy = objSliderEnergy.GetComponent<Slider>();
            
                var objHp = KtUnity.findObj(m_obj, "hp");
                m_pbHp.init(objHp);
            
                var objEnergy = KtUnity.findObj(m_obj, "energy");
                m_pbEnergy.init(objEnergy);
            
                var objTxtLv = KtUnity.findObj(m_obj, "txtLv");
                m_txtLv = objTxtLv.GetComponent<Text>();

                m_buffIconList = new BuffIconList();
                var rectTransform = m_obj.GetComponent<RectTransform>();
                m_buffIconList.init(m_obj, new Vector3(0, rectTransform.sizeDelta.y, 0));

                var objImgCamp = KtUnity.findObj(m_obj, "imgCamp");
                m_imgCamp = objImgCamp.GetComponent<Image>();
            });
            
        }

        public void setMaxHp(long maxHp)
        {
            m_maxHp = maxHp;
            updateHp();
        }

        public void setMaxEnergy(int maxEnergy)
        {
            m_maxEnergy = maxEnergy;
            updateEnergy();
        }

        public void setCurHp(long hp)
        {
            m_curHp = hp;
            updateHp();
        }

        public void setCurEnergy(int energy)
        {
            m_curEnergy = energy;
            updateEnergy();
        }

        public void setLv(int lv)
        {
            m_lv = lv;
            m_txtLv.text = m_lv.ToString();
        }

        public void setCamp(ECamp camp)
        {
            var path = CampUtil.getCampIconPath(camp);
            ResMgr.getInstance().load<Sprite>(path, false, true, o =>
            {
                m_imgCamp.sprite = (Sprite)o;
            });
        }

        private void updateHp()
        {
            float per = (float)m_curHp / (float)m_maxHp;
            m_pbHp.setVal(per);
        }
        
        private void updateEnergy()
        {
            float per = (float)m_curEnergy / (float)m_maxEnergy;
            m_pbEnergy.setVal(per);
        }

        public void addBuff(int typeId)
        {
            m_buffIconList.addBuff(typeId);
        }

        public void removeBuff(int typeId)
        {
            m_buffIconList.removeBuff(typeId);
        }
        
    }
}