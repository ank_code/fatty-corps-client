
using System;
using ak;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace kf
{
    // 视频组件
    public class Video : RawImage
    {
        private Action m_callback = null;
        private VideoPlayer m_videoPlayer = null;

        public void addCompleteListener(Action callback)
        {
            m_callback = callback;
        }

        public VideoPlayer getVideoPlayer()
        {
            return m_videoPlayer;
        }

        public void init(int w, int h, VideoClip videoClip)
        {
            this.rectTransform.sizeDelta = new Vector2(w, h);

            var audioSource = KtUnity.addComponent<AudioSource>(gameObject);

            RenderTextureFormat format = RenderTextureFormat.DefaultHDR;
            // 创建新的Render Texture对象
            RenderTexture renderTexture = new RenderTexture(w, h, 0);
            renderTexture.format = format;
            renderTexture.name = "tempRenderTexture";

            this.texture = renderTexture;
            
            m_videoPlayer = KtUnity.addComponent<VideoPlayer>(gameObject);
            m_videoPlayer.playOnAwake = false;
            m_videoPlayer.source = VideoSource.VideoClip;
            m_videoPlayer.aspectRatio = VideoAspectRatio.FitOutside;
            m_videoPlayer.loopPointReached += onVideoCompleteCore;
            m_videoPlayer.targetTexture = renderTexture;
            m_videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
            m_videoPlayer.SetTargetAudioSource(0, audioSource);

            if (videoClip != null)
            {
                m_videoPlayer.clip = videoClip;
            }
        }

        public void play()
        {
            m_videoPlayer.Play();
        }
        
        private void onVideoCompleteCore(VideoPlayer source)
        {
            m_videoPlayer.targetTexture.Release();
            
            if (m_callback != null)
            {
                m_callback();
            }
        }
    }
    

}
