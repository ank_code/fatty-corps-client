namespace script.game.net.msg.server
{
    public class RecElemEffect
    {
        // 触发后影响类型
        public ERecEffectType type;
        // 类型对应的值
        public long value;
        // 额外参数
        public string extra;
        // 触发时机
        public ERecEffectPlayOpportunity opportunity = ERecEffectPlayOpportunity.AfterTargetUnderAttack;
        // 目标站位序号
        public HeroPos targetPos;
    }
}