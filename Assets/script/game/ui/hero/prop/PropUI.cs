using ak;
using DG.Tweening;
using kf;
using script.game.net.msg.server.hero;
using script.game.ui.bag.skill;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.bag.prop
{
    public class PropUI : GameUI
    {
        private GameObject m_objBody;
        private GameObject m_objContent;

        private HeroDetail m_heroDetail;
        
        private const float ENTER_ANIM_TIME = 0.2f;
        
        public class Param : IUIParam
        {
            public HeroDetail heroDetail;
        }
        
        public override void onCreate(IUIParam param)
        {
            base.onCreate(param);

            Param p = (Param)param;
            m_heroDetail = p.heroDetail;
            
            KtUI.addListenerAutoScale(KtUnity.findObj(gameObject, "btnClose"), () =>
            {
                this.close();
            });
            
            m_objBody = KtUnity.findObj(gameObject, "bg");

            m_objContent = KtUnity.findObj(gameObject, "Content");
            LayoutRebuilder.ForceRebuildLayoutImmediate(m_objContent.GetComponent<RectTransform>());

            initProp();
        }

        public override void onShow()
        {
            base.onShow();
            m_objBody.transform.DOScaleX(0.1f, ENTER_ANIM_TIME).From();
            m_objBody.transform.DOScaleY(0.1f, ENTER_ANIM_TIME).From();
        }

        private void initProp()
        {
            generatePropElem("att", "攻击", m_heroDetail.att);
            generatePropElem("def", "护甲", m_heroDetail.def);
            generatePropElem("maxHp", "最大hp", m_heroDetail.maxHp);
            generatePropElem("speed", "速度", m_heroDetail.speed);
            generatePropElem("skillDamagePer", "技能伤害", m_heroDetail.skillDamagePer);
            generatePropElem("precisePer", "精准", m_heroDetail.precisePer);
            generatePropElem("parryPer", "格挡", m_heroDetail.parryPer);
            generatePropElem("criticalPer", "暴击", m_heroDetail.criticalPer);
            generatePropElem("criticalDamagePer", "暴击伤害", m_heroDetail.criticalDamagePer);
            generatePropElem("armorBreakPer", "破甲", m_heroDetail.armorBreakPer);
            generatePropElem("immunityControlPer", "免控率", m_heroDetail.immunityControlPer);
            generatePropElem("reduceDamagePer", "减伤率", m_heroDetail.reduceDamagePer);
            generatePropElem("sacredDamagePer", "神圣伤害", m_heroDetail.sacredDamagePer);
            generatePropElem("reduceCriticalPer", "暴击抵抗", m_heroDetail.reduceCriticalPer);
            generatePropElem("incDamagePer", "增伤率", m_heroDetail.incDamagePer);
            generatePropElem("incControlPer", "增控率", m_heroDetail.incControlPer);
        }

        private void generatePropElem(string objName, string name, string val)
        {
            var objProp = KtUnity.findObj(m_objContent, objName);
            KtUnity.findObj(objProp, "txtName").GetComponent<TMP_Text>().text = name;
            KtUnity.findObj(objProp, "txtVal").GetComponent<TMP_Text>().text = val;
        }
    }
}