using System;

public abstract class ABaseTab
{
    public enum EState {
        Focus,
        LoseFocus,
    }

    protected Action m_onClickTabOut;
    protected EState m_state = EState.LoseFocus;

    public void setOnClickLinstener(Action onClickOut) {
        m_onClickTabOut = onClickOut;
    }

    public virtual void setTabState(EState state) {
        m_state = state;

        switch (m_state) {
            case EState.Focus:
                {
                    onFocus();
                }
                break;

            case EState.LoseFocus:
                {
                    onLoseFocus();
                }
                break;
        }
    }

    public void refresh() {
        setTabState(EState.LoseFocus);
    }

    public EState getTabState() {
        return m_state;
    }

    public abstract void onFocus();
    public abstract void onLoseFocus();
    
    public void onClickTab() {
        if (m_onClickTabOut != null) {
            m_onClickTabOut();
        }
    }
}
