using System;
using kf;
using script.game.mgr;

namespace script.game.net.http
{
    public abstract class MsgHttp : JsonHttp
    {
        private const string HEADER_TOKEN = "token";

        public bool hasToken()
        {
            return !string.IsNullOrEmpty(getToken());
        }

        public abstract String getToken();
        
        public virtual void postAsync<Req, RepMsg>(string path, Req param, Action<RepMsg> success, bool useToken = true, Action<IBaseHttp.Err> fail = null) {
            if (useToken)
            {
                addHeader(HEADER_TOKEN, getToken());
            }
            else
            {
                removeHeader(HEADER_TOKEN);
            }
        
            postAsync(path, param, success, fail);
        }
        
        public override void postAsync<Req, RepMsg>(string path, Req param, Action<RepMsg> success, Action<IBaseHttp.Err> fail = null)
        {
            base.postAsync<Req, SMsg<RepMsg>>(path, param, (rep) =>
            {
                if (rep.code != GameStatusCode.OK)
                {
                    invokeFail(IBaseHttp.EErrType.Custom, fail, rep);
                }
                else
                {
                    success(rep.data);
                }
            }, fail);
        }

        private void invokeFail<RepMsg>(IBaseHttp.EErrType type, Action<IBaseHttp.Err> fail, SMsg<RepMsg> rep)
        {
            IBaseHttp.Err err = new IBaseHttp.Err();
            err.type = type;
            err.code = rep.code;
            err.msg = rep.msg;
            
            if (base.m_failCallback != null)
            {
                base.m_failCallback(err);
            }

            if (fail != null)
            {
                fail(err);
            }
        }
    }
}