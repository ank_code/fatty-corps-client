using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.battle.effect.number
{
    /**
     * 飘字，用于伤害与治疗
     * 创建后可以反复展示，每次展示播放完整飘字动画
     */
    public class BattleNum
    {
        private GameObject m_objNum;
        private Text m_txt;

        private const int DEFAULT_FONT_SIZE = 40;
        private const float NOR_DAMAGE_DEST_Y = 100;
        private const float MOVE_TIME = 2;
        private const float FADE_TIME = 1;
        private const float ENLARGE_SCALE_MAX = 1.5f;
        private const float ENLARGE_SCALE_MIN = 1.1f;
        private const float SCALE_TIME = 0.1f;
        
        
        private const float CRITICAL_ENLARGE_SCALE_MAX = 2f;
        private const float CRITICAL_ENLARGE_SCALE_MIN = 1.5f;
        
        private const float OTHER_DEST_Y = 50;
        private const float OTHER_MOVE_TIME = 1;
        private const float OTHER_FADE_TIME = 0.5f;
        
        public void init(GameObject parent)
        {
            m_objNum = new GameObject("number");
            m_objNum.transform.SetParent(parent.transform);
            m_txt = m_objNum.AddComponent<Text>();
            m_txt.fontSize = DEFAULT_FONT_SIZE;
            m_txt.alignment = TextAnchor.MiddleCenter;

            var fitter = m_objNum.AddComponent<ContentSizeFitter>();
            fitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
            fitter.verticalFit = ContentSizeFitter.FitMode.PreferredSize;
        }

        public void setType(NumMgr.ENumType type)
        {
            m_txt.font = NumMgr.getInstance().getFont(type);
        }

        public GameObject obj()
        {
            return m_objNum;
        }

        public void show(string num, NumMgr.ENumType type)
        {
            m_objNum.SetActive(true);
            m_txt.text = num;
            m_objNum.transform.localScale = Vector3.one;
            m_txt.color = new Color(m_txt.color.r, m_txt.color.g, m_txt.color.b, 1f);

            Sequence seq = DOTween.Sequence();
            
            if (type.Equals(NumMgr.ENumType.NorDamage))
            {
                seq.Append(m_objNum.transform.DOScale(ENLARGE_SCALE_MAX, SCALE_TIME));
                seq.Append(m_objNum.transform.DOScale(1, SCALE_TIME));
                seq.Append(m_objNum.transform.DOScale(ENLARGE_SCALE_MIN, SCALE_TIME));
                seq.Append(m_objNum.transform.DOScale(1, SCALE_TIME));
                seq.Append(m_objNum.transform.DOMoveY(m_objNum.transform.position.y + NOR_DAMAGE_DEST_Y, MOVE_TIME));
                seq.Insert(FADE_TIME, 
                    DOTween.To(() => m_txt.color, value => m_txt.color = value, 
                        new Color(m_txt.color.r, m_txt.color.g, m_txt.color.b, 0f), MOVE_TIME - FADE_TIME));
            } else if (type.Equals(NumMgr.ENumType.Critical))
            {
                seq.Append(m_objNum.transform.DOScale(CRITICAL_ENLARGE_SCALE_MAX, SCALE_TIME));
                seq.Append(m_objNum.transform.DOScale(1, SCALE_TIME));
                seq.Append(m_objNum.transform.DOScale(CRITICAL_ENLARGE_SCALE_MIN, SCALE_TIME));
                seq.Append(m_objNum.transform.DOScale(1, SCALE_TIME));
                seq.Append(m_objNum.transform.DOMoveY(m_objNum.transform.position.y + NOR_DAMAGE_DEST_Y, MOVE_TIME));
                seq.Insert(FADE_TIME, 
                    DOTween.To(() => m_txt.color, value => m_txt.color = value, 
                        new Color(m_txt.color.r, m_txt.color.g, m_txt.color.b, 0f), MOVE_TIME - FADE_TIME));
            }
            else
            {
                seq.Append(m_objNum.transform.DOScale(ENLARGE_SCALE_MAX, SCALE_TIME));
                seq.Append(m_objNum.transform.DOScale(1, SCALE_TIME));
                seq.Append(m_objNum.transform.DOScale(ENLARGE_SCALE_MIN, SCALE_TIME));
                seq.Append(m_objNum.transform.DOScale(1, SCALE_TIME));
                seq.Append(m_objNum.transform.DOMoveY(m_objNum.transform.position.y + OTHER_DEST_Y, OTHER_MOVE_TIME));
                seq.Insert(OTHER_FADE_TIME, 
                    DOTween.To(() => m_txt.color, value => m_txt.color = value, 
                        new Color(m_txt.color.r, m_txt.color.g, m_txt.color.b, 0f), OTHER_MOVE_TIME - OTHER_FADE_TIME));
            }
            
            seq.OnComplete(() =>
            {
                NumMgr.getInstance().onNumPlayComplete(this);
            });
        }

        public bool active()
        {
            return m_objNum.activeSelf;
        }

        public void setActive(bool active)
        {
            m_objNum.SetActive(active);
        }
    }
}