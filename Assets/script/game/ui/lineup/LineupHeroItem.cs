using ak;
using kf;
using script.game.enums;
using script.game.mgr;
using script.game.ui.hero;
using UnityEngine;

namespace script.game.ui.lineup
{
    public class LineupHeroItem : HeroItem
    {
        private bool m_selected = false;
        private GameObject m_objImgSelected;
        private LineupUI m_ui;
        
        protected override void init()
        {
            base.init();

            m_objImgSelected = KtUnity.findObj(gameObject, "imgSelected");
            dragAndClickElem(m_objImgSelected);
            
            m_objTxtName.SetActive(false);
            setClicked(false);
        }

        public void setLineupUI(LineupUI ui)
        {
            m_ui = ui;
        }
        
        protected override void onClick()
        {
            if (!m_selected && m_ui.isFull())
            {
                // 阵容满了
                return;
            }
            
            // setClicked(!m_selected);
            GameEvent.sendEvent(EEvent.OnLineupHeroItemClick, new object[] {m_heroShort, !m_selected});
        }

        public void setClicked(bool b)
        {
            m_selected = b;
            m_objImgSelected.SetActive(m_selected);
        }
    }
}