using System;
using kf;
using script.game.enums;
using script.game.enums.battle;
using script.game.mgr;
using script.game.net.msg.server;
using script.game.obj.config;
using script.game.ui.battle.record;
using script.game.ui.battle.state;
using UnityEngine;

namespace script.game.ui.battle.effect.anim.state
{
    /**
     * stage1、自己动作阶段
     */
    public class StateSelf : FsmState
    {
        private CfgBehaviorDisplay m_cfg;
        private RecElem m_recElem;
        private BattleHero m_caster;
        
        public override void onEnter()
        {
            m_cfg = (CfgBehaviorDisplay)getBlackboard(DisplayInvoker.BB_CFG);
            m_recElem = (RecElem)getBlackboard(DisplayInvoker.BB_REC_ELEM);
            m_caster = m_recElem.caster == null ? null : BattleHeroMgr.getInstance().getHeroByPos(m_recElem.caster);
            
            if (m_cfg == null)
            {
                getMachine().setState<StateTarget>();
                RecordInvoker.getInstance().next();
                return;
            }
            
            // 把caster提到最高层，压着其它英雄显示
            GameEvent.sendEvent(EEvent.OnBattleUICasterToTop, new object[]{m_recElem});
            
            // 先播放self
            switch (m_cfg.self_display_type)
            {
                case EBhvCasterDisplayType.Standby:
                    procStandBy();
                    break;
                case EBhvCasterDisplayType.MoveToCenter:
                    procMoveToCenter();
                    break;
                case EBhvCasterDisplayType.MoveToTarget:
                    procMoveToTarget();
                    break;
                default:
                    throw new Exception("暂不支持的表现类型");
            }
        }

        // 播放原地攻击动画，需要关键帧
        private void procStandBy()
        {
            PlayCmdQueue cmdQueue = new PlayCmdQueue();
            PlayCmdQueue.PlayAnimCmd cmd = new PlayCmdQueue.PlayAnimCmd();
            cmd.animName = BattleHero.ANIM_NAME_ATT;
            cmd.keyFrameCallback = () =>
            {
                // 如果有特效，在特效动画的关键帧触发目标受攻击动画
                getMachine().setState<StateTarget>();
            };
            
            cmdQueue.push(cmd);
            
            m_caster.play(cmdQueue, () =>
            {
                RecordInvoker.getInstance().next();
            });
        }

        // 移动到地方正前方中心点，播放攻击动画，攻击动画关键帧触发目标受击动画，攻击动画播放完毕后返回原位
        private void procMoveToCenter()
        {
            procMoveTo(BattlePos.getInstance().getEnemyFront(m_caster.getPos().leftTeam));
        }

        // 移动到目标身前
        private void procMoveToTarget()
        {
            var effect = m_recElem.effects[0];
            procMoveTo(BattlePos.getInstance().getHeroUnderAttPos(effect.targetPos));
        }

        private void procMoveTo(Vector3 pos)
        {
            PlayCmdQueue cmdQueue = new PlayCmdQueue();
            
            // 移动
            PlayCmdQueue.PlayMoveToCmd moveToCmd = new PlayCmdQueue.PlayMoveToCmd();
            moveToCmd.pos = pos;
            cmdQueue.push(moveToCmd);
            
            // 播放攻击
            PlayCmdQueue.PlayAnimCmd cmdAnim = new PlayCmdQueue.PlayAnimCmd();
            cmdAnim.animName = BattleHero.ANIM_NAME_ATT;
            cmdAnim.keyFrameCallback = () =>
            {
                // 如果有特效，在特效动画的关键帧触发目标受攻击动画
                getMachine().setState<StateTarget>();
            };
            
            cmdQueue.push(cmdAnim);
            
            // 播放完攻击动画时，立即播放idle
            PlayCmdQueue.PlayIdleCmd cmdIdle = new PlayCmdQueue.PlayIdleCmd();
            cmdQueue.push(cmdIdle);
            
            // 播放序列
            m_caster.play(cmdQueue, () =>
            {
                RecordInvoker.getInstance().next();
            });
        }
    }
}