using DG.Tweening;
using kf;
using kf.anim;
using Spine.Unity;
using UnityEngine;

namespace script.game.ui.battle.state
{
    public class StateDead : FsmState
    {
        private const float FADE_OUT_TIME = 1.0f;
        public override void onEnter()
        {
            SpineAnim sa = (SpineAnim)getBlackboard(BattleHero.BB_SPINE_ANIM);
            BattleHero battleHero = (BattleHero)getBlackboard(BattleHero.BB_BATTLE_HERO);
            
            // 淡出消失动画
            SkeletonGraphic sg = sa.getSG();
            DOTween.To(() => sg.color, value => sg.color = value,
                new Color(sg.color.r, sg.color.g, sg.color.b, 0f), FADE_OUT_TIME).OnComplete(() =>
            {
                battleHero.setActive(false);
                sg.color = new Color(sg.color.r, sg.color.g, sg.color.b, 1f);
            });
        }
    }
}