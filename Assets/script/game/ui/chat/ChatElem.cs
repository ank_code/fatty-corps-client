using ak;
using kf;
using script.game.net.msg.server.chat;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.chat
{
    public class ChatElem : MonoBehaviour
    {
        private ChatInfo m_info;
        private Text m_txtChat;
        private ContentSizeFitter m_sizeFitter;
        
        private const float TXT_CHAT_PADDING = 20;
        private const float CONTENT_SPACE = 20;
        private const float CHAT_MAX_W = 500;
        // private const string FONT_PATH = "Assets/res/buildin/font/MSYHBD.TTC";
        
        public void init(ChatInfo info)
        {
            m_info = info;

            GameObject objChat = KtUnity.findObj(gameObject, "txtChat");
            m_txtChat = objChat.GetComponent<Text>();
            m_txtChat.text = info.content;
            //
            // GameObject objContent = KtUnity.findObj(gameObject, "content");
            // m_sizeFitter = objContent.GetComponent<ContentSizeFitter>();
            // m_sizeFitter.SetLayoutVertical();
            // Vector2 size = objContent.GetComponent<RectTransform>().sizeDelta;
            // RectTransform tf = gameObject.GetComponent<RectTransform>();
            // tf.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y);
            
            // LoopListView2不支持行根节点有ContentSizeFitter组件
            var txtChatRt = objChat.GetComponent<RectTransform>();
            Vector2 size = txtChatRt.sizeDelta;

            float chatW = KtUI.getTxtWidth(m_txtChat);
            if (chatW > CHAT_MAX_W)
            {
                chatW = CHAT_MAX_W;
            }

            // 重置txt大小
            txtChatRt.sizeDelta = new Vector2(chatW, txtChatRt.sizeDelta.y);
            var sizeFitter = objChat.GetComponent<ContentSizeFitter>();
            sizeFitter.SetLayoutVertical();
            
            size.x = chatW + TXT_CHAT_PADDING * 2;
            size.y = txtChatRt.sizeDelta.y + TXT_CHAT_PADDING * 2;
            
            // LayoutRebuilder.ForceRebuildLayoutImmediate(txtChatRt);

            GameObject objImgBg = KtUnity.findObj(gameObject, "imgChatBg");
            RectTransform bgTf = objImgBg.GetComponent<RectTransform>();

            // 重置txtBg大小
            bgTf.sizeDelta = size;
            txtChatRt.localPosition = new Vector3(TXT_CHAT_PADDING, -TXT_CHAT_PADDING, 0);
            
            float objH = (-bgTf.localPosition.y) + bgTf.sizeDelta.y + TXT_CHAT_PADDING * 2 + CONTENT_SPACE;
            
            GameObject objImgAvatarBg = KtUnity.findObj(gameObject, "imgAvatarBg");
            var objImgAvatarBgTr = objImgAvatarBg.GetComponent<RectTransform>();
            float avatarH = objImgAvatarBgTr.sizeDelta.y;
            if (objH < avatarH)
            {
                objH = avatarH;
            }

            RectTransform tf = gameObject.GetComponent<RectTransform>();
            tf.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, objH);

            KtUnity.findObj(gameObject, "txtLv").GetComponent<TMP_Text>().text = info.lv + "";
            KtUnity.findObj(gameObject, "txtName").GetComponent<TMP_Text>().text = info.nickname;
            KtUnity.findObj(gameObject, "txtTime").GetComponent<TMP_Text>().text = 
                "[" + KtTime.formatTimpTime(info.time, KtTime.FORMAT_HH_MM_MM_DD) + "]";
        }

        public void refreash()
        {
            m_sizeFitter.SetLayoutVertical();
        }
    }
}