namespace kf
{
    // ui层级 越大显示越靠前
    public enum EUILayer
    {
        // 普通
        Nor,
        // 飘字提示
        Tips,
        // 引导
        Guide,
        // 弹框
        Alert,
        // 加载
        Loading,
    }
}