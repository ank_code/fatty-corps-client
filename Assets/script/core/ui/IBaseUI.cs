using System;

namespace kf
{
    public interface IBaseUI
    {
        ////// 生命周期

        public void onCreate(IUIParam param);
        // 显示
        public void onShow();
        // 隐藏
        public void onHide();
        // 关闭
        public void close();
        
        ////// 个性化配置
        
        // 层级，默认nor
        public EUILayer getUILayer();

    }
}