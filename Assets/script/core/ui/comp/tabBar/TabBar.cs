using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * 切页bar控件
 * 结构
 *  obj
 *      - tab1
 *      - tab2
 *      ...
 *      - tabN
 *      
 *  外部使用流程：1、创建TabBar，2、增加TabBar中自定义切页Tab，其中自定义切页Tab继承自BaseTab，并实现onClick方法
 *              3、在点击Tab时，调用Tab的base.onClickTab方法
 */
public class TabBar<T> where T : ABaseTab
{
    private List<T> m_tabs = new List<T>();

    public void addTab(T tab) {
        m_tabs.Add(tab);
        tab.setOnClickLinstener(() => { onClickTab(tab); });
    }

    public List<T> getTabs() {
        return m_tabs;
    }

    public void onClickTab(T dTab) {
        if (dTab.getTabState() == ABaseTab.EState.Focus) {
            return;
        }

        dTab.setTabState(ABaseTab.EState.Focus);

        foreach (ABaseTab tab in m_tabs)
        {
            if (tab != dTab) {
                if (tab.getTabState() == ABaseTab.EState.Focus) {
                    tab.setTabState(ABaseTab.EState.LoseFocus);
                }
            }
        }
    }
}
