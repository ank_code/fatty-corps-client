using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace script.game.ui.battle.state
{
    public class PlayCmdQueue
    {
        public abstract class APlayCmd
        {
            public Action keyFrameCallback = null;
        }

        public class PlayAnimCmd : APlayCmd
        {
            public string animName = "";
        }
        
        public class PlayIdleCmd : APlayCmd
        {
        }

        public class PlayMoveToCmd : APlayCmd
        {
            public Vector3 pos;
        }

        public class PlayMoveToOrgCmd : APlayCmd
        {
            
        }

        private Queue<APlayCmd> m_cmds = new Queue<APlayCmd>();
        
        public void push(APlayCmd cmd)
        {
            m_cmds.Enqueue(cmd);
        }

        public APlayCmd pop()
        {
            return m_cmds.Dequeue();
        }

        public APlayCmd last()
        {
            return m_cmds.Peek();
        }

        public bool empty()
        {
            return m_cmds.Count <= 0;
        }
    }
}