using ak;
using kf;
using TMPro;
using UnityEngine;

namespace script.game.ui.bag.prop
{
    public class PropElem
    {
        private GameObject m_parent;

        private const string PREFAB_PATH = "Assets/res/prefab/hero/detail/prop/propElem.prefab";
        
        public void init(GameObject parent, string name, string val)
        {
            GameObject obj = ResMgr.getInstance().load<GameObject>(PREFAB_PATH, true);
            obj.transform.parent = parent.transform;
            obj.transform.localPosition = Vector3.zero;

            KtUnity.findObj(obj, "txtName").GetComponent<TMP_Text>().text = name;
            KtUnity.findObj(obj, "txtVal").GetComponent<TMP_Text>().text = val;
        }
    }
}