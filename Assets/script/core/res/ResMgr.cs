﻿using System;
using UnityEngine;
using System.IO;
using UnityEditor;
using YooAsset;
using Object = UnityEngine.Object;

namespace kf
{
    /**
     * 资源管理
     * 加载流程
     * 1、编辑器环境下
     *      开发模式时，从editor的Assets中加载
     *      发布模式时，从yoo/xxx目录下获取ab文件加载资源
     * 2、打包环境下
     *      从yoo/xxx目录下获取ab文件加载资源
     */
    public class ResMgr : Singleton<ResMgr>
    {
        private bool m_dev = true;

        // 是否从editor中加载资源
        public void setEditor(bool b)
        {
            m_dev = b;
        }

        public T loadFromResources<T>(string path) where T : Object
        {
            return loadFromResources<T>(path, true);
        }
        
        public T loadFromResources<T>(string path, bool ins) where T : Object
        {
            T objRef = Resources.Load<T>(path);

            if (objRef == null)
            {
                Debug.LogError("无法加载资源:" + path + "!");
                return objRef;
            }

            return ins ? (T)Object.Instantiate(objRef) : objRef;
        }

        // 加载文件,注意：微信不支持异步加载
        public byte[] loadRawFile(string path, bool async = false, Action<System.Object> callback = null)
        {
#if UNITY_EDITOR
            if (m_dev)
            {
                var btyes = File.ReadAllBytes(path);
                if (async)
                {
                    if (callback != null)
                    {
                        callback(btyes);
                    }
                }

                return btyes;
            }
#endif
            return yooAssetLoadRawFile(path, async, callback);
        }

        private byte[] yooAssetLoadRawFile(string path, bool async, Action<System.Object> callback)
        {
            if (async)
            {
                var handle = YooAssets.LoadRawFileAsync(path);
                handle.Completed += (RawFileOperationHandle handle) => { callback(handle.GetRawFileData()); };
            }
            else
            {
                {
                    return YooAssets.LoadRawFileSync(path).GetRawFileData();
                }
            }

            return null;
        }

        public T load<T>(string path, bool needIns = false, bool async = false, Action<Object> onLoaded = null) where T : Object
        {
            T objRef = null;
#if UNITY_EDITOR
            if (m_dev)
            {
                // 编辑器下从Assets中加载
                objRef = loadFromAssets<T>(path);
                var obj = objRef;
                if (needIns)
                {
                    obj = (T)Object.Instantiate(objRef);
                }
                
                if (async)
                {
                    onLoaded(obj);
                }
                else
                {
                    return obj;
                }

                return null;
            }
#endif
            return yooAssetLoad<T>(path, needIns, async, onLoaded);
        }

        private T yooAssetLoad<T>(string path, bool prefab, bool async, Action<Object> onLoaded) where T : Object
        {
            if (async)
            {
                AssetOperationHandle handle = YooAssets.LoadAssetAsync<T>(path);
                handle.Completed += (AssetOperationHandle handle) =>
                {
                    if (onLoaded != null)
                    {
                        if (prefab)
                        {
                            onLoaded(handle.InstantiateSync());
                        }
                        else
                        {
                            onLoaded((T)handle.AssetObject);
                        }
                    }
                };
            } else
            {
                if (prefab)
                {
                    return (T)Object.Instantiate(YooAssets.LoadAssetSync<T>(path).AssetObject);
                }
                else
                {
                    return (T)YooAssets.LoadAssetSync<T>(path).AssetObject;
                }
                
            }

            return null;
        }

        // editor模式下可用
        private T loadFromAssets<T>(string path) where T : Object
        {
#if UNITY_EDITOR
            T t = AssetDatabase.LoadAssetAtPath<T>(path);
            
            if (t == null)
            {
                Debug.LogError("无法加载资源:" + path + "!");
            }

            return t;
#else
            return null;
#endif
            
        }
    }
}