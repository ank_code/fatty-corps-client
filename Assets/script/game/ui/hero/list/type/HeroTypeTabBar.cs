using System;
using ak;
using kf;
using script.game.enums;
using script.game.enums.item;
using UnityEngine;

namespace script.game.ui.bag.type
{
    public class HeroTypeTabBar : MonoBehaviour
    {
        private TabBar<HeroTypeTab> m_tabBar = new TabBar<HeroTypeTab>();

        private void Awake()
        {
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                var objTab = gameObject.transform.GetChild(i).gameObject;
                HeroTypeTab tab = new HeroTypeTab();
                HeroUICampUtil.EUICamp camp = (HeroUICampUtil.EUICamp)Enum.GetValues(typeof(HeroUICampUtil.EUICamp)).GetValue(i);
                tab.init(objTab, camp);
                m_tabBar.addTab(tab);
            }
            
            m_tabBar.getTabs()[0].setTabState(ABaseTab.EState.Focus);
        }
    }
}