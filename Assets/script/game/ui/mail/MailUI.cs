using System.Collections.Generic;
using ak;
using kf;
using script.game.enums;
using script.game.net.http;
using script.game.net.msg.client.hero;
using script.game.ui.award;
using SuperScrollView;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.mail
{
    public class MailUI : GameUI
    {
        private GameObject m_objScrollView;
        private RectTransform m_tipRT;
        private LoopListView2 m_loopListView;
        private List<DMail> m_mails = new List<DMail>();
        private Dictionary<int, MailListElem> m_mailElems = new Dictionary<int, MailListElem>();

        private bool m_initLoopList = false;
        
        public override void onCreate(IUIParam param)
        {
            base.onCreate(param);

            var objTip = KtUnity.findObj(gameObject, "tip");
            m_tipRT = objTip.GetComponent<RectTransform>();
            
            var objBtnClose = KtUnity.findObj(gameObject, "btnClose");
            KtUI.addListenerAutoScale(objBtnClose, () =>
            {
                this.close();
            });

            var objBtnDelAllRead = KtUnity.findObj(gameObject, "btnDelAllRead");
            var objBtnDelAllReadClickArea = KtUnity.findObj(objBtnDelAllRead, "btnClickArea");
            
            KtUI.addListener(objBtnDelAllRead, objBtnDelAllReadClickArea, () =>
            {
                GameRequest.getInstance().delAllReadMails((rep) =>
                {
                    Debug.Log("aktest 删除成功");
                    for (var i = 0; i < rep.ids.Count; i++)
                    {
                        for (var j = 0; j < m_mails.Count; j++)
                        {
                            if (m_mails[j].id == rep.ids[i])
                            {
                                m_mails.RemoveAt(j);
                                break;
                            }
                        }
                    }
                    
                    initData();
                });
            });
            
            var objBtnGetAllAttachment = KtUnity.findObj(gameObject, "btnGetAllAttachment");
            var objBtnGetAllAttachmentClickArea = KtUnity.findObj(objBtnGetAllAttachment, "btnClickArea");
            
            KtUI.addListener(objBtnGetAllAttachment, objBtnGetAllAttachmentClickArea, () =>
            {
                GameRequest.getInstance().getAllAttachment((rep) =>
                {
                    if (m_mails != null && m_mails.Count <= 0)
                    {
                        AlertUtil.alertInfo("没有附件可以领取了");
                        return;
                    }

                    if (rep.attachment == null || rep.attachment.Count == 0)
                    {
                        AlertUtil.alertInfo("没有附件可以领取了");
                        return;
                    }

                    for (var i = 0; i < m_mails.Count; i++)
                    {
                        m_mails[i].getAttachment = true;
                        m_mailElems[m_mails[i].id].refreshAttach(true);
                    }

                    AwardUI.Param p = new AwardUI.Param();
                    p.items = rep.attachment;
                    UIMgr.getInstance().show<AwardUI>(p);
                });
            });
            
            m_objScrollView = KtUnity.findObj(gameObject, "scrollView");
            m_loopListView = m_objScrollView.GetComponent<LoopListView2>();
        }

        public override void onShow()
        {
            base.onShow();
            // 使用布局中的子物体如果有contentSizeFitter或其它会改变大小带来冲突，导致不使用下面重置代码就会排列错乱
            LayoutRebuilder.ForceRebuildLayoutImmediate(m_tipRT);
            getMailList();
        }

        private void getMailList()
        {
            GameRequest.getInstance().getMailList((rep) =>
            {
                m_mails = rep.mails;
                initData();
            });
        }

        private void initData()
        {
            if (m_initLoopList)
            {
                m_loopListView.SetListItemCount(m_mails.Count, false);
                m_loopListView.RefreshAllShownItem();
            }
            else
            {
                m_loopListView.InitListView(m_mails.Count, onGetItem);
                m_initLoopList = true;
            }
        }
        
        private LoopListViewItem2 onGetItem(LoopListView2 view2, int rowIndex)
        {
            if (rowIndex < 0)
            {
                return null;
            }

            var row = view2.NewListViewItem("mailListElem");
            row.gameObject.name = "row_" + rowIndex;
            
            var mailElem = KtUnity.addComponent<MailListElem>(row.gameObject);
            if (rowIndex >= m_mails.Count)
            {
                return null;
            }

            if (m_mailElems.ContainsKey(m_mails[rowIndex].id))
            {
                m_mailElems.Remove(m_mails[rowIndex].id);
            }

            m_mailElems.Add(m_mails[rowIndex].id, mailElem);
            mailElem.init(m_mails[rowIndex], rowIndex);
            return row;
        }

        public override void onEvent(int eId, object[] objs)
        {
            base.onEvent(eId, objs);
            EEvent e = (EEvent)eId;
            switch (e)
            {
                case EEvent.OnMailRead:
                {
                    int mailId = (int)objs[0];
                    int idx = (int)objs[1];
                    m_mailElems[mailId].refreshRead(true);
                    m_mails[idx].read = true;
                    break;
                }
                case EEvent.OnMailGetAttachment:
                {
                    int mailId = (int)objs[0];
                    int idx = (int)objs[1];
                    m_mailElems[mailId].refreshAttach(true);
                    m_mails[idx].getAttachment = true;
                    break;
                }
                case EEvent.OnMailDel:
                {
                    int mailId = (int)objs[0];
                    int idx = (int)objs[1];
                    m_mails.RemoveAt(idx);
                    initData();
                    break;
                }
            }
        }
        
    }
}