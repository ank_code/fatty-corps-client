using ak;
using DG.Tweening;
using kf;
using script.game.mgr;
using script.game.ui.bag.comp;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.bag.skill
{
    public class SkillUI : GameUI
    {
        private int m_skillTypeId;
        private GameObject m_objBody;
        private const float ENTER_ANIM_TIME = 0.2f;

        public class Param : IUIParam
        {
            public int skillTypeId;
        }
        
        public override void onCreate(IUIParam param)
        {
            base.onCreate(param);
            Param p = (Param)param;
            m_skillTypeId = p.skillTypeId;
            
            KtUI.addListener(KtUnity.findObj(gameObject, "btnClose"), () =>
            {
                this.close();
            });

            m_objBody = KtUnity.findObj(gameObject, "bg");

            var cfgSkill = CfgMgr.getInstance().getSkill(m_skillTypeId);
            
            // icon
            GameObject objSkillIcon = KtUnity.findObj(gameObject, "skillIcon");
            SkillIcon si = new SkillIcon();
            si.init(objSkillIcon);
            si.setUnLock(true);
            si.setSkillTypeId(m_skillTypeId);

            KtUnity.findObj(gameObject, "txtName").GetComponent<TMP_Text>().text = cfgSkill.name;
            KtUnity.findObj(gameObject, "txtIntro").GetComponent<TMP_Text>().text = cfgSkill.intro;

            // 刷新
            var rt = m_objBody.GetComponent<RectTransform>();
            LayoutRebuilder.ForceRebuildLayoutImmediate(rt);
        }

        public override void onShow()
        {
            base.onShow();
            m_objBody.transform.DOScaleX(0.1f, ENTER_ANIM_TIME).From();
            m_objBody.transform.DOScaleY(0.1f, ENTER_ANIM_TIME).From();
        }
    }
}