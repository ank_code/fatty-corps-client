using System;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using UnityEngine;
using Object = System.Object;

namespace kf.anim
{
    public class SpineAnim
    {
        public enum EType
        {
            Sprite,
            UI,
        }

        private EType m_type = EType.UI;
        private SkeletonDataAsset m_sda;
        private Material m_mat;
        private SkeletonGraphic m_sg;
        private float m_timeScale = 1.0f;
        private string m_animPath;
        private string m_curAnimName;
        private bool m_curAnimLoop = false;
        // 是否播放过关键事件帧，仅在playOnce时有效
        private bool m_playedKeyFrame = false;

        public enum ECallBackEvent
        {
            KeyFrame,
            Complete
        }
        
        public class CallbackEvent
        {
            public ECallBackEvent e;
            public Object param = null;
            // 是否播放过关键事件帧，仅在playOnce时有效
            public bool playedKeyFrame = false;
        }
        private Action<CallbackEvent> m_onceCompleteCallback;
        private Action<SpineAnim> m_loadComplete = null;
        
        private const string SOUND_EVENT_NAME = "sound";
        private const string ANIM_PRE = "res/audio/anim/";
        private const string AUDIO_SUFFIX = ".mp3";
        
        // webgl不支持同步加载
        public SpineAnim(bool sync, string animPath, GameObject parent, EType type = EType.UI, Action<SpineAnim> loadComplete = null)
        {
            m_type = type;
            m_loadComplete = loadComplete;
            m_animPath = animPath;

            if (sync)
            {
                initSync(animPath, parent, type);
            }
            else
            {
                initAsync(animPath, parent, type);
            }
        }

        private void initSync(string animPath, GameObject parent, EType type)
        {
            switch (type)
            {
                case EType.UI:
                    initAsUISync(animPath, parent);
                    break;
                default:
                    throw new Exception("暂不支持的类型");
            }

            
            initEvent();
        }

        private void initAsync(string animPath, GameObject parent, EType type)
        {
            switch (type)
            {
                case EType.UI:
                    initAsUIAsync(animPath, parent, () =>
                    {
                        initEvent();
                        if (m_loadComplete == null)
                        {
                            throw new Exception("异步加载必须有回掉函数");
                        }
                        
                        m_loadComplete(this);
                    });
                    break;
                default:
                    throw new Exception("暂不支持的类型");
            }
        } 

        private void initAsUIAsync(string path, GameObject parent, Action complete)
        {
            ResMgr.getInstance().load<SkeletonDataAsset>(path + "_SkeletonData.asset", false, true, (sda) =>
            {
                m_sda = (SkeletonDataAsset)sda;
                ResMgr.getInstance().load<Material>(path + "_Material.mat", false, true, (mat) =>
                {
                    m_mat = (Material)mat;

                    initAsUI(parent);
                    complete();
                });
            });
        }
        
        private void initAsUISync(string path, GameObject parent)
        {
            m_sda = ResMgr.getInstance().load<SkeletonDataAsset>(path + "_SkeletonData.asset");
            m_mat = ResMgr.getInstance().load<Material>(path + "_Material.mat");

            initAsUI(parent);
        }

        private void initAsUI(GameObject parent)
        {
            m_sda.GetSkeletonData(false); // Preload SkeletonDataAsset.

            m_sg = SkeletonGraphic.NewSkeletonGraphicGameObject(m_sda, parent.transform, m_mat); // Spawn a new SkeletonGraphic GameObject.
            m_sg.gameObject.name = "objSpineAnim";
            
            var rt = m_sg.gameObject.GetComponent<RectTransform>();
            rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 1);
            rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 1);

            // Extra Stuff
            m_sg.Initialize(false);
        }
        
        private void initEvent()
        {
            if (m_sg != null && m_sg.AnimationState != null)
            {
                m_sg.AnimationState.Event += onEvent;
                m_sg.AnimationState.Complete += onLocalPlayOver;
            }
            else
            {
                Debug.LogError("aktest spine anim initEvent error!");
            }
        }
        
        public List<string> getAnimNameList() {
            List<string> nameList = new List<string>();
            for (int i = 0; i < m_sg.SkeletonData.Animations.Items.Length; i++) {
                nameList.Add(m_sg.SkeletonData.Animations.Items[i].Name);
            }

            return nameList;
        }

        public GameObject getObj()
        {
            return m_sg.gameObject;
        }

        public SkeletonGraphic getSG()
        {
            return m_sg;
        }

        public void setActive(bool active)
        {
            getObj().SetActive(active);
        }

        public void setScale(float scale)
        {
            getObj().transform.localScale *= scale;
        }

        public void playLoop(string animName = "")
        {
            animName = procAnimName(animName);
            m_curAnimName = animName;
            m_curAnimLoop = true;
            m_onceCompleteCallback = null;
            m_sg.AnimationState.SetAnimation(0, animName, m_curAnimLoop);
            
            // updateSize();
        }

        public void playOnceByFirst(Action<CallbackEvent> onComplete)
        {
            playOnce("", onComplete);
        }
        
        public void playOnce(string animName, Action<CallbackEvent> callback)
        {
            animName = procAnimName(animName);
            m_curAnimName = animName;
            m_onceCompleteCallback = callback;
            m_curAnimLoop = false;
            m_playedKeyFrame = false;
            
            // 不清理可能会出现上次播放该动画时遗留一帧未播放干净，再播放会直接从该帧开始，导致直接触发动画结束
            m_sg.AnimationState.ClearTrack(0);
            m_sg.AnimationState.SetAnimation(0, animName, m_curAnimLoop);

            // updateSize();
        }

        private void updateSize()
        {
            var boundsSize = m_sg.gameObject.GetComponent<SpriteRenderer>().bounds.size;
            var rt = m_sg.gameObject.GetComponent<RectTransform>();
            rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, boundsSize.x);
            rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, boundsSize.y);
        }

        private string procAnimName(string animName)
        {
            if (string.IsNullOrEmpty(animName))
            {
                return firstAnimName();
            }
            else
            {
                if (!findAnim(animName))
                {
                    Debug.LogError("SpineAnim error: can not find animName=" + animName + " in anim="+ m_animPath);
                    return "";
                }
            }

            return animName;
        }

        private void onLocalPlayOver(TrackEntry entry)
        {
            if (m_onceCompleteCallback != null && !m_curAnimLoop)
            {
                CallbackEvent ce = new CallbackEvent();
                ce.e = ECallBackEvent.Complete;
                ce.playedKeyFrame = m_playedKeyFrame;

                int oldCallbackHash = m_onceCompleteCallback.GetHashCode();
                m_onceCompleteCallback(ce);
                
                // 有可能在回调函数中，重新设置了回调函数
                if (m_onceCompleteCallback !=null && m_onceCompleteCallback.GetHashCode() == oldCallbackHash)
                {
                    m_onceCompleteCallback = null;
                }
            }
        }

        public void setTimeScale(float s)
        {
            if (s < 0 || s > 1)
            {
                return;
            }

            m_timeScale = s;
            if (m_sg != null)
            {
                m_sg.timeScale = m_timeScale;
            }
        }
        
        public void stop()
        {
            if (m_sg != null)
            {
                m_sg.timeScale = 0;
            }
        }

        public void resume()
        {
            setTimeScale(m_timeScale);
        }

        public void setSkin(string skinName) {
            m_sg.initialSkinName = skinName;
            // m_sg.Initialize(false);
            m_sg.Skeleton.SetSlotsToSetupPose();
        }

        public bool findAnim(string animName)
        {
            if (m_sg == null || m_sg.SkeletonData == null)
            {
                return false;
            }

            return m_sg.SkeletonData.FindAnimation(animName) != null;
        }

        public string firstAnimName()
        {
            var len = m_sg.SkeletonData.Animations.Items.Length;
            if (len > 0)
            {
                return m_sg.SkeletonData.Animations.Items[0].Name;
            }

            throw new Exception("SpineAnim error: no animation in path:" + m_animPath);
        }
        
        private void onEvent(TrackEntry trackEntry, Spine.Event e)
        {
            string eventName = KtFile.getFileNameByPath(e.Data.Name);
            if (eventName.Length >= SOUND_EVENT_NAME.Length)
            {
                string pre = eventName.Substring(0, SOUND_EVENT_NAME.Length);

                if (pre == SOUND_EVENT_NAME)
                {
                    string fileName = KtFile.getFileNameByPath(e.Data.AudioPath);
                    string path = ANIM_PRE + fileName.Substring(0, fileName.Length - AUDIO_SUFFIX.Length);
                    AudioMgr.getInstance().playSound(path, AudioMgr.SOUND_TYPE.SOUND_TYPE_OVERLAP);
                }
            }
            else
            {
                if (m_onceCompleteCallback != null)
                {
                    m_playedKeyFrame = true;
                    
                    CallbackEvent ce = new CallbackEvent();
                    ce.e = ECallBackEvent.KeyFrame;
                    ce.param = eventName;

                    m_onceCompleteCallback(ce);
                }
            }
        }
    }
}