using System;
using System.Collections.Generic;
using script.game.net.msg.server.hero;
using script.game.ui.battle;

namespace script.game.obj.config
{
    public class CfgMainGuanqia : BaseCfg
    {
        public string name;
        public int chapter;
        public int no;
        public int unlockLv;
        public string addExp;
        public string awards;
        public string enemies;

        public List<int> getEnemyTypeIds()
        {
            const string SPLIT_KEY = ",";
            const int STRUCT_SIZE = 4;
            // const int STRUCT_TYPE_ID_INDEX = 1;
            
            var strs = enemies.Split(SPLIT_KEY);
            List<int> typeIdList = new List<int>();
            List<int> posList = new List<int>();

            for (var i= 0; i < strs.Length; i++)
            {
                if (i % STRUCT_SIZE == 0)
                {
                    posList.Add(Int32.Parse(strs[i]));
                } else if (i % STRUCT_SIZE == 1)
                {
                    typeIdList.Add(Int32.Parse(strs[i]));
                }
            }

            List<int> retLineupList = new List<int>();
            for (int i = 0; i < GameConst.MAX_TEAM_NUM; i++)
            {
                int posNo = i + 1;
                if (posList.Contains(posNo))
                {
                    int index = posList.FindIndex(new Predicate<int>((v) =>
                    {
                        return v == posNo;
                    }));
                    retLineupList.Add(typeIdList[index]);
                }
                else
                {
                    retLineupList.Add(0);
                }
            }

            return retLineupList;
        }
    }
}