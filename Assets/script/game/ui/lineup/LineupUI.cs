using System;
using System.Collections.Generic;
using System.Linq;
using ak;
using kf;
using script.game.enums;
using script.game.mgr;
using script.game.net.http;
using script.game.net.msg.client.hero;
using script.game.net.msg.server.hero;
using script.game.ui.bag.type;
using script.game.ui.comm.comp;
using script.game.ui.hero;
using script.game.ui.hero.detail;
using SuperScrollView;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace script.game.ui.lineup
{
    public class LineupUI : BaseUI
    {
        public class UIParam : IUIParam
        {
            public List<HeroShort> bagHeroList;
            public List<int> enemyHeroTypeIdList;
            public Action onClickBattle;
            public string title;
        }
        
        private LoopListView2 m_loopListView;
        private GameObject m_objScrollView;
        
        private GameObject m_objTypeTabBar;
        
        private CurrencyInfo m_currencyInfo;

        private List<HeroShort> m_bagHeroList;
        private List<HeroShort> m_curHeroList = new List<HeroShort>();

        private LineupTeam m_leftTeam = new LineupTeam();
        private LineupTeam m_rightTeam = new LineupTeam();

        private List<LineupHeroItem> m_items = new List<LineupHeroItem>();

        private bool m_initLoopList = false;
        private Action m_onClickBattle;
        
        private const int PER_COL_ITEM_NUM = 1;
        
        public static void open(Action onClickBattle, string title, List<int> enemyHeroTypeIdList = null)
        {
            GameRequest.getInstance().getHeroList((rep) =>
            {
                UIParam param = new UIParam();
                param.bagHeroList = rep.heroList;
                param.enemyHeroTypeIdList = enemyHeroTypeIdList;
                param.onClickBattle = onClickBattle;
                param.title = title;
                UIMgr.getInstance().show<LineupUI>(param);
            });
        }
        
        public override void onCreate(IUIParam param)
        {
            m_bagHeroList = ((UIParam)param).bagHeroList;
            m_onClickBattle = ((UIParam)param).onClickBattle;
            string title = ((UIParam)param).title;

            KtUnity.findObj(gameObject, "txtTitle").GetComponent<TMP_Text>().text = title;

            GameObject m_objBtnClose = KtUnity.findObj(gameObject, "btnClose");
            KtUI.addListener(m_objBtnClose, () =>
            {
                this.close();
            });
            
            ////////// mid
            var leftLineup = Player.getInstance().info.getHSLineup(m_bagHeroList);
            GameObject objLeftTeam = KtUnity.findObj(gameObject, "leftTeam");
            m_leftTeam.init(objLeftTeam, leftLineup, true);

            GameObject objEnemyTeam = KtUnity.findObj(gameObject, "rightTeam");
            var enemyHeroShorts = HeroShort.makeByTypeIds(((UIParam)param).enemyHeroTypeIdList);
            m_rightTeam.init(objEnemyTeam, enemyHeroShorts, false);
            
            ////////// bottom
            m_objScrollView = KtUnity.findObj(gameObject, "svHeroList");
            m_loopListView = m_objScrollView.GetComponent<LoopListView2>();
            
            // type tabbar
            m_objTypeTabBar = KtUnity.findObj(gameObject, "heroTypeSwitchBar");
            KtUnity.addComponent<HeroTypeTabBar>(m_objTypeTabBar);
            
            // btn
            GameObject objBtnQuickArraying = KtUnity.findObj(gameObject, "btnQuickArraying");
            KtUI.addListener(objBtnQuickArraying, () =>
            {
                m_leftTeam.clear();

                for (var i = 0; i < m_bagHeroList.Count; i++)
                {
                    if (i >= GameConst.MAX_TEAM_NUM)
                    {
                        break;
                    }

                    GameEvent.sendEvent(EEvent.OnLineupHeroItemClick, new object[] {m_bagHeroList[i], true});
                }
            }, EventTriggerType.PointerDown, true, true);
            
            GameObject objBtnBattle = KtUnity.findObj(gameObject, "btnBattle");
            KtUI.addListener(objBtnBattle, () =>
            {
                var leftHeroIds = m_leftTeam.getLineup();

                bool empty = true;
                for (var i = 0; i < leftHeroIds.Count; i++)
                {
                    if (leftHeroIds[i] > 0)
                    {
                        empty = false;
                        break;
                    }
                }
                
                if (empty)
                {
                    AlertUtil.alertInfo("请选择上阵英雄");
                    return;
                }
                
                Action doBattle = () =>
                {
                    if (m_onClickBattle != null)
                    {
                        m_onClickBattle();
                    }
                    
                    this.close();
                };

                // 有变化则设置
                if (!Player.getInstance().info.getDefLineup().SequenceEqual(leftHeroIds))
                {
                    CSetLineup req = new CSetLineup();
                    req.heroIds = leftHeroIds;
                    GameRequest.getInstance().setLineup(req, lineup =>
                    {
                        GameEvent.sendEvent(EEvent.OnSetLineupSuccess, new object[] { leftHeroIds });
                        Player.getInstance().refresh();
                        doBattle();
                    });
                }
                else
                {
                    doBattle();
                }
            }, EventTriggerType.PointerDown, true, true);
        }

        public override void onShow()
        {
            // 入场动画
        }

        private void initData(HeroUICampUtil.EUICamp uiCamp)
        {
            m_curHeroList.Clear();
            
            if (uiCamp == HeroUICampUtil.EUICamp.All)
            {
                m_curHeroList.AddRange(m_bagHeroList);
            }
            else
            {
                for (var i = 0; i < m_bagHeroList.Count; i++)
                {
                    var cfgHero = CfgMgr.getInstance().getHero(m_bagHeroList[i].typeId);
                    int campIndex = (int)cfgHero.camp;
                    int showCampIndex = (int)uiCamp - 1;
                    if (campIndex == showCampIndex)
                    {
                        m_curHeroList.Add(m_bagHeroList[i]);
                    }
                }
            }
            
            int rows = m_curHeroList.Count / PER_COL_ITEM_NUM;
            if (m_curHeroList.Count % PER_COL_ITEM_NUM > 0)
            {
                rows++;
            }

            if (m_initLoopList)
            {
                m_loopListView.SetListItemCount(rows, false);
                m_loopListView.RefreshAllShownItem();
            }
            else
            {
                m_loopListView.InitListView(rows, onGetItem);
                m_initLoopList = true;
            }
        }
        
        private LoopListViewItem2 onGetItem(LoopListView2 view2, int rowIndex)
        {
            if (rowIndex < 0)
            {
                return null;
            }

            List<int> lineup = m_leftTeam.getLineup();

            var row = view2.NewListViewItem("heroElemColumn");
            row.gameObject.name = "row_" + rowIndex;
            
            for (int i = 0; i < row.transform.childCount; i++)
            {
                GameObject objItem = row.transform.GetChild(i).gameObject;
                var item = KtUnity.addComponent<LineupHeroItem>(objItem);
                item.setLineupUI(this);
                
                int index = rowIndex * PER_COL_ITEM_NUM + i;
                
                if (index >= m_curHeroList.Count)
                {
                    item.setVisible(false);
                    item.showName(false);
                    continue;
                }
                
                objItem.name = "hero_" + index;
                HeroShort heroShort = m_curHeroList[index];
                item.reload(heroShort, index);
                
                // 初始化选中状态
                item.setClicked(lineup.Contains(item.getId()));

                if (!m_items.Contains(item))
                {
                    m_items.Add(item);
                }
            }
            
            return row;
        }

        public bool isFull()
        {
            return m_leftTeam.isFull();
        }
        
        public override void onEvent(int eId, object[] objs)
        {
            EEvent e = (EEvent)eId;
            switch (e)
            {
                case EEvent.OnHeroSwitchType:
                {
                    HeroUICampUtil.EUICamp type = (HeroUICampUtil.EUICamp)objs[0];
                    initData(type);
                    break;
                }
                case EEvent.OnLineupHeroItemClick:
                {
                    HeroShort heroShort = (HeroShort)objs[0];
                    bool selected = (bool)objs[1];

                    if (selected)
                    {
                        m_leftTeam.add(heroShort);
                    }
                    else
                    {
                        m_leftTeam.remove(heroShort.id);
                    }
                    
                    for (var i = 0; i < m_items.Count; i++)
                    {
                        if (m_items[i].getId() == heroShort.id)
                        {
                            m_items[i].setClicked(selected);
                        }
                    }
                    
                    break;
                }
                case EEvent.OnLineupHeroDragEnd:
                {
                    int lPosNo = (int)objs[0];
                    Vector3 pos = (Vector3)objs[1];
                    m_leftTeam.onHeroDragEnd(lPosNo, pos);
                    break;
                }
            }
        }
        
        private List<int> getSelfLineupHeroTypeIds()
        {
            List<int> heroTypeIds = new List<int>();
            List<int> lineup = Player.getInstance().info.getDefLineup();
            
            if (lineup != null)
            {
                for (var i = 0; i < lineup.Count; i++)
                {
                    if (lineup[i] == 0)
                    {
                        heroTypeIds.Add(0);
                    }
                    else
                    {
                        var heroShort = HeroShort.findInBagHeroes(m_bagHeroList, lineup[i]);
                        heroTypeIds.Add(heroShort.typeId);
                    }
                }
            }
 
            return heroTypeIds;
        }
    }
}