using System.Collections.Generic;
using ak;
using DG.Tweening;
using kf;
using script.game.mgr;
using script.game.net.msg.server.item;
using script.game.ui.comm.comp;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.award
{
    public class HangAwardUI : GameUI
    {
        public class Param : IUIParam
        {
            public string hangTime;
            public List<DItem> items;
        }

        private GameObject m_objBody;

        private const string HANG_TXT_PREFIX = "挂机时间：";
        private const float ENTER_ANIM_TIME = 0.2f;
        
        public override void onCreate(IUIParam param)
        {
            base.onCreate(param);
            Param p = (Param)param;
            if (p.items != null && p.items.Count > 0)
            {
                var objAwards = KtUnity.findObj(gameObject, "awards");
                for (var i = 0; i < p.items.Count; i++)
                {
                    ItemAward award = new ItemAward();
                    award.init(objAwards, p.items[i].typeId, p.items[i].num, false);
                }
            
                GameObject objSvItems = KtUnity.findObj(gameObject, "svAwards");
                GameObject objContent = KtUnity.findObj(objSvItems, "Content");
                var contentRT = objContent.GetComponent<RectTransform>();
                var awardsRT = objAwards.GetComponent<RectTransform>();
            
                LayoutRebuilder.ForceRebuildLayoutImmediate(awardsRT);
            
                contentRT.sizeDelta = new Vector2(awardsRT.rect.width, awardsRT.rect.height);
                contentRT.localPosition = Vector3.zero;
            }
            
            KtUI.addListenerAutoScale(KtUnity.findObj(gameObject, "btnClose"), onClickClose);

            var objBtnGet = KtUnity.findObj(gameObject, "btnGet");
            KtUI.addListener(objBtnGet, KtUnity.findObj(objBtnGet, "btnClickArea"), onClickClose);
            
            var objBtnVideoGet = KtUnity.findObj(gameObject, "btnVideoGet");
            KtUI.addListener(objBtnVideoGet, KtUnity.findObj(objBtnVideoGet, "btnClickArea"), () =>
            {
                Debug.Log("aktest 视频领取奖励");
                onClickClose();
            });

            var txtHangTime = KtUnity.findObj(gameObject, "txtHangTime").GetComponent<TMP_Text>();
            txtHangTime.text = HANG_TXT_PREFIX + p.hangTime;

            m_objBody = KtUnity.findObj(gameObject, "bg");
        }

        public override void onShow()
        {
            m_objBody.transform.DOScaleX(0.1f, ENTER_ANIM_TIME).From();
            m_objBody.transform.DOScaleY(0.1f, ENTER_ANIM_TIME).From();
        }

        private void onClickClose()
        {
            Player.getInstance().refresh();
            this.close();
        }
    }
}