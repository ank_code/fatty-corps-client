using kf;
using script.game.mgr;

namespace script.game.net.http
{
    public class AccountHttp : MsgHttp
    {
        public override string getBaseUrl()
        {
            return GlobalCfg.getAccHost();
        }

        public override string getToken()
        {
            return (string)StoreMgr.getInstance().get(StoreMgr.KEY_ACC_TOKEN);
        }
    }
}