using System;
using kf;
using UnityEngine;

namespace script.game.ui.comm.comp
{
    public class CurrencyInfo
    {
        private GameObject m_obj;
        
        // 货币类型
        public enum ECurrencyType
        {
            Money,
            Coin,
        }

        private const string PREFAB_PATH = "Assets/res/prefab/commen/currency/currencyInfo.prefab";
        
        public void init(GameObject parent, Action callback)
        {
            ResMgr.getInstance().load<GameObject>(PREFAB_PATH, true, true, o =>
            {
                m_obj = (GameObject)o;
                m_obj.transform.SetParent(parent.transform);
                m_obj.transform.localScale = Vector3.one;
                m_obj.transform.localPosition = Vector3.zero;

                callback();
            });
        }

        public void add(ECurrencyType type)
        {
            ABaseCurrencyElem elem = null;
            switch (type)
            {
                case ECurrencyType.Coin:
                {
                    elem = new CoinElem();
                    break;
                }
                case ECurrencyType.Money:
                {
                    elem = new MoneyElem();
                    break;
                }
            }
            
            elem.init(m_obj);
        }
    }
}