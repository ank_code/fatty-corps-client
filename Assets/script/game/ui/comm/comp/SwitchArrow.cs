using System;
using ak;
using kf;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.hero.detail.comp
{
    // 选择箭头
    public class SwitchArrow
    {
        public enum EState
        {
            Enable,
            Unable,
        }

        public enum EDirect
        {
            Left,
            Right
        }

        private GameObject m_obj;
        private GameObject m_objImgBtn;
        private Image m_imgBtn;
        private EDirect m_direct;
        private EState m_state;
        private Action m_action;

        public void init(GameObject obj, EDirect eDirect, Action action)
        {
            m_obj = obj;
            m_direct = eDirect;
            m_action = action;
            
            m_objImgBtn = KtUnity.findObj(obj, "imgBtn");
            GameObject objBtnClickArea = KtUnity.findObj(obj, "btnClickArea");
            
            m_imgBtn = m_objImgBtn.GetComponent<Image>();

            KtUI.addLintenerNoScale(m_objImgBtn, objBtnClickArea, () =>
            {
                if (m_state.Equals(EState.Unable))
                {
                    return;
                }

                if (m_action != null)
                {
                    m_action();
                }
            });
            
            setDirect(eDirect);
        }

        public void setState(EState eState)
        {
            this.m_state = eState;
            float a = 0.4f;
            if (m_state.Equals(EState.Enable))
            {
                a = 1.0f;
            }
            
            var tintColor = m_imgBtn.color;
            m_imgBtn.color = new Color(tintColor.r, tintColor.g, tintColor.b, a);
        }

        private void setDirect(EDirect eDirect)
        {
            var rt = m_objImgBtn.GetComponent<RectTransform>();
            if (eDirect.Equals(EDirect.Left))
            {
                rt.localScale = new Vector3(-1, 1, 1);
            }
            else
            {
                rt.localScale = new Vector3(1, 1, 1);
            }
        }
    }
}