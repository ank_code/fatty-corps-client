﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kf
{
    public class SMsg<T>
    {
        public int code { get; set; }
        public string msg { get; set; }
        public T data { get; set; }
    }
}