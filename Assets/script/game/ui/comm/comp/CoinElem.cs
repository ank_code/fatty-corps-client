using script.game.enums;
using script.game.mgr;
using UnityEngine;

namespace script.game.ui.comm.comp
{
    public class CoinElem : ABaseCurrencyElem
    {
        private const string ICON_PATH = "Assets/res/img/ui/comm/currency/zuanshi.png";
        
        protected override string getIconPath()
        {
            return ICON_PATH;
        }

        protected override long getVal()
        {
            return Player.getInstance().info.coin;
        }

        protected override bool showBtnDefault()
        {
            return true;
        }

        protected override void onClickAdd()
        {
            // todo : 弹获取金币的框
            Debug.Log("普通货币加号被点击了");
            AlertUtil.alertInfo("贱强还不速速起床设计界面！");
        }
        
        
        public override void onEvent(int eId, object[] objs)
        {
            EEvent e = (EEvent)eId;

            // todo: 收到金币发生变化消息设置当前值
            switch (e)
            {
                case EEvent.OnPlayerCoinChanged:
                {
                    int v = (int)objs[0];
                    setVal(v);
                    break;
                }
            }
        }
    }
}