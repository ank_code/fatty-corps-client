﻿using System;
using System.Collections.Generic;
using System.Timers;

namespace kf
{
    class AFrameTask
    {
        public int tid;
        public Action callback;
        public int destFrame;
        public int delay;
        public int count;
        public bool compensate;

        public AFrameTask(int tid, Action callback, int destFrame, int delay, int count, bool compensate)
        {
            this.tid = tid;
            this.callback = callback;
            this.destFrame = destFrame;
            this.delay = delay;
            this.count = count;
            this.compensate = compensate;
        }
    }
}