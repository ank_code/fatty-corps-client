namespace kf
{
    public class KtFile
    {
        // 获取路径后的文件名
        // 如：path=abc/123/abc/fn，返回fn
        public static string getFileNameByPath(string path) {
            return System.IO.Path.GetFileName(path);
        }
    }
}