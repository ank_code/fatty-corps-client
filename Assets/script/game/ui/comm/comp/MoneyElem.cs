using script.game.enums;
using script.game.mgr;
using UnityEngine;

namespace script.game.ui.comm.comp
{
    public class MoneyElem : ABaseCurrencyElem
    {
        private const string ICON_PATH = "Assets/res/img/ui/comm/currency/jinbi.png";
        
        protected override string getIconPath()
        {
            return ICON_PATH;
        }

        protected override long getVal()
        {
            return Player.getInstance().info.money;
        }

        protected override bool showBtnDefault()
        {
            return false;
        }

        protected override void onClickAdd()
        {
            // todo : 弹获取金币的框
            Debug.Log("金币加号被点击了");
            AlertUtil.alertInfo("贱强还不速速起床设计界面！");
        }

        public override void onEvent(int eId, object[] objs)
        {
            EEvent e = (EEvent)eId;

            switch (e)
            {
                case EEvent.OnPlayerMoneyChanged:
                {
                    int v = (int)objs[0];
                    setVal(v);
                    break;
                }
            }
        }
    }
}