using script.game.enums;

namespace script.game.ui.bag
{
    public class HeroUtil
    {
        private const string ICON_PATH_PREFIX = "Assets/res/img/hero/icon/";
        private const string ICON_SUFFIX = ".png";
        
        private const string CAMP_BG_PATH_PREFIX = "Assets/res/img/ui/hero/camp/bg/";
        private const string CAMP_ICON_PATH_PREFIX = "Assets/res/img/ui/hero/camp/icon/";

        public static string getIconPath(int typeId)
        {
            return ICON_PATH_PREFIX + typeId + ICON_SUFFIX;
        }

        public static string getCampBgPath(ECamp camp)
        {
            return CAMP_BG_PATH_PREFIX + (int)camp + ICON_SUFFIX;
        }
        
        public static string getCampIconPath(ECamp camp)
        {
            return CAMP_ICON_PATH_PREFIX + (int)camp + ICON_SUFFIX;
        }
    }
}