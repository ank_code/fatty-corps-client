using System.Collections.Generic;

namespace script.game.net.msg.server
{
    public class RecElem
    {
        public EBehaviorCondition cnd;
        // 施法者
        public HeroPos caster;
        // 表现方式id
        public int displayId;
        // 影响效果
        public List<RecElemEffect> effects;
    }
}