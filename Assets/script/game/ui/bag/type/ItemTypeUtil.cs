using script.game.enums.item;

namespace script.game.ui.bag.type
{
    public class ItemTypeUtil
    {
        public static string getTypeName(EItemType type)
        {
            switch (type)
            {
                case EItemType.Nor:
                    return "道具";
                case EItemType.Equipment:
                    return "装备";
                case EItemType.Fragment:
                    return "碎片";
            }

            return "未知";
        }

        // public static string getSubTypeName(EItemSubType subType)
        // {
        //     switch (type)
        //     {
        //         case EItemSubType.Nor:
        //             return "道具";
        //         case EItemSubType.HeroFragment:
        //             return "装备";
        //         case EItemSubType.Coin:
        //             return "碎片";
        //         case EItemSubType.Coin:
        //             return "碎片";
        //     }
        //
        //     return "未知";
        // }
    }
}