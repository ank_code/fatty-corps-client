using System.Collections.Generic;

namespace script.game.net.msg.server.hero
{
    public class SGetHeroList
    {
        public List<HeroShort> heroList;
    }
}