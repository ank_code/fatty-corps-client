using System;
using ak;
using kf;
using script.game.net.msg.client;
using script.game.net.msg.client.chat;
using script.game.net.msg.client.hero;
using script.game.net.msg.client.item;
using script.game.net.msg.server;
using script.game.net.msg.server.chat;
using script.game.net.msg.server.hero;
using script.game.net.msg.server.item;
using script.game.ui.award;
using UnityEngine;

namespace script.game.net.http
{
    public class GameRequest : Singleton<GameRequest>
    {
        private bool m_init = false;
        private GameHttp m_http = null;
        
        const double HEART_BEAT_TIME = 10.0f;
        
        public void init()
        {
            if (m_init)
            {
                return;
            }

            m_init = true;

            GameObject obj = new GameObject("gameRequest");
            GameObject.DontDestroyOnLoad(obj);

            m_http = KtUnity.addComponent<GameHttp>(obj);
            m_http.init();
            m_http.setFailCallback(ErrHandler.onFail);
            
            // addHeartbeat();
        }
        
        public void addHeartbeat()
        {
            TimerMgr.getInstance().AddTimerTask(() =>
            {
                if (m_http.hasToken())
                {
                    GameRequest.getInstance().heartberat((rep) =>
                    {
                        // Debug.Log("收到心跳返回");
                    });
                }
            }, HEART_BEAT_TIME, ETimeUnit.Second, -1, false);
        }
        
        public void testBattle(Action<SBattleRes> callback)
        {
            m_http.postAsync<CNull, SBattleRes>("/battle/testPost", null, (rep) =>
            {
                if (callback != null)
                {
                    callback(rep);
                }
            }, false, null);
        }

        private void norPost<CReq, SRep>(Action<SRep> callback, CReq req, String uri)
        {
            m_http.postAsync<CReq, SRep>(uri, req, (rep) =>
            {
                if (callback != null)
                {
                    callback(rep);
                }
            });
        }
        
        public void getInfo(CGetInfo req, Action<SGetInfo> callback)
        {
            m_http.postAsync<CGetInfo, SGetInfo>("/player/getInfo", req, (rep) =>
            {
                addHeartbeat();
                if (callback != null)
                {
                    callback(rep);
                }
            }, false);
        }
        
        public void getCacheInfo(Action<SGetInfo> callback)
        {
            norPost<CNull, SGetInfo>(callback, null, "/player/getCacheInfo");
        }
        
        public void getHangAward(Action<SGetHangAward> callback)
        {
            norPost<CNull, SGetHangAward>(callback, null, "/player/getHangAward");
        }

        public void cfg(Action<SCfg> callback)
        {
            m_http.postAsync<CNull, SCfg>("/player/cfg", null, (rep) =>
            {
                if (callback != null)
                {
                    callback(rep);
                }
            }, false);
        }
        
        public void heartberat(Action<SNull> callback)
        {
            // 发心跳消息的时候，不需要有loading界面参与，不然在发心跳包时，界面是无法触摸的
            // norPost<CNull, SNull>(callback, null, "/player/heartBeat");
            
            ((MsgHttp)m_http).postAsync<CNull, SNull>("/player/heartBeat", null, (rep) =>
            {
                if (callback != null)
                {
                    callback(rep);
                }
            });
        }
        
        public void getItemList(Action<SGetItemList> callback)
        {
            norPost<CNull, SGetItemList>(callback, null, "/item/getItemList");
        }
        
        public void addItem(CAddItem req, Action<SNull> callback)
        {
            norPost<CAddItem, SNull>(callback, req, "/item/addItem");
        }
        
        public void useItem(CUseItem req, Action<SUseItem> callback)
        {
            norPost<CUseItem, SUseItem>(callback, req, "/item/useItem");
        }
        
        public void saleItem(CSaleItem req, Action<SSaleItem> callback)
        {
            norPost<CSaleItem, SSaleItem>(callback, req, "/item/saleItem");
        }
        
        
        public void getHeroList(Action<SGetHeroList> callback)
        {
            norPost<CNull, SGetHeroList>(callback, null, "/hero/getHeroList");
        }
        
        public void getHeroDetail(CHeroDetail req, Action<SHeroDetail> callback)
        {
            norPost<CHeroDetail, SHeroDetail>(callback, req, "/hero/heroDetail");
        }

        public void heroLvUp(CLvUp req, Action<SLvUp> callback)
        {
            norPost<CLvUp, SLvUp>(callback, req, "/hero/lvUp");
        }
        
        public void heroStarUp(CStarUp req, Action<SStarUp> callback)
        {
            norPost<CStarUp, SStarUp>(callback, req, "/hero/starUp");
        }
        
        public void getChatList(CGetChatListMsg req, Action<SGetChatListMsg> callback)
        {
            norPost<CGetChatListMsg, SGetChatListMsg>(callback, req, "/chat/getChatList");
        }
        
        public void chat(CChat req, Action<SChat> callback)
        {
            norPost<CChat, SChat>(callback, req, "/chat/chat");
        }
        
        public void setLineup(CSetLineup req, Action<SSetLineup> callback)
        {
            norPost<CSetLineup, SSetLineup>(callback, req, "/player/setLineup");
        }
        
        public void getMailList(Action<SGetMailList> callback)
        {
            norPost<CNull, SGetMailList>(callback, null, "/mail/getMailList");
        }
        
        public void readMail(CReadMail req, Action<SReadMail> callback)
        {
            norPost<CReadMail, SReadMail>(callback, req, "/mail/readMail");
        }
        
        public void getAttachment(int mailId, Action<SGetAttachmentMail> callback)
        {
            CGetAttachmentMail req = new CGetAttachmentMail();
            req.mailId = mailId;
            
            norPost<CGetAttachmentMail, SGetAttachmentMail>((rep) =>
            {
                // todo： 展示道具奖励界面
                AwardUI.Param param = new AwardUI.Param();
                param.items = rep.attachment;
                UIMgr.getInstance().show<AwardUI>(param);
                
                callback(rep);
            }, req, "/mail/getAttachment");
        }
        
        public void getAllAttachment(Action<SGetAllAttachmentMail> callback)
        {
            norPost<CNull, SGetAllAttachmentMail>((rep) =>
            {
                AwardUI.Param param = new AwardUI.Param();
                param.items = rep.attachment;
                UIMgr.getInstance().show<AwardUI>(param);
                
                callback(rep);
            }, null, "/mail/getAllAttachment");
        }
        
        public void delMail(CDelMail req, Action<SDelMail> callback)
        {
            norPost<CDelMail, SDelMail>(callback, req, "/mail/delMail");
        }
        
        public void delAllReadMails(Action<SDelAllReadMails> callback)
        {
            norPost<CNull, SDelAllReadMails>(callback, null, "/mail/delAllReadMails");
        }
        
        ////////////// battle
        public void mainGuanqia(Action<SMainGuanqia> callback)
        {
            norPost<CNull, SMainGuanqia>(callback, null, "/battle/mainGuanqia");
        }
    }
}