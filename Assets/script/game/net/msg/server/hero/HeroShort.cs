using System.Collections.Generic;

namespace script.game.net.msg.server.hero
{
    public class HeroShort
    {
        public int id = 0;
        public int typeId = 0;
        public int lv = 0;
        public int star = 0;


        public bool isEmpty()
        {
            return typeId <= 0;
        }

        public static HeroShort empty()
        {
            return new HeroShort();
        }

        public static HeroShort makeByTypeId(int heroTypeId)
        {
            HeroShort heroShort = new HeroShort();
            heroShort.typeId = heroTypeId;
            return heroShort;
        }

        public static List<HeroShort> makeByTypeIds(List<int> heroTypeIds)
        {
            List<HeroShort> list = new List<HeroShort>();

            if (heroTypeIds == null)
            {
                for (var i = 0; i < GameConst.MAX_TEAM_NUM; i++)
                {
                    list.Add(empty());
                }
            }
            else
            {
                for (var i = 0; i < heroTypeIds.Count; i++)
                {
                    list.Add(makeByTypeId(heroTypeIds[i]));
                }
            }

            return list;
        }

        public static HeroShort findInBagHeroes(List<HeroShort> bagHeroList, int heroId)
        {
            for (var i = 0; i < bagHeroList.Count; i++)
            {
                if (bagHeroList[i].id == heroId)
                {
                    return bagHeroList[i];
                }
            }

            return null;
        }
    }
}