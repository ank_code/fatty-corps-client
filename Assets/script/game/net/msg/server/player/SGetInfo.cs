using System.Collections.Generic;
using script.game.net.msg.server.hero;

namespace script.game.net.msg.server
{
    public class SGetInfo
    {
        public string nickname;
        public int lv;
        public int exp;
        public int head;
        public int money;
        public int coin;
        public int guanqia;
        public string newToken;
        public List<int> defLineup = new List<int>();
        public int lastHangTime;
        public int serverTime;
        public long combatVal;

        public List<HeroShort> getHSLineup(List<HeroShort> bagHeroes)
        {
            List<HeroShort> retList = new List<HeroShort>();
            
            if (defLineup == null)
            {
                for (int i = 0; i < GameConst.MAX_TEAM_NUM; i++)
                {
                    retList.Add(HeroShort.empty());
                }

                return retList;
            }
            
            for (var i = 0; i < defLineup.Count; i++)
            {
                retList.Add(HeroShort.findInBagHeroes(bagHeroes, defLineup[i]));
            }

            return retList;
        }

        public List<int> getDefLineup()
        {
            if (defLineup == null)
            {
                defLineup = new List<int>();
            }

            return defLineup;
        }
    }
}