namespace script.game.net.msg.server
{
    public enum EBehaviorCondition
    {
        // 单局开始前
        BeforeBattleBegin,
        // 回合开始前
        BeforeRoundBegin,
        // 回合结束后
        AfterRoundEnd,

        // 英雄回合行动前(无论是否能出手均会触发)
        BeforeHeroActive,
        // 英雄回合行动后(无论是否能出手均会触发)
        AfterHeroActive,

        // 自己回合行动前
        BeforeSelfActive,
        // 自己回合行动时，开始激活后触发
        SelfActive,
        // 自己回合行动后
        AfterSelfActive,

        // 英雄普通攻击|技能攻击前
        BeforeHeroAtt,
        // 英雄普通攻击|技能攻击 攻击/特效关键帧触发
        HeroAtt,
        // 英雄普通攻击|技能攻击后
        AfterHeroAtt,

        // 自己普通攻击|技能攻击前
        BeforeSelfAtt,
        // 自己普通攻击|技能攻击 攻击/特效关键帧触发
        SelfAtt,
        // 自己普通攻击|技能攻击后
        AfterSelfAtt,
    
        // 英雄普通攻击前
        BeforeHeroNorAtt,
        // 英雄普通攻击 攻击/特效关键帧触发
        HeroNorAtt,
        // 英雄普通攻击后
        AfterHeroNorAtt,

        // 自己普通攻击前
        BeforeSelfNorAtt,
        // 自己普通攻击 攻击/特效关键帧触发
        SelfNorAtt,
        // 自己普通攻击后
        AfterSelfNorAtt,
    
        // 英雄技能攻击前
        BeforeHeroSkillAtt,
        // 英雄技能攻击时 攻击/特效关键帧触发
        HeroSkillAtt,
        // 英雄技能攻击后
        AfterHeroSkillAtt,

        // 自己技能攻击前
        BeforeSelfSkillAtt,
        // 自己技能攻击时 攻击/特效关键帧触发
        SelfSkillAtt,
        // 自己技能攻击后
        AfterSelfSkillAtt,
    
        // 英雄死亡后
        HeroDead,
        // 敌方单位死亡后
        EnemyDead,
        // 我方单位死亡(除自己)后
        PartnerExceptSelfDead,
        // 自己死亡后
        SelfDead,
        // 自己击杀敌方单位后
        AfterSelfKillEnemy,

        // 我方暴击后
        AfterPartnerCritical,
        // 敌方暴击后
        AfterEnemyCritical,
        // 自己暴击后
        AfterSelfCritical,

        // 自己被攻击前
        BeforeSelfUnderAtt,
        // 自己被攻击后
        AfterSelfUnderAtt,

        // 自己被暴击后
        AfterSelfBeCritical,

        // 自己获得某buff后
        AfterSelfAddBuff,
        // 自己（仅触发一次）生命低于xx%后
        AfterSelfHpBelow,

        // 自己格挡后
        AfterSelfParry,
        // 目标格挡后
        AfterEnemyParry,

        // 自己抵抗后
        AfterSelfResist,
        // 目标抵抗后
        AfterEnemyResist,

        // 自己造成伤害或回复时
        SelfCalcDamage,
        // 属性计算时
        CalcProp,

        // 触发时
        Triggered
    }
}