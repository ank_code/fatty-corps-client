using ak;
using kf;
using script.game.ui.bag;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.comm.comp
{
    public class ItemAward
    {
        private GameObject m_objParent;
        private GameObject m_objImgAward;

        private const string PREFAB = "Assets/res/prefab/commen/itemAward.prefab";
        
        public void init(GameObject parent, int itemTypeId, int num, bool award)
        {
            m_objParent = parent;
            
            ResMgr.getInstance().load<GameObject>(PREFAB, true, true, o =>
            {
                var gameObject = (GameObject)o;
                
                gameObject.transform.parent = parent.transform;
                gameObject.transform.localPosition = Vector3.zero;
                gameObject.transform.localScale = Vector3.one;

                var iconPath = ItemUtil.getIconPath(itemTypeId);
                GameObject objIcon = KtUnity.findObj(gameObject, "icon");
                Image imgIcon = objIcon.GetComponent<Image>();
                ResMgr.getInstance().load<Sprite>(iconPath, false, true, o1 =>
                {
                    imgIcon.sprite = (Sprite)o1;
                });
            
                GameObject objTxtNum = KtUnity.findObj(gameObject, "num");
                TMP_Text txtNum = objTxtNum.GetComponent<TMP_Text>();
                txtNum.text = num + "";
            
                GameObject objClickArea = KtUnity.findObj(gameObject, "clickArea");
                KtUnity.addComponent<ScrollElem>(objClickArea);
                KtUI.addListener(gameObject, objClickArea, () =>
                {
                    // todo: 展示道具介绍弹框 
                });
                
                m_objImgAward = KtUnity.findObj(gameObject, "imgAward");
                setAward(award);
            });
        }

        public void setAward(bool b)
        {
            m_objImgAward.SetActive(b);
        }
    }
}