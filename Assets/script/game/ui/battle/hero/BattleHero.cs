using System;
using System.Collections.Generic;
using ak;
using DG.Tweening;
using kf;
using kf.anim;
using script.game.mgr;
using script.game.net.msg.server;
using script.game.obj.config;
using script.game.ui.battle.effect;
using script.game.ui.battle.state;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.battle
{
    public class BattleHero
    {
        private CfgHero m_cfg;
        private HeroPos m_standPos;
        private GameObject m_parent;
        private GameObject m_obj;
        private SpineAnim m_sa;
        private HeroInfoUI m_heroInfoUI;
        private BattleHeroInfo m_initHeroInfo;
        private GameObject m_objHalo;

        private long m_curHp;
        private int m_curEnergy;

        private StateMachine m_mach;
        private bool m_playStateRunning = false;

        public class PlayInvoker
        {
            public PlayCmdQueue queue;
            public Action callback;
        }
        
        private Queue<PlayInvoker> m_playInvokerCache = new Queue<PlayInvoker>();

        private Action m_playStateComplete = null;

        public const float DEFAULT_ANIM_SCALE = 4f;
        public const float MOVE_TIME = 0.4f;
        
        // 黑板key
        public const string BB_SPINE_ANIM = "spine_anim";
        public const string BB_CMD_QUEUE = "cmd_queue";
        public const string BB_BATTLE_HERO = "battle_hero";
        
        // 动画名
        public const string ANIM_NAME_IDLE = "idle";
        public const string ANIM_NAME_ATT = "attack";

        public const int MAX_ENERGY = 100;

        private const string PREFAB_HALO_PATH = "Assets/res/prefab/battle/halo.prefab";

        public void init(int typeId, GameObject parent, BattleHeroInfo heroInfo)
        {
            this.m_standPos = heroInfo.pos;
            this.m_parent = parent;
            this.m_initHeroInfo = heroInfo;
            m_cfg = CfgMgr.getInstance().getHero(typeId);

            m_obj = new GameObject("hero_" + typeId); 
            m_obj.transform.SetParent(parent.transform);
            m_obj.transform.localPosition = Vector3.zero;
            m_obj.transform.localScale = Vector3.one;
            
            new SpineAnim(false, m_cfg.anim_path, m_obj, SpineAnim.EType.UI, (sa) =>
            {
                m_sa = sa;
                m_sa.getObj().transform.localPosition = Vector3.zero;
                m_sa.setScale(DEFAULT_ANIM_SCALE);
                
                if (!m_standPos.leftTeam)
                {
                    var scale = m_sa.getObj().transform.localScale;
                    scale.x *= -1.0f;
                    m_sa.getObj().transform.localScale = scale;
                }

                m_curHp = heroInfo.maxHp;
                m_curEnergy = heroInfo.initEnergy;
                initHeroInfoUI();
            
                m_mach = new StateMachine(this);
            
                initState();
                initBlackboard();
                initHalo();
            
                m_mach.setState<StateIdle>();
            });
        }

        private void initHeroInfoUI()
        {
            m_heroInfoUI = new HeroInfoUI();
            m_heroInfoUI.init(m_obj, new Vector3(0, m_cfg.height, 0), () =>
            {
                m_heroInfoUI.setMaxHp(m_initHeroInfo.maxHp);
                m_heroInfoUI.setCurHp(m_initHeroInfo.maxHp);
            
                m_heroInfoUI.setMaxEnergy(MAX_ENERGY);
                m_heroInfoUI.setCurEnergy(m_initHeroInfo.initEnergy);
            
                m_heroInfoUI.setLv(m_initHeroInfo.lv);
                m_heroInfoUI.setCamp(m_cfg.camp);
            });
        }

        private void initHalo()
        {
            ResMgr.getInstance().load<GameObject>(PREFAB_HALO_PATH, true, true, o =>
            {
                m_objHalo = (GameObject)o;
                m_objHalo.name = "energyHalo";
                m_objHalo.transform.SetParent(m_obj.transform);
                m_objHalo.transform.localPosition = Vector3.zero;
                m_objHalo.transform.localScale = Vector3.one;
                m_objHalo.transform.SetAsFirstSibling();
                m_objHalo.SetActive(false);
            });
        }

        public CfgHero getCfg()
        {
            return m_cfg;
        }

        public HeroInfoUI getHeroInfoUI()
        {
            return m_heroInfoUI;
        }

        public HeroPos getPos()
        {
            return m_standPos;
        }

        public GameObject getObj()
        {
            return m_obj;
        }

        public void setActive(bool b)
        {
            getObj().SetActive(b);
        }

        public void setParent(Transform parent)
        {
            getObj().transform.SetParent(parent);
        }

        private void initState()
        {
            m_mach.addState<StateIdle>();
            m_mach.addState<StatePlay>();
            m_mach.addState<StateDead>();
        }

        private void initBlackboard()
        {
            m_mach.setBlackboard(BB_SPINE_ANIM, m_sa);
            m_mach.setBlackboard(BB_BATTLE_HERO, this);
        }

        public void play(PlayCmdQueue cmdQueue, Action onComplete = null)
        {
            if (cmdQueue.last() is PlayCmdQueue.PlayMoveToOrgCmd == false)
            {
                // 默认增加返回原位指令，有些行为不需要默认增加
                PlayCmdQueue.PlayMoveToOrgCmd moveToOrgCmd = new PlayCmdQueue.PlayMoveToOrgCmd();
                cmdQueue.push(moveToOrgCmd);
            }
            
            // 如果当前还没有play完，就先缓存
            if (m_playStateRunning)
            {
                var playInvoker = new PlayInvoker();
                playInvoker.queue = cmdQueue;
                playInvoker.callback = onComplete;
                m_playInvokerCache.Enqueue(playInvoker);
                return;
            }

            m_playStateRunning = true;
            
            m_playStateComplete = onComplete;
            m_mach.setBlackboard(BB_CMD_QUEUE, cmdQueue);
            m_mach.setState<StatePlay>();
        }

        public void onStatePlayComplete()
        {
            m_mach.setState<StateIdle>();
            
            if (m_playStateComplete != null)
            {
                int oldCallbackHash = m_playStateComplete.GetHashCode();
                m_playStateComplete();
                    
                if (m_playStateComplete != null && m_playStateComplete.GetHashCode() == oldCallbackHash)
                {
                    m_playStateComplete = null;
                }
            }
            
            m_playStateRunning = false;

            if (m_playInvokerCache.Count > 0)
            {
                var playCmdInvoker = m_playInvokerCache.Dequeue();
                play(playCmdInvoker.queue, playCmdInvoker.callback);
            }
        }
        
        public bool isDead()
        {
            return m_curHp <= 0;
        }

        public void addHp(long hp)
        {
            m_curHp += hp;
            if (m_curHp > m_initHeroInfo.maxHp)
            {
                m_curHp = m_initHeroInfo.maxHp;
            }
        }
        
        public void onDamage(long damage, NumMgr.ENumType type)
        {
            addHp(-damage);
            m_heroInfoUI.setCurHp(m_curHp);

            if (damage > 0)
            {
                showNum(damage, type);
            }
            else
            {
                showNum(damage, NumMgr.ENumType.Treat);
            }

            if (isDead())
            {
                m_mach.setState<StateDead>();
            }
        }

        public void addEnergy(int energy, bool showAnim = true)
        {
            m_curEnergy += energy;
            if (m_curEnergy > MAX_ENERGY)
            {
                m_curEnergy = MAX_ENERGY;
            }
            
            m_objHalo.SetActive(m_curEnergy == MAX_ENERGY);

            if (showAnim)
            {
                showNum(energy, NumMgr.ENumType.Energy);
            }
        }
        
        public void onAddEnergy(int energy, bool showAnim = true)
        {
            addEnergy(energy, showAnim);
            m_heroInfoUI.setCurEnergy(m_curEnergy);
        }
        
        private void showNum(long value, NumMgr.ENumType numType)
        {
            var heroPostion = getObj().transform.position;
            int heroHeight = getCfg().height;
            Vector3 numberPos = new Vector3(heroPostion.x, heroPostion.y + heroHeight / 2, heroPostion.z);
            NumMgr.getInstance().showNum(numType, value, numberPos);
        }

        public void dead()
        {
            m_mach.setState<StateDead>();
        }

        public void moveTo(Vector3 pos, Action onComplete)
        {
            getObj().transform.DOMove(pos, MOVE_TIME).OnComplete(() =>
            {
                if (onComplete != null)
                {
                    onComplete();
                }
            });
        }

        public void reset(Action onComplete)
        {
            getObj().transform.SetParent(m_parent.transform);

            // 只有移动的英雄，需要返回原位 
            if (!getObj().transform.localPosition.Equals(Vector3.zero))
            {
                getObj().transform.DOLocalMove(Vector3.zero, MOVE_TIME).OnComplete(() =>
                {
                    if (onComplete != null)
                    {
                        onComplete();
                    }
                });
            }
            else
            {
                if (onComplete != null)
                {
                    onComplete();
                }
            }
        }
    }
}