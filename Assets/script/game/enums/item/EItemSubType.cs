namespace script.game.enums.item
{
    public enum EItemSubType
    {
        // 普通
        Nor,
        // 英雄碎片
        HeroFragment,
        // 元宝
        Coin,
        // 挂机奖励
        IdlePrice,
    }
}