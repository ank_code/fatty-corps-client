using kf;
using script.game.net.msg.server;
using UnityEngine;

namespace script.game.ui.battle
{
    public class BattlePos : Singleton<BattlePos>
    {
        private Vector3 m_centerPosLeft;
        private Vector3 m_frontPosLeft;
        private Vector3 m_backPosLeft;
        
        private Vector3 m_centerPosRight;
        private Vector3 m_frontPosRight;
        private Vector3 m_backPosRight;

        public bool init = false;

        public void setFrontPosLeft(Vector3 p)
        {
            m_frontPosLeft = p;
        }

        public void setCenterPosLeft(Vector3 p)
        {
            m_centerPosLeft = p;
        }
        
        public void setBackPosLeft(Vector3 p)
        {
            m_backPosLeft = p;
        }
        
        public void setFrontPosRight(Vector3 p)
        {
            m_frontPosRight = p;
        }
        
        public void setCenterPosRight(Vector3 p)
        {
            m_centerPosRight = p;
        }
        
        public void setBackPosRight(Vector3 p)
        {
            m_backPosRight = p;
        }

        public Vector3 getEnemyCenter(bool leftTeam)
        {
            return leftTeam ? m_centerPosRight : m_centerPosLeft;
        }

        public Vector3 getEnemyFront(bool leftTeam)
        {
            return leftTeam ? m_frontPosRight : m_frontPosLeft;
        }
        
        public Vector3 getEnemyBack(bool leftTeam)
        {
            return leftTeam ? m_backPosRight : m_backPosLeft;
        }

        private const float ATT_DIS_POS_X = 150;
        
        public Vector3 getHeroUnderAttPos(HeroPos pos)
        {
            BattleHero hero = BattleHeroMgr.getInstance().getHeroByPos(pos);

            if (pos.leftTeam)
            {
                return hero.getObj().transform.position + new Vector3(ATT_DIS_POS_X, 0, 0);
            } 
            
            return hero.getObj().transform.position - new Vector3(ATT_DIS_POS_X, 0, 0);
        }
    }
}