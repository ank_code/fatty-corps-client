using System;
using System.Collections.Generic;
using System.Linq;
using ak;
using kf;
using kf.anim;
using script.game.mgr;
using script.game.net.http;
using script.game.net.msg.client.hero;
using script.game.net.msg.server.hero;
using script.game.obj.config;
using script.game.ui.bag.comp;
using script.game.ui.bag.prop;
using script.game.ui.battle;
using script.game.ui.hero.detail.comp;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.hero.detail
{
    public class HeroDetailUI : GameUI
    {
        public class UIParam : IUIParam
        {
            public List<HeroShort> allHeroShorts;
            public int bagIndex;
        }

        // 当前英雄在背包中的序号，用于前后切换
        private List<HeroShort> m_allHeroShorts;
        private int m_bagIndex;
        private HeroDetail m_heroDetail;

        private GameObject m_objBtnLvUp;
        private GameObject m_objBtnStarUp;
        
        private HeroStar m_heroStar;
        private CfgHero m_cfg;
        private HeroAnim m_heroAnim;
        private SwitchArrow m_switchArrowLeft = new SwitchArrow();
        private SwitchArrow m_switchArrowRight = new SwitchArrow();

        /// left
        private TMP_Text m_txtName;
        // 当前等级
        private TMP_Text m_txtLv;
        // 当前等级信息 curLv/maxLv
        private TMP_Text m_txtLvInfo;
        private Slider m_lvProgress;
        private Text m_txtConsumeCoin;
        private Text m_txtConsumeExp;

        private List<SkillIcon> m_skillIcons = new List<SkillIcon>();
        
        /// mid
        private TMP_Text m_txtCombatValue;
        
        /// right
        private TMP_Text m_txtAtt;
        private TMP_Text m_txtDef;
        private TMP_Text m_txtMaxHp;
        private TMP_Text m_txtSpeed;
        
        public override void onCreate(IUIParam param)
        {
            UIParam uiParam = (UIParam)param;
            m_bagIndex = uiParam.bagIndex;
            m_allHeroShorts = uiParam.allHeroShorts;

            GameObject objBtnClose = KtUnity.findObj(gameObject, "btnClose");
            KtUI.addListener(objBtnClose, () =>
            {
                Player.getInstance().refresh();
                HeroListUI.open();
                this.close();
            });

            initLeft();
            initMid();
            initRight();

            msgRefresh();
        }

        private void initLeft()
        {
            GameObject objTxtName = KtUnity.findObj(gameObject, "txtName");
            m_txtName = objTxtName.GetComponent<TMP_Text>();
            
            GameObject objHeroStar = KtUnity.findObj(gameObject, "starContainer");
            m_heroStar = KtUnity.addComponent<HeroStar>(objHeroStar);
            
            GameObject objTxtLv = KtUnity.findObj(gameObject, "txtLv");
            m_txtLv = objTxtLv.GetComponent<TMP_Text>();
            
            GameObject objTxtLvInfo = KtUnity.findObj(gameObject, "txtLvInfo");
            m_txtLvInfo = objTxtLvInfo.GetComponent<TMP_Text>();
            
            GameObject objLvProgress = KtUnity.findObj(gameObject, "lvProgress");
            m_lvProgress = objLvProgress.GetComponent<Slider>();
            
            GameObject objCurrencyCoin = KtUnity.findObj(gameObject, "currencyCoin");
            m_txtConsumeCoin = KtUnity.findObj(objCurrencyCoin, "txt").GetComponent<Text>();
            
            GameObject objCurrencyExp = KtUnity.findObj(gameObject, "currencyExp");
            m_txtConsumeExp = KtUnity.findObj(objCurrencyExp, "txt").GetComponent<Text>();
            
            m_objBtnLvUp = KtUnity.findObj(gameObject, "btnLvUp");
            GameObject objBtnLvUpClickArea = KtUnity.findObj(m_objBtnLvUp, "btnClickArea");
            
            KtUI.addListener(m_objBtnLvUp, objBtnLvUpClickArea, () =>
            {
                Debug.Log("aktest click lvUp");
                CLvUp msg = new CLvUp();
                msg.heroId = this.m_heroDetail.id;
                
                GameRequest.getInstance().heroLvUp(msg, rep =>
                {
                    refresh(rep.heroDetail);
                });
            });
            
            m_objBtnStarUp = KtUnity.findObj(gameObject, "btnStarUp");
            GameObject objBtnStarUpClickArea = KtUnity.findObj(m_objBtnStarUp, "btnClickArea");
            KtUI.addListener(m_objBtnStarUp, objBtnStarUpClickArea, () =>
            {
                Debug.Log("aktest click starUp");
                CStarUp msg = new CStarUp();
                msg.heroId = this.m_heroDetail.id;
                
                GameRequest.getInstance().heroStarUp(msg, rep =>
                {
                    refresh(rep.heroDetail);
                });
            });
        }

        private void initMid()
        {
            GameObject objAnimHero = KtUnity.findObj(gameObject, "animHero");
            m_heroAnim = KtUnity.addComponent<HeroAnim>(objAnimHero);
            
            GameObject objBtnHeroAnim = KtUnity.findObj(gameObject, "btnHeroAnim");
            KtUI.addListener(objBtnHeroAnim, () =>
            {
                m_heroAnim.setState(HeroAnim.EState.Attack);
            });

            // 左右箭头
            GameObject objSwitchLeft = KtUnity.findObj(gameObject, "switchLeft");
            GameObject objSwitchRight = KtUnity.findObj(gameObject, "switchRight");
            
            m_switchArrowLeft.init(objSwitchLeft, SwitchArrow.EDirect.Left, () =>
            {
                m_bagIndex--;
                if (m_bagIndex < 0)
                {
                    m_bagIndex = 0;
                    return;
                }

                msgRefresh();
            });
            
            m_switchArrowRight.init(objSwitchRight, SwitchArrow.EDirect.Right, () =>
            {
                m_bagIndex++;
                if (m_bagIndex >= m_allHeroShorts.Count)
                {
                    m_bagIndex = m_allHeroShorts.Count - 1;
                    return;
                }
                
                msgRefresh();
            });
            
            // 战力
            GameObject objTxtCombatValue = KtUnity.findObj(gameObject, "txtCombatValue");
            m_txtCombatValue = objTxtCombatValue.GetComponent<TMP_Text>();
            
            // 技能
            GameObject objSkillList = KtUnity.findObj(gameObject, "skillList");
            addSkillIcon(objSkillList, "skill1");
            addSkillIcon(objSkillList, "skill2");
            addSkillIcon(objSkillList, "skill3");
            addSkillIcon(objSkillList, "skill4");
        }

        private void addSkillIcon(GameObject objSkillList, string objName)
        {
            SkillIcon si = new SkillIcon();
            si.init(KtUnity.findObj(objSkillList, objName));
            m_skillIcons.Add(si);
        }

        private void initRight()
        {
            GameObject objTxtAtt = KtUnity.findObj(gameObject, "txtAtt");
            m_txtAtt = objTxtAtt.GetComponent<TMP_Text>();
            
            GameObject objTxtDef = KtUnity.findObj(gameObject, "txtDef");
            m_txtDef = objTxtDef.GetComponent<TMP_Text>();
            
            GameObject objTxtMaxHp = KtUnity.findObj(gameObject, "txtMaxHp");
            m_txtMaxHp = objTxtMaxHp.GetComponent<TMP_Text>();
            
            GameObject objTxtSpeed = KtUnity.findObj(gameObject, "txtSpeed");
            m_txtSpeed = objTxtSpeed.GetComponent<TMP_Text>();
            
            KtUI.addListenerAutoScale(KtUnity.findObj(gameObject, "btnPropDetail"), () =>
            {
                PropUI.Param p = new PropUI.Param();
                p.heroDetail = m_heroDetail;
                UIMgr.getInstance().show<PropUI>(p);
            });
        }

        private void refreshArrow()
        {
            m_switchArrowLeft.setState(SwitchArrow.EState.Enable);
            m_switchArrowRight.setState(SwitchArrow.EState.Enable);
            
            if (m_bagIndex == 0)
            {
                m_switchArrowLeft.setState(SwitchArrow.EState.Unable);
            }

            if (m_bagIndex == m_allHeroShorts.Count - 1)
            {
                m_switchArrowRight.setState(SwitchArrow.EState.Unable);
            }
        }

        private void refreshSkill()
        {
            var cfgHero = CfgMgr.getInstance().getHero(m_heroDetail.typeId);
            string[] cfgGroupIds = cfgHero.skill_group_ids.Split(",");

            int showNum = cfgGroupIds.Length;
            
            for (var i = 0; i < m_skillIcons.Count; i++)
            {
                // 获取英雄基础技能组个数，决定展示几个技能
                m_skillIcons[i].setActive(i < showNum);

                if (i < showNum)
                {
                    // 设置技能展示的内容
                    bool lockIcon = true;

                    var cfgSkillGroup = CfgMgr.getInstance().getSkillGroup(int.Parse(cfgGroupIds[i]));
                    string[] groupSkillIds = cfgSkillGroup.skill_ids.Split(",");
                    
                    // 判断是否已经解锁了
                    for (var j = 0; j < m_heroDetail.heroSkillIds.Count; j++)
                    {
                        if (Array.IndexOf(groupSkillIds, m_heroDetail.heroSkillIds[j].ToString()) >= 0)
                        {
                            lockIcon = false;
                            m_skillIcons[i].setSkillTypeId(m_heroDetail.heroSkillIds[j]);
                            break;
                        }
                    }
                    
                    m_skillIcons[i].setUnLock(!lockIcon);

                    if (lockIcon)
                    {
                        // 使用技能组等级最低的第一个技能图标
                        m_skillIcons[i].setSkillTypeId(int.Parse(groupSkillIds[0]));
                    }
                }
            }
        }

        private void msgRefresh()
        {
            // 获取英雄详情，然后打开详情界面
            CHeroDetail msg = new CHeroDetail();
            msg.heroId = m_allHeroShorts[m_bagIndex].id;
            GameRequest.getInstance().getHeroDetail(msg, (rep) =>
            {
                refresh(rep.heroDetail);
            });
        }

        private void refresh(HeroDetail dtl)
        {
            this.m_heroDetail = dtl;
            m_cfg = CfgMgr.getInstance().getHero(dtl.typeId);
            CfgStar cfgStar = CfgMgr.getInstance().getStar(dtl.star);
            m_txtName.text = m_cfg.name;
            m_heroStar.setStar(m_heroDetail.star);
            m_txtLv.text = m_heroDetail.lv + "";
            m_txtLvInfo.text = m_heroDetail.lv + "/" + cfgStar.max_lv;
            m_lvProgress.value = m_heroDetail.lv / (float)cfgStar.max_lv;

            var cfgHeroExp = CfgMgr.getInstance().getHeroExp(m_heroDetail.lv);
            m_txtConsumeCoin.text = cfgHeroExp.coin + "";
            m_txtConsumeExp.text = cfgHeroExp.exp + ""; 

            m_objBtnLvUp.SetActive(m_heroDetail.lv != cfgStar.max_lv);
                
            m_heroAnim.init(m_cfg);
            refreshArrow();

            m_txtCombatValue.text = m_heroDetail.combat;

            m_txtAtt.text = m_heroDetail.att;
            m_txtDef.text = m_heroDetail.def;
            m_txtMaxHp.text = m_heroDetail.maxHp;
            m_txtSpeed.text = m_heroDetail.speed;

            refreshSkill();
        }
    }
}