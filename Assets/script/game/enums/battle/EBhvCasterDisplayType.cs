namespace script.game.enums.battle
{
    public enum EBhvCasterDisplayType
    {
        // 无
        None,
        // 原地播放攻击动画（关键帧触发目标表现）
        Standby,

        // 原地播放攻击动画+投掷元素（动画或图片）到目标（在动画关键帧时触发投掷、投掷物到达目标时，触发目标表现）
        Throw,

        // 迅速移动到目标身前（单体）播放攻击动画（关键帧触发目标表现）
        MoveToTarget,

        // 迅速移动到敌方前排（没有前排就去后排正中心）正中心（如：目标是全体或前排）播放攻击动画（关键帧触发目标表现）
        MoveToFront,

        // 迅速移动到地方后排（没有后排就去前排正中心）正中心（如：目标是后排）播放攻击动画（关键帧触发目标表现）
        MoveToBack,
        // 迅速移动到敌方前排正中心
        MoveToCenter,
    }
}