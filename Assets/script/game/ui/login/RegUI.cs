

using System;
using ak;
using kf;
using script.game.enums;
using script.game.net.http;
using script.game.ui;
using script.game.ui.comm;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;
using Object = System.Object;

public class RegUI : GameUI
{
    private void Start()
    {
        var objBtnReg = KtUnity.findObj(gameObject, "btnReg");
        var objBtnCancel = KtUnity.findObj(gameObject, "btnCancel");
        var objInputAcc = KtUnity.findObj(gameObject, "inputAcc");
        var objInputPwd = KtUnity.findObj(gameObject, "inputPwd");
        var objInputPwdConfirm = KtUnity.findObj(gameObject, "inputPwdConfirm");

        var inputAcc = objInputAcc.GetComponent<TMP_InputField>();
        var inputPwd = objInputPwd.GetComponent<TMP_InputField>();
        var inputPwdConfirm = objInputPwd.GetComponent<TMP_InputField>();

        KtUI.addListener(objBtnReg, () =>
        {
            if (!UIUtil.verifyInputAndAlert(inputAcc.text, "账号不能为空"))
            {
                return;
            }
            
            if (!UIUtil.verifyInputAndAlert(inputPwd.text, "密码不能为空"))
            {
                return;
            }
            
            if (!UIUtil.verifyInputAndAlert(inputPwdConfirm.text, "密码确认不能为空"))
            {
                return;
            }

            if (inputPwd.text != inputPwdConfirm.text)
            {
                AlertUtil.alertInfo("两次输入的密码不同");
                return;
            }

            if (!UIUtil.accountRegexMatch(inputAcc.text))
            {
                AlertUtil.alertInfo("账号格式错误：只能输入a-z,0-9的字符，5-20位");
                return;
            }
            
            if (!UIUtil.passwordRegexMatch(inputPwd.text))
            {
                AlertUtil.alertInfo("密码格式错误：8-16位，至少包含一个字母&一个数字&一个特殊字符（@、$、!、%、*、? 或 &）");
                return;
            }
            
            Debug.Log("aktest on objBtnReg click, acc=>" + inputAcc.text + ",pwd=>" + inputPwd.text);
            
            AccountRequest.getInstance().reg(inputAcc.text, inputPwd.text, () =>
            {
                AlertUtil.alertInfo("注册成功");
                
                this.close();
                // var loginUI = UIMgr.getInstance().getUI<LoginUI>();
                // loginUI.reSet(inputAcc.text, inputPwd.text);
                sendEvent(EEvent.OnRegAccountSuccess, new []{inputAcc.text, inputPwd.text});
            });
        });
        
        KtUI.addListener(objBtnCancel, () =>
        {
            UIMgr.getInstance().destroy<RegUI>();
        });
    }
}