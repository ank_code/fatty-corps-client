using System;
using Newtonsoft.Json;

namespace kf
{
    public abstract class JsonHttp : BaseHttp
    {
        public override void init()
        {
            base.init();
            addHeader("Content-Type", "application/json");
        }
        
        // get
        public void getAsync<O>(string path, Action<O> success, Action<IBaseHttp.Err> fail)
        {
            throw new NotImplementedException();
        }
        
        // post
        public virtual void postAsync<Req, Rep>(string path, Req param, Action<Rep> success, Action<IBaseHttp.Err> fail = null)
        {
            string json = JsonConvert.SerializeObject(param);
            base.postAsync(path, json, (response) =>
            {
                Rep obj = JsonConvert.DeserializeObject<Rep>(response);
                if (success != null)
                {
                    success(obj);
                }
            }, fail);
        }
    }
}