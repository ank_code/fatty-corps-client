using System.Collections;
using UnityEngine;
using YooAsset;

namespace kf.hotpUpdate.state
{
    // 初始化yooAsset
    public class UpdateVersion : FsmState
    {
        public override void onEnter()
        {
            HotUpdate.HotUpdateMono mono = (HotUpdate.HotUpdateMono)getMachine().getBlackboard(HotUpdate.BBK_MONOBEHAVIOUR);
            mono.startCoroutine(updateVersion());
        }
        
        private IEnumerator updateVersion()
        {
            // yield return new WaitForSecondsRealtime(0.5f);

            var packageName = (string)getMachine().getBlackboard(HotUpdate.BBK_YOOASSET_PACKAGE_NAME);
            var package = YooAssets.GetPackage(packageName);
            var operation = package.UpdatePackageVersionAsync();
            yield return operation;

            if (operation.Status != EOperationStatus.Succeed)
            {
                Debug.LogWarning(operation.Error);
                getMachine().setBlackboard(HotUpdate.BBK_ERR, operation.Error);
                getMachine().setState<Error>();
            }
            else
            {
                getMachine().setBlackboard(HotUpdate.BBK_PACKAGE_VERSION, operation.PackageVersion);
                getMachine().setState<UpdateManifest>();
            }
        }
    }
}