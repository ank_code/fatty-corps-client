
using System;
using System.Collections;
using System.Reflection;
using ak;
using kf.hotpUpdate.state;
using UnityEngine;
using YooAsset;

namespace kf
{
    /**
     * 热更新，依赖YooAsset与Hybrid
     * 流程：
     * 1、init
     * 2、设置回调 setOnStateChange、setOnDownloadProgress
     * 3、next开始热更
     * 4、回调中setOnStateChange为WaitForConfirm时，再次next
     * 
     */
    public class HotUpdate : Singleton<HotUpdate>, IDisposable
    {
        private MonoBehaviour m_mono;
        private bool m_init = false;
        private EPlayMode m_mode;
        private string m_hotUpdateAssemblyName = "hotUpdate";
        private GameObject m_objHotUpdate = null;
        private string m_packageName = "DefaultPackage";
        private StateMachine m_stateMachine = null;

        private DownloaderOperation.OnDownloadProgress m_onDownloadProgress = null;

        private StateMachine.onStateChange m_onStateChange = null;
        
        // blackboard key
        public const string BBK_YOOASSET_PACKAGE_NAME = "yooAssetPackageName";
        public const string BBK_SCRIPT_PACKAGE_NAME = "scriptPackageName";
        // public const string BBK_BUILDPIPELINE = "buildPipeline";
        public const string BBK_PLAY_MODE = "playMode";
        public const string BBK_HOTUPDATE_ASSEMBLY_NAME = "hotUpdateAssemblyName";
        public const string BBK_MONOBEHAVIOUR = "hotUpdateMonoBehaviour";
        public const string BBK_HOST = "host";
        public const string BBK_ERR = "err";
        public const string BBK_PACKAGE_VERSION = "packageVersion";
        public const string BBK_DOWNLOADER = "downloader";
        public const string BBK_TOTAL_DOWNLOAD_COUNT = "totalDownloadCount";
        public const string BBK_TOTAL_DOWNLOAD_BYTES = "totalDownloadBytes";
        public const string BBK_SCRIPT_DLL_FOLDER = "scriptDllFolder";
        public const string BBK_HOTUPDATE_ASSEMBLY = "hotUpdateAssembly";

        public class HotUpdateMono : MonoBehaviour
        {
            public void startCoroutine(IEnumerator routine)
            {
                StartCoroutine(routine);
            }
        }
        
        /**
         * host: 服务器热更目录地址
         * mode：资源加载模式
         * hotUpdateAssemblyName：热更程序集名
         * scriptDllFolder：热更dll路径
         * packageName：YooAsset包名
         */
        public void init(string host, EPlayMode mode, string hotUpdateAssemblyName, string scriptDllFolder, string packageName = "DefaultPackage")
        {
            if (m_init)
            {
                return;
            }

            m_init = true;
            m_hotUpdateAssemblyName = hotUpdateAssemblyName;
            m_packageName = packageName;
            m_mode = mode;
            
#if !UNITY_EDITOR
            if (m_mode == EPlayMode.EditorSimulateMode) {
                m_mode = EPlayMode.HostPlayMode;
            }
#endif
            
            m_objHotUpdate = new GameObject(nameof(HotUpdate).ToLower());
            m_mono = KtUnity.addComponent<HotUpdateMono>(m_objHotUpdate);

            m_stateMachine = new StateMachine(this);
            m_stateMachine.addState<Init>();
            m_stateMachine.addState<InitOver>();
            m_stateMachine.addState<UpdateVersion>();
            m_stateMachine.addState<UpdateManifest>();
            m_stateMachine.addState<CreateDownloader>();
            m_stateMachine.addState<WaitForConfirm>();
            m_stateMachine.addState<DownloadFiles>();
            m_stateMachine.addState<InitHybrid>();
            m_stateMachine.addState<CleanCache>();
            m_stateMachine.addState<HotUpdateOver>();
            m_stateMachine.addState<Error>();
            
            m_stateMachine.setBlackboard(BBK_YOOASSET_PACKAGE_NAME, m_packageName);
            m_stateMachine.setBlackboard(BBK_HOTUPDATE_ASSEMBLY_NAME, m_hotUpdateAssemblyName);
            m_stateMachine.setBlackboard(BBK_PLAY_MODE, m_mode);
            m_stateMachine.setBlackboard(BBK_MONOBEHAVIOUR, m_mono);
            // m_stateMachine.setBlackboard(BBK_BUILDPIPELINE, EDefaultBuildPipeline.BuiltinBuildPipeline.ToString());
            m_stateMachine.setBlackboard(BBK_HOST, host);
            m_stateMachine.setBlackboard(BBK_ERR, "");
            m_stateMachine.setBlackboard(BBK_SCRIPT_DLL_FOLDER, scriptDllFolder);
            
            m_stateMachine.setOnStateChgCallback(onStateChange);
            m_stateMachine.setState<Init>();
        }
        
        public void next()
        {
            if (m_stateMachine.getCurState() is InitOver)
            {
                m_stateMachine.setState<UpdateVersion>();
            } 
            else if (m_stateMachine.getCurState() is WaitForConfirm)
            {
                m_stateMachine.setState<DownloadFiles>();
            }
        }

        public void setOnStateChange(StateMachine.onStateChange callback)
        {
            m_onStateChange = callback;
        }

        public void setOnDownloadProgress(DownloaderOperation.OnDownloadProgress callback)
        {
            m_onDownloadProgress = callback;
        }

        public string getErr()
        {
            return (string)m_stateMachine.getBlackboard(BBK_ERR);
        }

        public Assembly getHotUpdateAssembly()
        {
            return (Assembly)m_stateMachine.getBlackboard(BBK_HOTUPDATE_ASSEMBLY);
        }

        public long getDownloadTotalBytes()
        {
            return (long)m_stateMachine.getBlackboard(BBK_TOTAL_DOWNLOAD_BYTES);
        }
        
        public int getDownloadTotalCount()
        {
            return (int)m_stateMachine.getBlackboard(BBK_TOTAL_DOWNLOAD_COUNT);
        }
        
        private void onStateChange(IFsmState state)
        {
            if (state is WaitForConfirm)
            {
                var downloader = (ResourceDownloaderOperation)(m_stateMachine.getBlackboard(HotUpdate.BBK_DOWNLOADER));
                downloader.OnDownloadErrorCallback = (string fileName, string error) =>
                {
                    m_stateMachine.setBlackboard(BBK_ERR, fileName + ":" + error);
                    m_stateMachine.setState<Error>();
                };
                
                downloader.OnDownloadProgressCallback = m_onDownloadProgress;
            }

            if (m_onStateChange != null)
            {
                m_onStateChange(state);
            }
        }

        public void Dispose()
        {
            if (m_objHotUpdate != null)
            {
                KtUnity.destory(m_objHotUpdate);
            }
        }
    }
}
