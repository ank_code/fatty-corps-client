using System;
using kf;
using Newtonsoft.Json;
using script.core.store;
using script.game.enums;

namespace script.game.mgr
{
    public class StoreMgr : Singleton<StoreMgr>, IEventListener
    {
        private Store m_store = new Store();
        
        public const string KEY_ACC = "account";
        public const string KEY_PWD = "password";
        public const string KEY_ACC_TOKEN = "acc_token";
        public const string KEY_GAME_TOKEN = "game_token";
        // public const string KEY_CHAT_ID = "chat_id";
        public const string KEY_CHAT_LIST = "chat_list";

        public void init()
        {
            m_store.init();
            EventMgr.getInstance().addEventListener(this);
        }

        public object get(string key)
        {
            return m_store.get(key);
        }

        public int getInt(string key, int defVal)
        {
            var o = this.get(key);
            if (o == null)
            {
                return defVal;
            }

            return Convert.ToInt32(o);
        }

        public string getStr(string key, string defVal)
        {
            var o = this.get(key);
            if (o == null)
            {
                return defVal;
            }

            return Convert.ToString(o);
        }

        public T getObj<T>(string key)
        {
            var type = typeof(T);
            var o = this.get(key);
            if (o == null)
            {
                return default(T);
            }

            var t = JsonConvert.DeserializeObject<T>((string)o);
            return t;
        }

        public void set(string key, object val)
        {
            m_store.set(key, val);
        }

        public void clearToken()
        {
            m_store.set(KEY_ACC_TOKEN, "");
            m_store.set(KEY_GAME_TOKEN, "");
        }

        // public void setObj(string key, object o)
        // {
        //     var json = JsonConvert.SerializeObject(o);
        //     this.set(key, json);
        // }

        public void onEvent(int eId, object[] objs)
        {
            EEvent e = (EEvent)eId;
            switch (e)
            {
                case EEvent.OnLoginSuccess:
                {
                    string token = (string)objs[0];
                    m_store.set(KEY_ACC_TOKEN, token);
                    
                    // 保存账号密码，用于下次重新登陆
                    string acc = (string)objs[1];
                    string pwd = (string)objs[2];
                    m_store.set(KEY_ACC, acc);
                    m_store.set(KEY_PWD, pwd);
                    break;
                }
                case EEvent.OnRefreshGameToken:
                {
                    string token = (string)objs[0];
                    m_store.set(KEY_GAME_TOKEN, token);
                    break;
                }
            }
        }
    }
}