namespace script.game.obj.config
{
    public class CfgStar : BaseCfg
    {
        public int star;
        public int max_lv;
        public int prop_inc_per;
        public string consume_items;
        public string consume_heroes;
    }
}