using UnityEngine;
using System.Collections;
using System;


namespace ak
{
    public class KtUnity
    {
        public static T addComponent<T>(GameObject obj) where T : Component {
            T t = obj.GetComponent<T>();
            if (t == null) {
                t = obj.AddComponent<T>();
            }

            return t;
        }

        public static Component addComponent(GameObject obj, Type compType)
        {
            var comp  = obj.GetComponent(compType);
            if (comp == null)
            {
                comp = obj.AddComponent(compType);
            }

            return comp;
        }

        public static void destory(UnityEngine.Object o) {
            UnityEngine.Object.Destroy(o);
        }

        public static void destroyImmediate(UnityEngine.Object o)
        {
            UnityEngine.Object.DestroyImmediate(o);
        }

        public static void destroyAllChildren(GameObject parent)
        {
            for (int i = 0; i < parent.transform.childCount; i++)
            {
                GameObject objChild = parent.transform.GetChild(i).gameObject;
                destroyImmediate(objChild);
            }
        }

        public static T copyComponent<T>(T original, GameObject destination) where T : Component
        {
            Type type = original.GetType();
            Component copy = destination.AddComponent(type);
            System.Reflection.FieldInfo[] fields = type.GetFields();
            foreach (System.Reflection.FieldInfo field in fields)
            {
                field.SetValue(copy, field.GetValue(original));
            }
            
            return copy as T;
        }
        
        public static GameObject findObj(GameObject parent, string name)
        {
            var gameObject = findObjCore(parent, name);
            if (gameObject == null)
            {
                UnityEngine.Debug.LogError("error: 没有在" + parent.name + "中找到名为" + name + "的子物体");
            }

            return gameObject;
        }
    
        private static GameObject findObjCore(GameObject parent, string name)
        {
        
            for (int i = 0; i < parent.transform.childCount; i++)
            {
                Transform trans = parent.transform.GetChild(i);
                GameObject obj = trans.gameObject;
                if (obj.name.Equals(name))
                {
                    return obj;
                }

                obj = findObjCore(obj, name);
                if (obj != null)
                {
                    return obj;
                }
            }

            return null;
        }

        public static void stretchFull(RectTransform rt)
        {
            rt.anchorMin = Vector2.zero;
            rt.anchorMax = Vector2.one;
            rt.offsetMin = Vector2.zero;
            rt.offsetMax = Vector2.zero;
            
            rt.localScale = Vector3.one;
            rt.localPosition = Vector3.zero;
        }

        public static void stopCoroutine(MonoBehaviour mono, Coroutine c)
        {
            if (c != null)
            {
                mono.StopCoroutine(c);
            }
        }
    }
}
