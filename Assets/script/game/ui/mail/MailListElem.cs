using System;
using System.Collections.Generic;
using ak;
using kf;
using script.game.enums;
using script.game.net.http;
using script.game.net.msg.client.hero;
using script.game.net.msg.server.item;
using script.game.ui.bag;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.mail
{
    public class MailListElem : MonoBehaviour
    {
        private DMail m_mail;
        private int m_index;

        private TMP_Text m_txtTitle;
        private TMP_Text m_txtSender;
        private TMP_Text m_txtTime;

        private GameObject m_objAttachIconBg;
        private GameObject m_objBtnDel;
        private GameObject m_objBtnAward;
        
        public void init(DMail mail, int index)
        {
            m_mail = mail;
            m_index = index;

            GameObject objTxtTitle = KtUnity.findObj(gameObject, "txtTitle");
            m_txtTitle = objTxtTitle.GetComponent<TMP_Text>();
            m_txtTitle.text = mail.title;

            GameObject objTxtSender = KtUnity.findObj(gameObject, "txtSender");
            m_txtSender = objTxtSender.GetComponent<TMP_Text>();
            m_txtSender.text = "系统邮件";
            
            GameObject objTxtTime = KtUnity.findObj(gameObject, "txtTime");
            m_txtTime = objTxtTime.GetComponent<TMP_Text>();
            m_txtTime.text = KtTime.formatTimpTime(mail.time, KtTime.FORMAT_YYYY_MM_DD);

            m_objAttachIconBg = KtUnity.findObj(gameObject, "attachIconBg");
            
            // 按钮控制
            m_objBtnDel = KtUnity.findObj(gameObject, "btnDel");
            GameObject objBtnDelClickArea = KtUnity.findObj(m_objBtnDel, "btnClickArea");
            KtUnity.addComponent<ScrollElem>(objBtnDelClickArea);
            KtUI.addListener(m_objBtnDel, objBtnDelClickArea, () =>
            {
                AlertUtil.alertOkCancel("确定要删除么？", () =>
                {
                    CDelMail req = new CDelMail();
                    req.mailId = m_mail.id;
                    GameRequest.getInstance().delMail(req, (rep) =>
                    {
                        EventMgr.getInstance().sendEvent((int)EEvent.OnMailDel, new object[]{m_mail.id, m_index});
                    });
                });
            });
                
            m_objBtnAward = KtUnity.findObj(gameObject, "btnAward");
            GameObject objBtnAwardClickArea = KtUnity.findObj(m_objBtnAward, "btnClickArea");
            KtUnity.addComponent<ScrollElem>(objBtnAwardClickArea);
            KtUI.addListener(m_objBtnAward, objBtnAwardClickArea, () =>
            {
                Debug.Log("aktest send getAttachment");
                GameRequest.getInstance().getAttachment(m_mail.id, (rep) =>
                {
                    EventMgr.getInstance().sendEvent((int)EEvent.OnMailGetAttachment, new object[]{m_mail.id, m_index});
                });
            });
            
            // 整体被点击
            GameObject objBtnDetail = KtUnity.findObj(gameObject, "btnDetail");
            KtUnity.addComponent<ScrollElem>(objBtnDetail);
            KtUI.addListener(objBtnDetail, () =>
            {
                Debug.Log("aktest objBtnDetail mailId=>" + m_mail.id);
                // 打开详情页
                if (!m_mail.read)
                {
                    CReadMail req = new CReadMail();
                    req.mailId = m_mail.id;
                    GameRequest.getInstance().readMail(req, (rep) =>
                    {
                        // 通知列表，刷新状态
                        EventMgr.getInstance().sendEvent((int)EEvent.OnMailRead, new object[]{m_mail.id, m_index});
                    });
                }

                MailDetailUI.Param param = new MailDetailUI.Param();
                param.mail = this.m_mail;
                param.index = this.m_index;
                UIMgr.getInstance().show<MailDetailUI>(param);
            });

            if (m_mail.attachment != null && m_mail.attachment.Length > 0)
            {
                List<DItem> itemList = ItemUtil.strToAwardList(m_mail.attachment);

                // 使用第一个道具附件的道具信息
                int typeId = itemList[0].typeId;
                int num = itemList[0].num;

                var iconPath = ItemUtil.getIconPath(typeId);
                GameObject objAttachIcon = KtUnity.findObj(m_objAttachIconBg, "attachIcon");
                var imgAttachIcon = objAttachIcon.GetComponent<Image>();
                ResMgr.getInstance().load<Sprite>(iconPath, false, true, o =>
                {
                    imgAttachIcon.sprite = (Sprite)o;
                });

                GameObject objTxtNum = KtUnity.findObj(m_objAttachIconBg, "txtNum");
                var txtNum = objTxtNum.GetComponent<TMP_Text>();
                txtNum.text = num + "";
            }

            refreshRead(m_mail.read);
            refreshAttach(m_mail.getAttachment);
        }

        public void refreshRead(bool read)
        {
            m_mail.read = read;
            
            // 是否已读
            GameObject objRead = KtUnity.findObj(gameObject, "readIcon");
            objRead.SetActive(!m_mail.read);
        }
        
        public void refreshAttach(bool getAttach)
        {
            m_mail.getAttachment = getAttach;
            
            if (m_mail.attachment != null && m_mail.attachment.Length > 0)
            {
                m_objAttachIconBg.SetActive(true);
                
                // 是否被领取
                GameObject objGetIcon = KtUnity.findObj(m_objAttachIconBg, "getIcon");
                objGetIcon.SetActive(m_mail.getAttachment);

                m_objBtnDel.SetActive(m_mail.getAttachment);
                m_objBtnAward.SetActive(!m_mail.getAttachment);
            }
            else
            {
                // 没有附件
                m_objAttachIconBg.SetActive(false);
                m_objBtnDel.SetActive(true);
                m_objBtnAward.SetActive(false);
            }
        }
    }
}