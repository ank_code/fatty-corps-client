using script.game.enums.battle;

namespace script.game.obj.config
{
    public class CfgBehaviorDisplay : BaseCfg
    {
        public EBattleAttType battleAttType;
        public EBhvCasterDisplayType self_display_type;
        public EBhvTargetDisplayType target_display_type;
        // 目标特效动画
        public string effect_path;
        // 投掷物动画
        public string missile_path;
    }
}