using System;

namespace script.game.net.msg.server.chat
{
    public class ChatInfo : IComparable<ChatInfo>
    {
        public int id;
        public int playerId;
        public int time;
        public int head;
        public int lv;
        public string nickname;
        public string content;
        public int CompareTo(ChatInfo other)
        {
            return this.id - other.id;
        }
    }
}