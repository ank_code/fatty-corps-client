﻿using System;
using System.Collections.Generic;
using System.Timers;

namespace kf
{
    class ATimeTask
    {
        public int tid;
        public Action callback;
        public double destTime; //单位：毫秒
        public double delay;
        public int count;
        public bool compensate; // 超时补偿

        public ATimeTask(int tid, Action callback, double destTime, double delay, int count, bool compensate)
        {
            this.tid = tid;
            this.callback = callback;
            this.destTime = destTime;
            this.delay = delay;
            this.count = count;
            this.compensate = compensate;
        }
    }
}


