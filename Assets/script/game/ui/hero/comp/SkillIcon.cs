using ak;
using kf;
using script.game.mgr;
using script.game.ui.bag.skill;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.bag.comp
{
    public class SkillIcon
    {
        private GameObject m_obj;
        private int m_skillTypeId;

        private GameObject m_objLvBg;
        private GameObject m_objImgLock;

        private Image m_imgSkillIcon;
        private TMP_Text m_txtSkillLv;

        private bool m_unlock = false;
        
        public void init(GameObject obj, bool clickable = true)
        {
            m_obj = obj;
            
            GameObject objImgSkillIcon = KtUnity.findObj(obj, "imgSkillIcon");
            m_imgSkillIcon = objImgSkillIcon.GetComponent<Image>();

            m_objLvBg = KtUnity.findObj(obj, "bg");
            GameObject objTxtSkillLv = KtUnity.findObj(obj, "txtSkillLv");
            m_txtSkillLv = objTxtSkillLv.GetComponent<TMP_Text>();
            
            m_objImgLock = KtUnity.findObj(obj, "imgLock");

            if (clickable)
            {
                KtUI.addListener(m_obj, KtUnity.findObj(obj, "clickArea"), () =>
                {
                    SkillUI.Param p = new SkillUI.Param();
                    p.skillTypeId = m_skillTypeId;
                    UIMgr.getInstance().show<SkillUI>(p);
                });
            }
        }

        public void setActive(bool b)
        {
            m_obj.SetActive(b);
        }

        public void setSkillTypeId(int skillTypeId)
        {
            m_skillTypeId = skillTypeId;
            var cfgSkill = CfgMgr.getInstance().getSkill(skillTypeId);
            m_imgSkillIcon.sprite = ResMgr.getInstance().load<Sprite>(cfgSkill.icon);
            
            m_txtSkillLv.text = cfgSkill.lv + "";
        }

        public void setUnLock(bool unlock)
        {
            m_unlock = unlock;
            if (m_unlock)
            {
                m_objLvBg.SetActive(true);
                m_objImgLock.SetActive(false);
            }
            else
            {
                m_objLvBg.SetActive(false);
                m_objImgLock.SetActive(true);
            }
        }
    }
}