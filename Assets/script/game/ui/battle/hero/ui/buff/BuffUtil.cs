namespace script.game.ui.battle
{
    public class BuffUtil
    {
        private const string BUFF_ICON_PATH_PREFIX = "Assets/res/img/buff/";
        private const string BUFF_ICON_SUFFIX = ".png";

        public static string getIconPath(int buffTypeId)
        {
            return BUFF_ICON_PATH_PREFIX + buffTypeId + BUFF_ICON_SUFFIX;
        }
    }
}