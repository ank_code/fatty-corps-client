using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using HybridCLR;
using UnityEngine;
using YooAsset;

namespace kf.hotpUpdate.state
{
    public class InitHybrid : FsmState
    {
        private static List<string> AOTMetaAssemblyFiles { get; } = new List<string>()
        {
            "mscorlib.dll",
            "System.dll",
            "System.Core.dll",
        };

        private const string DLL_FILE_SUFFIX = ".dll.bytes";
        
        public override void onEnter()
        {
            HotUpdate.HotUpdateMono mono = (HotUpdate.HotUpdateMono)getMachine().getBlackboard(HotUpdate.BBK_MONOBEHAVIOUR);
            mono.startCoroutine(init());
        }
        
        private IEnumerator init()
        {
            yield return null;

            var scriptDllFolder = (string)getMachine().getBlackboard(HotUpdate.BBK_SCRIPT_DLL_FOLDER);
            var playMode = (EPlayMode)getMachine().getBlackboard(HotUpdate.BBK_PLAY_MODE);
            string assemblyName = (string)getMachine().getBlackboard(HotUpdate.BBK_HOTUPDATE_ASSEMBLY_NAME);
            var packageName = (string)getMachine().getBlackboard(HotUpdate.BBK_YOOASSET_PACKAGE_NAME);
            var package = YooAssets.GetPackage(packageName);
            
            Assembly hotUpdateAss = null;
            
            // 加载aot
            for (var i = 0; i < AOTMetaAssemblyFiles.Count; i++)
            {
                string path = scriptDllFolder + "/" + AOTMetaAssemblyFiles[i];

                byte[] bytes = null;
                if (playMode == EPlayMode.EditorSimulateMode)
                {
                    // 开发模式并且编辑器环境下不需要加载aot
#if !UNITY_EDITOR
                    var handle = package.LoadRawFileAsync(path);
                    yield return handle;
                    bytes = handle.GetRawFileData();
#endif
                }
                else
                {
                    var handle = package.LoadRawFileAsync(path);
                    yield return handle;
                    bytes = handle.GetRawFileData();
                }

                RuntimeApi.LoadMetadataForAOTAssembly(bytes, HomologousImageMode.SuperSet);
            }

#if UNITY_EDITOR
            {
                var assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
                for (var i = 0; i < assemblies.Length; i++)
                {
                    if (assemblies[i].GetName().Name == assemblyName)
                    {
                        hotUpdateAss = assemblies[i];
                        break;
                    }
                }
            }
#else
            {
                // 使用YooAsset加载dll文件
                string path = scriptDllFolder + "/" + assemblyName + DLL_FILE_SUFFIX;
                var handle = package.LoadRawFileAsync(path);
                yield return handle;

                hotUpdateAss = Assembly.Load(handle.GetRawFileData());
            }
#endif

            // if (playMode == EPlayMode.HostPlayMode)
            // {
            //     // 使用YooAsset加载dll文件
            //     string path = scriptDllFolder + "/" + assemblyName + DLL_FILE_SUFFIX;
            //     var handle = package.LoadRawFileAsync(path);
            //     yield return handle;
            //
            //     hotUpdateAss = Assembly.Load(handle.GetRawFileData());
            // } 
            // else
            // {
            //     var assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
            //     for (var i = 0; i < assemblies.Length; i++)
            //     {
            //         if (assemblies[i].GetName().Name == assemblyName)
            //         {
            //             hotUpdateAss = assemblies[i];
            //             break;
            //         }
            //     }
            // }
            
            getMachine().setBlackboard(HotUpdate.BBK_HOTUPDATE_ASSEMBLY, hotUpdateAss);
            getMachine().setState<CleanCache>();
        }
    }
}