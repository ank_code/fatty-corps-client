
// 注册热更新前需要使用的ui

using kf;
using UnityEngine;

public class LaunchUIRegister : Singleton<LaunchUIRegister>
{
    public void init()
    {
        UIMgr.getInstance().reg(typeof(HotUpdateUI), "Assets/res/buildin/prefab/hotUpdateUI.prefab");
        UIMgr.getInstance().reg(typeof(AlertWnd), "Assets/res/buildin/prefab/alertWnd.prefab");
    }
}