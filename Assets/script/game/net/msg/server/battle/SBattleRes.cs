using System.Collections.Generic;
using kf;

namespace script.game.net.msg.server
{
    public class SBattleRes
    {
        public List<BattleHeroInfo> heroes;
        public EBattleResult br;
        public List<RecElem> records;
        // 战斗名
        public string name;
    }
}