namespace script.game.net.msg.server
{
    public enum ERecEffectType
    {
        // 加buff
        AddBuff,
        // 移除buff
        RemoveBuff,
        // 伤害
        Damage,
        // 增加能量
        AddEnergy,
        // 死亡
        Dead,
        // 复活
        Revive
    }
}