namespace kf
{
    // 模态界面，自带半透明背景
    public interface IBaseModalUI : IBaseUI
    {
        // 模态界面背景透明度，默认50%
        public float modalBgTrans();
        // 模态界面背景被点击，默认行为关闭界面
        public void onClickModalBg();
    }
}