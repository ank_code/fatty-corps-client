﻿using UnityEngine;
using System.Collections.Generic;
using Object = System.Object;

namespace kf
{
    public class EventMgr : Singleton<EventMgr>
    {
        private HashSet<IEventListener> m_listeners = new HashSet<IEventListener>();

        public delegate bool scope(IEventListener listenerObj);

        public class EventException : System.Exception
        {

        }

        // private scope m_onBefore = null;
        // private scope m_onComplete = null;

        public void addEventListener(IEventListener l)
        {
            m_listeners.Add(l);
        }

        public void removeEventListener(IEventListener l)
        {
            m_listeners.Remove(l);
        }

        public void sendEvent(int eId, Object[] objs = null, scope onBefore = null, scope onComplete = null)
        {
            HashSet<IEventListener> listenersCopy = new HashSet<IEventListener>(m_listeners);
            
            var enumerator = listenersCopy.GetEnumerator();
            while (enumerator.MoveNext())
            {
                IEventListener l = enumerator.Current;
                bool doNext = true;
                if (onBefore != null)
                {
                    doNext = onBefore(l);
                }

                if (doNext)
                {
                    try
                    {
                        l.onEvent(eId, objs);
                    }
                    catch (EventException e)
                    {
                        // 手动抛出异常的代码，可能是要跳转页面，后面监听者就可能已经被干掉了
                        Debug.LogError("aktest Exception:" + e.ToString());
                        break;
                    }

                    if (onComplete != null)
                    {
                        onComplete(l);
                    }
                }
            }
        }
    }
}