﻿
namespace kf
{
    public enum ETimeUnit
    {
        Millisecond,
        Second,
        Minute,
        Hour,
        Day
    }
}