namespace script.game.ui.bag.type
{
    public class HeroUICampUtil
    {
        public enum EUICamp
        {
            All,
            Shui,
            Huo,
            Mu,
            Sheng,
            Guang,
            An
        }

        public static string getUICampName(EUICamp e)
        {
            switch (e)
            {
                case EUICamp.All:
                    return "所有";
                case EUICamp.Shui:
                    return "水系";
                case EUICamp.Huo:
                    return "火系";
                case EUICamp.Mu:
                    return "木系";
                case EUICamp.Sheng:
                    return "圣系";
                case EUICamp.Guang:
                    return "光系";
                case EUICamp.An:
                    return "暗系";
            }

            return "未知";
        }
    }
}