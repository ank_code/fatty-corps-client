﻿using System;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;


//1 //实例化计时类
//2 PETimer pt = new PETimer();
//3 //时间定时任务
//4 pt.AddTimerTask(TimerTask, 500, EATimeUnit.Millisecond, 3);
//5 //帧数定时任务
//6 pt.AddFrameTask(FrameTask, 100, 3);
//7
//8 int tempID = pt.AddTimerTask(() => {
//9     Debug.Log("定时等待替换......");
//10 }, 1, EATimeUnit.Second, 0);
//11
//12 //定时任务替换
//13 pt.ReplaceTimeTask(tempID, () => {
//14     Debug.Log("定时任务替换完成......");
//15 }, 2, EATimeUnit.Second, 0);
//16
//17 //定时任务删除
//18 pt.DeleteTimeTask(tempID);

namespace kf
{
    public class TimerMgr : Singleton<TimerMgr>
    {
        private Action<string> taskLog;

        private static readonly string lockTid = "lockTid";
        private DateTime startDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
        private double nowTime;

        private int tid;
        private List<int> tidLst = new List<int>();
        private List<int> recTidLst = new List<int>();

        private static readonly string lockTime = "lockTime";
        private List<ATimeTask> tmpTimeLst = new List<ATimeTask>();
        private List<ATimeTask> taskTimeLst = new List<ATimeTask>();
        private List<int> tmpDelTimeLst = new List<int>();

        private int frameCounter;
        private static readonly string lockFrame = "lockFrame";
        private List<AFrameTask> tmpFrameLst = new List<AFrameTask>();
        private List<AFrameTask> taskFrameLst = new List<AFrameTask>();
        private List<int> tmpDelFrameLst = new List<int>();

        public void Update()
        {
            CheckTimeTask();
            CheckFrameTask();

            DelTimeTask();
            DelFrameTask();

            if (recTidLst.Count > 0)
            {
                lock (lockTid)
                {
                    RecycleTid();
                }
            }
        }
        private void DelTimeTask()
        {
            if (tmpDelTimeLst.Count > 0)
            {
                lock (lockTime)
                {
                    for (int i = 0; i < tmpDelTimeLst.Count; i++)
                    {
                        bool isDel = false;
                        int delTid = tmpDelTimeLst[i];
                        for (int j = 0; j < taskTimeLst.Count; j++)
                        {
                            ATimeTask task = taskTimeLst[j];
                            if (task.tid == delTid)
                            {
                                isDel = true;
                                taskTimeLst.RemoveAt(j);
                                recTidLst.Add(delTid);
                                //LogInfo("Del taskTimeLst ID:" + System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());
                                break;
                            }
                        }

                        if (isDel)
                            continue;

                        for (int j = 0; j < tmpTimeLst.Count; j++)
                        {
                            ATimeTask task = tmpTimeLst[j];
                            if (task.tid == delTid)
                            {
                                tmpTimeLst.RemoveAt(j);
                                recTidLst.Add(delTid);
                                //LogInfo("Del tmpTimeLst ID:" + System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());
                                break;
                            }
                        }
                    }
                }
            }
        }
        private void DelFrameTask()
        {
            if (tmpDelFrameLst.Count > 0)
            {
                lock (lockFrame)
                {
                    for (int i = 0; i < tmpDelFrameLst.Count; i++)
                    {
                        bool isDel = false;
                        int delTid = tmpDelFrameLst[i];
                        for (int j = 0; j < taskFrameLst.Count; j++)
                        {
                            AFrameTask task = taskFrameLst[j];
                            if (task.tid == delTid)
                            {
                                isDel = true;
                                taskFrameLst.RemoveAt(j);
                                recTidLst.Add(delTid);
                                break;
                            }
                        }

                        if (isDel)
                            continue;

                        for (int j = 0; j < tmpFrameLst.Count; j++)
                        {
                            AFrameTask task = tmpFrameLst[j];
                            if (task.tid == delTid)
                            {
                                tmpFrameLst.RemoveAt(j);
                                recTidLst.Add(delTid);
                                break;
                            }
                        }
                    }
                }
            }
        }
        private void CheckTimeTask()
        {
            if (tmpTimeLst.Count > 0)
            {
                lock (lockTime)
                {
                    //加入缓存区中的定时任务
                    for (int tmpIndex = 0; tmpIndex < tmpTimeLst.Count; tmpIndex++)
                    {
                        taskTimeLst.Add(tmpTimeLst[tmpIndex]);
                    }
                    tmpTimeLst.Clear();
                }
            }

            //遍历检测任务是否达到条件
            nowTime = GetUTCMilliseconds();
            for (int index = 0; index < taskTimeLst.Count; index++)
            {
                ATimeTask task = taskTimeLst[index];
                if (nowTime.CompareTo(task.destTime) < 0)
                {
                    continue;
                }
                else
                {
                    Action cb = task.callback;

                    bool rm = false;
                    //移除已经完成的任务
                    if (task.count == 1)
                    {
                        rm = true;
                    }
                    else
                    {
                        // 超时补偿则保证一定会执行预设的次数
                        if (task.compensate)
                        {
                            if (task.count != 0)
                            {
                                task.count -= 1;
                            }
                        
                            task.destTime += task.delay;
                        }
                        else
                        {
                            // 超时不补偿
                            if (task.count > 0)
                            {
                                rm = true;
                            } 

                            task.destTime += task.delay;
                            
                            if (task.destTime.CompareTo(nowTime) < 0)
                            {
                                Debug.Log("aktest 触发超时不补偿，置空逻辑");
                                task.destTime = nowTime;
                            }
                        }
                    }

                    if (rm)
                    {
                        taskTimeLst.RemoveAt(index);
                        index--;
                        recTidLst.Add(task.tid);
                    }

                    if (cb != null)
                    {
                        cb();
                    }
                }
            }
        }
        private void CheckFrameTask()
        {
            if (tmpFrameLst.Count > 0)
            {
                lock (lockFrame)
                {
                    //加入缓存区中的定时任务
                    for (int tmpIndex = 0; tmpIndex < tmpFrameLst.Count; tmpIndex++)
                    {
                        taskFrameLst.Add(tmpFrameLst[tmpIndex]);
                    }
                    tmpFrameLst.Clear();
                }
            }

            frameCounter += 1;
            //遍历检测任务是否达到条件
            for (int index = 0; index < taskFrameLst.Count; index++)
            {
                AFrameTask task = taskFrameLst[index];
                if (frameCounter < task.destFrame)
                {
                    continue;
                }
                else
                {
                    Action cb = task.callback;
                    try
                    {
                        if (cb != null)
                        {
                            cb();
                        }
                    }
                    catch (Exception e)
                    {
                        LogInfo(e.ToString());
                    }

                    //移除已经完成的任务
                    if (task.count == 1)
                    {
                        //UnityEngine.Debug.Log("aktest timer del callback="+ task.callback);
                        taskFrameLst.RemoveAt(index);
                        index--;
                        recTidLst.Add(task.tid);
                    }
                    else
                    {
                        if (task.compensate)
                        {
                            
                        }
                        if (task.count != 0)
                        {
                            task.count -= 1;
                        }
                        
                        task.destFrame += task.delay;
                    }
                }
            }
        }

        #region TimeTask
        public int AddTimerTask(Action callback, double delay, ETimeUnit timeUnit = ETimeUnit.Second, int count = 1, bool compensate = true)
        {
            //UnityEngine.Debug.Log("aktest AddTimerTask callback="+ callback.ToString());
            if (timeUnit != ETimeUnit.Millisecond)
            {
                switch (timeUnit)
                {
                    case ETimeUnit.Second:
                        delay = delay * 1000;
                        break;
                    case ETimeUnit.Minute:
                        delay = delay * 1000 * 60;
                        break;
                    case ETimeUnit.Hour:
                        delay = delay * 1000 * 60 * 60;
                        break;
                    case ETimeUnit.Day:
                        delay = delay * 1000 * 60 * 60 * 24;
                        break;
                    default:
                        LogInfo("Add Task TimeUnit Type Error...");
                        break;
                }
            }
            int tid = GetTid(); ;
            nowTime = GetUTCMilliseconds();
            lock (lockTime)
            {
                tmpTimeLst.Add(new ATimeTask(tid, callback, nowTime + delay, delay, count, compensate));
            }
            return tid;
        }
        public void DeleteTimeTask(int tid)
        {
            lock (lockTime)
            {
                tmpDelTimeLst.Add(tid);
                //LogInfo("TmpDel ID:" + System.Threading.Thread.CurrentThread.ManagedThreadId.ToString());
            }

        }
        public bool ReplaceTimeTask(int tid, Action callback, float delay, ETimeUnit timeUnit = ETimeUnit.Millisecond, int count = 1, bool compensate = true)
        {
            if (timeUnit != ETimeUnit.Millisecond)
            {
                switch (timeUnit)
                {
                    case ETimeUnit.Second:
                        delay = delay * 1000;
                        break;
                    case ETimeUnit.Minute:
                        delay = delay * 1000 * 60;
                        break;
                    case ETimeUnit.Hour:
                        delay = delay * 1000 * 60 * 60;
                        break;
                    case ETimeUnit.Day:
                        delay = delay * 1000 * 60 * 60 * 24;
                        break;
                    default:
                        LogInfo("Replace Task TimeUnit Type Error...");
                        break;
                }
            }
            nowTime = GetUTCMilliseconds();
            ATimeTask newTask = new ATimeTask(tid, callback, nowTime + delay, delay, count, compensate);

            bool isRep = false;
            for (int i = 0; i < taskTimeLst.Count; i++)
            {
                if (taskTimeLst[i].tid == tid)
                {
                    taskTimeLst[i] = newTask;
                    isRep = true;
                    break;
                }
            }

            if (!isRep)
            {
                for (int i = 0; i < tmpTimeLst.Count; i++)
                {
                    if (tmpTimeLst[i].tid == tid)
                    {
                        tmpTimeLst[i] = newTask;
                        isRep = true;
                        break;
                    }
                }
            }

            return isRep;
        }
        #endregion

        #region FrameTask
        public int AddFrameTask(Action callback, int delay, int count = 1, bool compensate = true)
        {
            int tid = GetTid();
            lock (lockTime)
            {
                tmpFrameLst.Add(new AFrameTask(tid, callback, frameCounter + delay, delay, count, compensate));
            }
            return tid;
        }
        public void DeleteFrameTask(int tid)
        {
            lock (lockFrame)
            {
                tmpDelFrameLst.Add(tid);
            }
        }
        public bool ReplaceFrameTask(int tid, Action callback, int delay, int count = 1, bool compensate = true)
        {
            AFrameTask newTask = new AFrameTask(tid, callback, frameCounter + delay, delay, count, compensate);

            bool isRep = false;
            for (int i = 0; i < taskFrameLst.Count; i++)
            {
                if (taskFrameLst[i].tid == tid)
                {
                    taskFrameLst[i] = newTask;
                    isRep = true;
                    break;
                }
            }

            if (!isRep)
            {
                for (int i = 0; i < tmpFrameLst.Count; i++)
                {
                    if (tmpFrameLst[i].tid == tid)
                    {
                        tmpFrameLst[i] = newTask;
                        isRep = true;
                        break;
                    }
                }
            }

            return isRep;
        }
        #endregion
        public void SetLog(Action<string> handle)
        {
            taskLog = handle;
        }

        public void Reset()
        {
            tid = 0;
            tidLst.Clear();
            recTidLst.Clear();

            tmpTimeLst.Clear();
            taskTimeLst.Clear();

            tmpFrameLst.Clear();
            taskFrameLst.Clear();

            taskLog = null;
        }

        #region Tool Methonds
        private int GetTid()
        {
            lock (lockTid)
            {
                tid += 1;

                //安全代码，以防万一
                while (true)
                {
                    if (tid == int.MaxValue)
                    {
                        tid = 0;
                    }

                    bool used = false;
                    for (int i = 0; i < tidLst.Count; i++)
                    {
                        if (tid == tidLst[i])
                        {
                            used = true;
                            break;
                        }
                    }
                    if (!used)
                    {
                        tidLst.Add(tid);
                        break;
                    }
                    else
                    {
                        tid += 1;
                    }
                }
            }

            return tid;
        }
        private void RecycleTid()
        {
            for (int i = 0; i < recTidLst.Count; i++)
            {
                int tid = recTidLst[i];

                for (int j = 0; j < tidLst.Count; j++)
                {
                    if (tidLst[j] == tid)
                    {
                        tidLst.RemoveAt(j);
                        break;
                    }
                }
            }
            recTidLst.Clear();
        }
        private void LogInfo(string info)
        {
            UnityEngine.Debug.Log("aktest error:" + info);

            if (taskLog != null)
            {
                taskLog(info);
            }
            else
            {
                throw new Exception(info);
            }
        }
        private double GetUTCMilliseconds()
        {
            TimeSpan ts = DateTime.UtcNow - startDateTime;
            return ts.TotalMilliseconds;
        }
        #endregion


    }
}