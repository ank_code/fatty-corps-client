using System.Collections.Generic;
using ak;
using kf;
using script.game.enums;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.loading
{
    public class SceneLoadingUI : GameUI
    {
        private Image m_imgBg;
        private Slider m_sliderSchedule;
        private TMP_Text m_txtTip;

        private List<string> m_tips = new List<string>();

        // 切换提示时间，暂时应该用不到
        private const float TIPS_CHANGE_TIME = 3.0f;
        
        // todo: 根据加载类型不同，使用不同的背景
        public override void onCreate(IUIParam param)
        {
            GameObject obj = KtUnity.findObj(gameObject, "imgBg");
            m_imgBg = obj.GetComponent<Image>();
            
            obj = KtUnity.findObj(gameObject, "sliderSchedule");
            m_sliderSchedule = obj.GetComponent<Slider>();
            
            obj = KtUnity.findObj(gameObject, "txtTip");
            m_txtTip = obj.GetComponent<TMP_Text>();

            initTips();
            updateTip();
        }

        public override void onShow()
        {
            updateTip();
            m_sliderSchedule.value = 0;
            
            // 顺便清理无用缓存，这个界面卡一卡没影响
            Resources.UnloadUnusedAssets();
        }

        private void initTips()
        {
            m_tips.Add("这里是提示1");
            m_tips.Add("这里是提示2");
            m_tips.Add("这里是提示3");
        }

        private void updateTip()
        {
            m_txtTip.text = randTip();
        }

        private string randTip()
        {
            var index = Random.Range(0, m_tips.Count);
            return m_tips[index];
        }

        public override void onEvent(int eId, object[] objs)
        {
            EEvent e = (EEvent)eId;
            switch (e)
            {
                case EEvent.OnLoadingUISchedule:
                    float value = (float)objs[0];
                    m_sliderSchedule.value = value;
                    break;
            }
        }

        public override EUILayer getUILayer()
        {
            return EUILayer.Loading;
        }
    }
}