using System.Collections.Generic;
using ak;
using DG.Tweening;
using kf;
using script.game.enums;
using script.game.enums.item;
using script.game.mgr;
using script.game.net.http;
using script.game.net.msg.server.item;
using script.game.ui.bag.type;
using script.game.ui.comm.comp;
using SuperScrollView;
using UnityEngine;

namespace script.game.ui.bag
{
    public class BagUI : GameUI
    {
        public class UIParam : IUIParam
        {
            public List<Item> itemList;
        }

        private List<Item> m_allItemList;
        private List<Item> m_curItemList = new List<Item>();
        
        private LoopListView2 m_loopListView;
        private CurrencyInfo m_currencyInfo;
        private ItemDetail m_detail;

        private GameObject m_objScrollView;
        private GameObject m_objTypeTabBar;
        private GameObject m_objType;
        private GameObject m_objDetail;
        private GameObject m_objBagLeft;
        private GameObject m_objBagRight;

        public class BagItemData
        {
            public bool choosed;
            public Item item;
        }
        
        private List<BagItemData> m_curItemDataList = new List<BagItemData>();
        
        // itemId->BagItem
        private Dictionary<int, BagItem> m_items = new Dictionary<int, BagItem>();
        
        private bool m_initLoopList = false;

        private const int PER_ROW_ITEM_NUM = 8;
        private const string ITEM_PREFAB_PATH = "Assets/res/prefab/bag/bagItem.prefab";
        private const float ENTER_ANIM_TIME = 0.3f;
        private const float ENTER_ANIM_DIS = 300f;

        public static void open()
        {
            GameRequest.getInstance().getItemList((rep) =>
            {
                BagUI.UIParam param = new BagUI.UIParam();
                param.itemList = rep.itemList;
                UIMgr.getInstance().show<BagUI>(param);
            });
        }
        
        public override void onCreate(IUIParam param)
        {
            m_allItemList = ((UIParam)param).itemList;
            
            GameObject m_objBtnClose = KtUnity.findObj(gameObject, "btnClose");
            KtUI.addListener(m_objBtnClose, () =>
            {
                this.close();
            });
            
            m_objScrollView = KtUnity.findObj(gameObject, "scrollView");
            m_loopListView = m_objScrollView.GetComponent<LoopListView2>();
            
            // detail
            m_objDetail = KtUnity.findObj(gameObject, "itemDetail");
            m_detail = m_objDetail.AddComponent<ItemDetail>();
            m_detail.setVisible(false);
            
            // type tabbar
            m_objType = KtUnity.findObj(gameObject, "type");
            m_objTypeTabBar = KtUnity.findObj(gameObject, "typeContent");
            KtUnity.addComponent<ItemTypeTabBar>(m_objTypeTabBar);
            
            // 货币
            GameObject objCurrency = KtUnity.findObj(gameObject, "currency");
            m_currencyInfo = new CurrencyInfo();
            m_currencyInfo.init(objCurrency, () =>
            {
                m_currencyInfo.add(CurrencyInfo.ECurrencyType.Money);            
                m_currencyInfo.add(CurrencyInfo.ECurrencyType.Coin);
            });

            m_objBagLeft = KtUnity.findObj(gameObject, "bagLeft");
            m_objBagRight = KtUnity.findObj(gameObject, "bagRight");
        }

        public override void onShow()
        {
            // 入场动画
            m_objBagLeft.transform.DOLocalMoveX(-ENTER_ANIM_DIS, ENTER_ANIM_TIME).From();
            m_objBagRight.transform.DOLocalMoveX(ENTER_ANIM_DIS, ENTER_ANIM_TIME).From();
        }

        private void initData(EItemType itemType)
        {
            clearItems();

            // 筛选当前分类下所有道具
            m_curItemList.Clear();
            for (var i = 0; i < m_allItemList.Count; i++)
            {
                var cfgItem = CfgMgr.getInstance().getItem(m_allItemList[i].typeId);
                if (cfgItem.type == itemType)
                {
                    m_curItemList.Add(m_allItemList[i]);
                }
            }

            for (var i = 0; i < m_curItemList.Count; i++)
            {
                BagItemData data = new BagItemData();
                data.item = m_curItemList[i];
                data.choosed = false;
                m_curItemDataList.Add(data);
            }
            
            // 重置当前分类列表
            int rows = m_curItemList.Count / PER_ROW_ITEM_NUM;
            if (m_curItemList.Count % PER_ROW_ITEM_NUM > 0)
            {
                rows++;
            }

            if (m_initLoopList)
            {
                m_loopListView.SetListItemCount(rows, false);
                m_loopListView.RefreshAllShownItem();
            }
            else
            {
                m_loopListView.InitListView(rows, onGetItem);
                m_initLoopList = true;
            }
        }

        private void clearItems()
        {
            foreach (var keyValuePair in m_items)
            {
                keyValuePair.Value.setChoosed(false);
            }
            
            m_items.Clear();
            m_curItemDataList.Clear();
            m_detail.setVisible(false);
        }

        private LoopListViewItem2 onGetItem(LoopListView2 view2, int rowIndex)
        {
            if (rowIndex < 0)
            {
                return null;
            }

            var row = view2.NewListViewItem("bagItemRow");
            row.gameObject.name = "row_" + rowIndex;
            
            for (int i = 0; i < row.transform.childCount; i++)
            {
                GameObject objItem = row.transform.GetChild(i).gameObject;
                var bagItem = KtUnity.addComponent<BagItem>(objItem);
                int index = rowIndex * PER_ROW_ITEM_NUM + i;
                
                if (index >= m_curItemList.Count)
                {
                    bagItem.setVisible(false);
                    continue;
                }
                
                objItem.name = "item_" + index;
                
                Item itemData = m_curItemDataList[index].item;
                bool choosed = m_curItemDataList[index].choosed;
                
                bagItem.reload(itemData, index);
                bagItem.setChoosed(choosed);
                bagItem.setOnClick(() =>
                {
                    // 选中
                    GameEvent.sendEvent(EEvent.OnBagItemClick, new object[] {itemData.id});
                });

                m_items[itemData.id] = bagItem;
            }
            
            return row;
        }

        public override void onEvent(int eId, object[] objs)
        {
            EEvent e = (EEvent)eId;
            switch (e)
            {
                case EEvent.OnBagItemClick:
                {
                    int clickId = (int)objs[0];
                    foreach (BagItem value in m_items.Values)
                    {
                        if (value.getId() != clickId && value.isChoosed())
                        {
                            value.setChoosed(false);
                            m_curItemDataList[value.getIndex()].choosed = false;
                            break;
                        }
                    }
                    
                    m_items[clickId].setChoosed(true);
                    var mItemData = m_curItemDataList[m_items[clickId].getIndex()];
                    mItemData.choosed = true;
                    m_detail.load(mItemData.item);

                    break;
                }
                case EEvent.OnBagSwitchType:
                {
                    EItemType type = (EItemType)objs[0];
                    initData(type);
                    break;
                }

                case EEvent.OnSellItemOk:
                {
                    int itemId = (int)objs[0];
                    int num = (int)objs[1];
                    m_items[itemId].delNum(num);
                    
                    for (var i = 0; i < m_allItemList.Count; i++)
                    {
                        if (m_allItemList[i].id == itemId)
                        {
                            m_allItemList[i].num -= num;
                            break;
                        }
                    }
                    break;
                }
            }
        }
    }
}