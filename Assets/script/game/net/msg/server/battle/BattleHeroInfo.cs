using script.game.enums;

namespace script.game.net.msg.server
{
    public class BattleHeroInfo
    {
        public int typeId;
        public int lv;
        public long maxHp;
        public int initEnergy;
        public HeroPos pos;
    }
}