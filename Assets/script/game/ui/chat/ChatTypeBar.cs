using System;
using kf;
using script.game.enums;
using script.game.enums.item;
using script.game.ui.bag.type;
using script.game.ui.comm.comp;
using UnityEngine;
using Object = System.Object;

namespace script.game.ui.chat
{
    
    public class ChatTypeBar : MonoBehaviour
    {
        private TabBar<TxtTab> m_tabBar = new TabBar<TxtTab>();

        private void Awake()
        {
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                var objTab = gameObject.transform.GetChild(i).gameObject;
                TxtTab tab = new TxtTab();
                ChatUI.EChatChannelType type = (ChatUI.EChatChannelType)i;
                tab.init(objTab, () =>
                {
                    EventMgr.getInstance().sendEvent((int)EEvent.OnChatSwitchChannel, new Object[]{type});
                });
                
                m_tabBar.addTab(tab);
            }
            
            m_tabBar.getTabs()[0].setTabState(ABaseTab.EState.Focus);
        }
    }
}