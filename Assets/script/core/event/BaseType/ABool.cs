﻿using UnityEngine;
using System.Collections;

namespace kf
{
    public class ABool : UnityEngine.Object
    {
        public bool value;

        public static ABool makeABool(bool v)
        {
            ABool i = new ABool();
            i.value = v;
            return i;
        }
    }
}

