using System;
using ak;
using kf.anim;
using UnityEngine;

namespace script.game.ui.battle.effect.anim
{
    /**
     * 全屏特效动画，播放一次后销毁
     */
    public class EffectAnim
    {
        private SpineAnim m_sa;
        private Action<SpineAnim.CallbackEvent> m_callback;
        
        public void init(GameObject parent, string animPath, Vector3 pos, bool flip, Action callback)
        {
            new SpineAnim(false, animPath, parent, SpineAnim.EType.UI, (sa) =>
            {
                m_sa = sa;
                m_sa.getObj().transform.position = pos;

                if (flip)
                {
                    var scale = m_sa.getObj().transform.localScale;
                    scale.x *= -1.0f;
                    m_sa.getObj().transform.localScale = scale;
                }

                callback();
            });
        }

        public void playOnce(Action<SpineAnim.CallbackEvent> callback)
        {
            m_callback = callback;
            m_sa.playOnce("", effectAnimCallback);
        }

        public void destory()
        {
            KtUnity.destory(m_sa.getObj());
        }
        
        private void effectAnimCallback(SpineAnim.CallbackEvent ce)
        {
            if (m_callback != null)
            {
                m_callback(ce);
            }
            
            if (ce.e.Equals(SpineAnim.ECallBackEvent.Complete))
            {
                destory();
            }
        }
    }
}