﻿using UnityEngine;
using System.Collections;

namespace kf
{
    public class AFloat : UnityEngine.Object
    {
        public float value;

        public static AFloat makeAFloat(float v)
        {
            AFloat i = new AFloat();
            i.value = v;
            return i;
        }
    }
}