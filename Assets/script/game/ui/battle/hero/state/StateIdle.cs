using kf;
using kf.anim;

namespace script.game.ui.battle.state
{
    public class StateIdle : FsmState
    {
        public override void onEnter()
        {
            SpineAnim sa = (SpineAnim)getBlackboard(BattleHero.BB_SPINE_ANIM);
            sa.playLoop(BattleHero.ANIM_NAME_IDLE);
        }
    }
}