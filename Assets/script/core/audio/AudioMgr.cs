using System.Collections;
using System.Collections.Generic;
using ak;
using UnityEngine;

namespace kf
{
    public class AudioMgr : Singleton<AudioMgr>
{
    public enum SOUND_TYPE {
        SOUND_TYPE_NONE,
        SOUND_TYPE_ONLY,    // 同一时间仅能出现一个的声音，如果有新声音将打断其他声音
        SOUND_TYPE_OVERLAP, // 可叠加的声音，同一时间可出现多个
        SOUND_TYPE_BG,      // 可叠加并且不会被打断
        SOUND_TYPE_MAX,
    }

    public class Sound {
        public int id;
        public string path;
        public AudioSource audioS;
        public System.Action onComplete;
        public GameObject obj;
        public SOUND_TYPE type;
        public long playBTime;
    }

    private GameObject m_parent;
    private Hashtable m_sounds = new Hashtable();
    // private float m_bgmVol = 1.0f;
    private float m_soundVol = 1.0f;
    private AudioSource m_asBg;
    private int m_nId;

    private object m_lock = new object();

    // 50毫秒内不播放同样的声音
    private long DIS_PLAY_SAME_TIME_MILL = 50;

    public void init(GameObject parent) {
        m_parent = parent;
        //m_asBg = UnityAdp.addComponent<AudioSource>(parent);
        m_nId = 0;
    }

    public void playMusic(string musicPath) {

    }

    // 播放声音
    public int playSound(string soundPath, SOUND_TYPE type = SOUND_TYPE.SOUND_TYPE_OVERLAP, System.Action onComplete = null, bool loop = false) {
        Sound playingSound = getSoundByPath(soundPath);
        long curMillTime = KtTime.getTimeStampToMilliseconds();

        //Debug.Log("aktest playingSound="+ playingSound+ " curMillTime="+ curMillTime);
        if (playingSound != null && DIS_PLAY_SAME_TIME_MILL + playingSound.playBTime > curMillTime) {
            Debug.Log("aktest playSound same time play soundPath=" + soundPath);
            return 0;
        }

        AudioClip ac = ResMgr.getInstance().loadFromResources<AudioClip>(soundPath);
        if (ac == null) {
            Debug.Log("aktest playSound can not find soundPath=" + soundPath);
            return 0;
        }

        //Debug.Log("播放声音:"+ soundPath);

        GameObject obj = new GameObject("sound");
        obj.transform.parent = m_parent.transform;
        AudioSource audioS = KtUnity.addComponent<AudioSource>(obj);
        audioS.clip = ac;
        audioS.loop = loop;
        audioS.volume = m_soundVol;

        if (type == SOUND_TYPE.SOUND_TYPE_ONLY) {
            releaseAllSound();
        }

        Sound s = new Sound();
        s.id = ++m_nId;
        s.path = soundPath;
        s.audioS = audioS;
        s.onComplete = onComplete;
        s.obj = obj;
        s.type = type;
        s.playBTime = curMillTime;
        addSound(s);

        audioS.Play();

        if (!loop)
        {
            TimerMgr.getInstance().AddTimerTask(() => { onSoundComplete(s); }, ac.length, ETimeUnit.Second);
        }

        return s.id;
    }

    private void addSound(Sound s) {
        lock (m_lock) {
            m_sounds.Add(s.id, s);
        }
    }

    private void onSoundComplete(Sound s) {
        releaseSound(s.id);
        
        if (s.onComplete != null) {
            s.onComplete();
        }
    }

    // 设置背景音乐音量 0~1
    public void setMusicVol(float vol) {

    }

    // 设置音效音量 0~1
    public void setSoundVol(float vol) {
        if (vol < 0.0f || vol > 1.0f) {
            return;
        }

        m_soundVol = vol;
    }

    public void pauseSound(string soundPath) {

    }

    public void pauseSound(int soundId) {
        lock (m_lock) {
            if (m_sounds[soundId] != null) {
                AudioSource audioS = ((Sound)m_sounds[soundId]).obj.GetComponent<AudioSource>();
                audioS.Pause();
            }
        }
    }

    public void resumeSound(string soundPath) {

    }

    public void resumeSound(int soundId) {
        lock (m_lock)
        {
            if (m_sounds[soundId] != null)
            {
                AudioSource audioS = ((Sound)m_sounds[soundId]).audioS;
                audioS.Play();
            }
        }
    }

    public void releaseSound(string soundPath)
    {
        lock (m_lock)
        {
            Hashtable t = new Hashtable(m_sounds);
            foreach (DictionaryEntry entry in t)
            {
                if (((Sound)entry.Value).path.Equals(soundPath))
                {
                    releaseSound((int)entry.Key);
                }
            }
        }
    }

    public void releaseSound(int soundId) {
        Sound s = (Sound)m_sounds[soundId];
        if (s == null)
        {
            return;
        }

        //Debug.Log("aktest releaseSound=" + s.path);

        s.audioS.Stop();
        KtUnity.destory(s.obj);

        lock (m_lock) { 
            m_sounds.Remove(soundId);
        }
    }

    public void pauseAllSound() {

    }

    public void resumeAllSound() {

    }

    public void releaseAllSound(bool relAllSound = false) {
        lock (m_lock)
        {
            Hashtable t = new Hashtable(m_sounds);
            foreach (DictionaryEntry entry in t)
            {
                int id = (int)entry.Key;
                if (!relAllSound && ((Sound)entry.Value).type == SOUND_TYPE.SOUND_TYPE_BG) {
                    continue;
                }

                releaseSound(id);
            }
        }
    }

    // 清除所有非循环音效
    public void releaseAllOnceSound() {
        lock (m_lock)
        {
            Hashtable t = new Hashtable(m_sounds);
            foreach (DictionaryEntry entry in t)
            {
                int id = (int)entry.Key;
                Sound s = (Sound)m_sounds[id];
                if (!s.audioS.loop) {
                    releaseSound(id);
                }
                
            }
        }
    }

    // 根据路径获取正在播放的声音
    public Sound getSoundByPath(string path) {
        lock (m_lock)
        {
            Hashtable t = new Hashtable(m_sounds);
            foreach (DictionaryEntry entry in t)
            {
                Sound s = (Sound)entry.Value;
                if (s.path.Equals(path))
                {
                    return s;
                }
            }
        }

        return null;
    }

    public float getSoundDuration(string path) {
            AudioClip ac = ResMgr.getInstance().loadFromResources<AudioClip>(path);
            if (ac == null)
            {
                return 0;
            }

            return ac.length;
        } 
    }
}

