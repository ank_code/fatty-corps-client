using ak;
using kf;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace script.game.ui.comm.comp
{
    public abstract class ABaseCurrencyElem : IEventListener
    {
        private GameObject m_obj;
        private TMP_Text m_txtNum;
        private GameObject m_objBtn;

        private const string PREFAB_PATH = "Assets/res/prefab/commen/currency/currencyElem.prefab";

        public void init(GameObject parent)
        {
            ResMgr.getInstance().load<GameObject>(PREFAB_PATH, true, true,o =>
            {
                m_obj = (GameObject)o;
                m_obj.transform.SetParent(parent.transform);
                m_obj.transform.localScale = Vector3.one;

                GameObject objImgIcon = KtUnity.findObj(m_obj, "imgIcon");
                var imgIcon = objImgIcon.GetComponent<Image>();
                ResMgr.getInstance().load<Sprite>(getIconPath(), false, true, o1 =>
                {
                    imgIcon.sprite = (Sprite)o1;
                    GameObject objTxtNum = KtUnity.findObj(m_obj, "txtNum");
                    m_txtNum = objTxtNum.GetComponent<TMP_Text>();
                    setVal(0);

                    m_objBtn = KtUnity.findObj(m_obj, "btnAdd");
                    if (!showBtnDefault())
                    {
                        m_objBtn.SetActive(false);
                    }
                    else
                    {
                        showBtnAdd();
                    }

                    setVal(getVal());

                    EventMgr.getInstance().addEventListener(this);
                    onInit();                    
                });
            });
        }

        public void showBtnAdd()
        {
            m_objBtn.SetActive(true);
            KtUI.addListener(m_objBtn, () =>
            {
                onClickAdd();
            });
        }

        public virtual void onDestory()
        {
            EventMgr.getInstance().removeEventListener(this);
        }

        protected virtual void onInit()
        {
            
        }

        public void setVal(long val)
        {
            //todo: 转化为M、B等
            m_txtNum.text = val + "";
        }

        protected abstract string getIconPath();
        protected abstract long getVal();
        protected abstract bool showBtnDefault();
        protected abstract void onClickAdd();

        public abstract void onEvent(int eId, object[] objs);
    }
}