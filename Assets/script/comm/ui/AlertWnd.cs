using System;
using ak;
using kf;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AlertWnd : BaseModalUI
{
    public enum EType
    {
        Ok,
        OkCancel
    }
    
    public class Param : IUIParam
    {
        public EType type = EType.Ok;
        public string title;
        public string info;
        public Action okCallback;
        public Action cancelCallback;
    }

    private const string DEFAULT_TITLE = "信息";
    
    public override void onCreate(IUIParam param)
    {
        Param p = (Param)param;
        var objTxtTitle = KtUnity.findObj(gameObject, "txtTitle");
        var objTxtInfo = KtUnity.findObj(gameObject, "txtInfo");
        var objBtnOk = KtUnity.findObj(gameObject, "btnOk");
        var objBtnCancel = KtUnity.findObj(gameObject, "btnCancel");
        
        Debug.Log("aktest alertWnd info=>" + objTxtInfo);

        var txtTitle = objTxtTitle.GetComponent<TMP_Text>();
        txtTitle.text = p.title == null ? DEFAULT_TITLE : p.title;
        
        var txtInfo = objTxtInfo.GetComponent<TMP_Text>();
        txtInfo.text = p.info;

        GameObject objBody = KtUnity.findObj(gameObject, "bg");
        LayoutRebuilder.ForceRebuildLayoutImmediate(objBody.GetComponent<RectTransform>());

        KtUI.addListener(objBtnOk, () =>
        {
            defaultCancelClick();
            if (p.okCallback != null)
            {
                p.okCallback();
            }
        });
        KtUI.addListener(objBtnCancel, () =>
        {
            defaultCancelClick();
            if (p.cancelCallback != null)
            {
                p.cancelCallback();
            }
        });

        if (p.type == EType.Ok)
        {
            objBtnCancel.SetActive(false);
            objBtnOk.transform.localPosition =
                new Vector3(0, objBtnOk.transform.localPosition.y, objBtnOk.transform.localPosition.z);
        }
    }

    private void defaultCancelClick()
    {
        UIMgr.getInstance().destroy<AlertWnd>();
    }
 
    public override EUILayer getUILayer()
    {
        return EUILayer.Alert;
    }
}