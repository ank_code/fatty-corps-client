using System.Collections.Generic;

namespace script.game.net.msg.server.hero
{
    public class HeroDetail
    {
        public int id;
        public int typeId = 0;
        public string heroName = ""; // 自定义昵称
        public int star = 1; // 星级
        public int lv = 1;

        public string combat; // 战力

        public string att; // 攻击
        public string def; // 防御/护甲
        public string maxHp; // 最大hp
        public string speed; // 速度
        public string skillDamagePer; // 技能伤害
        public string precisePer; // 精准
        public string parryPer; // 格挡
        public string criticalPer; // 暴击
        public string criticalDamagePer; // 暴击伤害
        public string armorBreakPer; // 破甲
        public string immunityControlPer; // 免控率
        public string reduceDamagePer; // 减伤率
        public string sacredDamagePer; // 神圣伤害
        public string reduceCriticalPer; // 暴击抵抗
        public string incDamagePer; // 增伤率
        public string incControlPer; // 增控率

        // 英雄自身解锁基础技能(客户端展示用)
        public List<int> heroSkillIds;
    }
}