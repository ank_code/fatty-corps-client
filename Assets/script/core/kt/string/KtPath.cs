using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace kf
{
    public class KtPath
    {
        // 获取路径后的文件名
        // 如：path=abc/123/abc/fn，返回fn
        public static string getFileNameByPath(string path)
        {
            return System.IO.Path.GetFileName(path);
        }
    }
}