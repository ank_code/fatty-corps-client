using System;
using ak;
using kf;
using script.game.enums;
using script.game.enums.item;
using UnityEngine;

namespace script.game.ui.bag.type
{
    public class ItemTypeTabBar : MonoBehaviour
    {
        private TabBar<ItemTypeTab> m_tabBar = new TabBar<ItemTypeTab>();

        private void Awake()
        {
            for (int i = 0; i < gameObject.transform.childCount; i++)
            {
                var objTab = gameObject.transform.GetChild(i).gameObject;
                ItemTypeTab tab = new ItemTypeTab();
                EItemType itemType = (EItemType)Enum.GetValues(typeof(EItemType)).GetValue(i);
                tab.init(objTab, itemType);
                m_tabBar.addTab(tab);
            }
            
            m_tabBar.getTabs()[0].setTabState(ABaseTab.EState.Focus);
        }
    }
}