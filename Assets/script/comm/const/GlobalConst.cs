
/**
 * 与游戏相关的常量
 */
public class GlobalConst
{
    // 屏幕设计宽高
    public const int DES_SCREEN_WIDTH = 1920;
    public const int DES_SCREEN_HEIGTH = 1080;

    // 脚本dll存放的位置
    public const string HOTUPDATE_DLL_FOLDER = "Assets/res/script";
    // 热更程序集名
    public const string HOTUPDATE_ASSEMBLY_NAME = "game";
    // 热更程序集入口GameObject类
    public const string HOTUPDATE_ENTRY_CLASS = "Entry";
    // 默认YooAsset包名
    public const string YOOASSET_DEFAULT_PACKAGE_NAME = "DefaultPackage";
    // 脚本包名
    // public const string YOOASSET_SCRIPT_PACKAGE_NAME = "ScriptPackage";
}