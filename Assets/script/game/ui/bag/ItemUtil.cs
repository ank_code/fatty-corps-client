using System;
using System.Collections.Generic;
using script.game.net.msg.server.item;

namespace script.game.ui.bag
{
    public class ItemUtil
    {
        private const string ICON_PATH_PREFIX = "Assets/res/img/item/icon/";
        private const string ICON_SUFFIX = ".png";
        
        private const string FRAME_PATH_PREFIX = "Assets/res/img/item/quality/";

        public static string getIconPath(int typeId)
        {
            return ICON_PATH_PREFIX + typeId + ICON_SUFFIX;
        }

        public static string getQualityFramePath(int quality)
        {
            return FRAME_PATH_PREFIX + quality + ICON_SUFFIX;
        }

        public static List<DItem> strToAwardList(string str)
        {
            List<DItem> itemList = new List<DItem>();
            if (str == null || str.Length == 0)
            {
                return itemList;
            }
            
            string[] strList = str.Split(",");
            
            DItem dItem = null;
            for (var i = 0; i < strList.Length; i++)
            {
                if (i % 2 == 0)
                {
                    dItem = new DItem();
                    dItem.typeId = Int32.Parse(strList[i]);
                }
                else
                {
                    dItem.num = Int32.Parse(strList[i]);
                    itemList.Add(dItem);
                }
            }

            return itemList;
        }
    }
}