using System;
using script.game.enums;

namespace script.game.obj.config
{
    public class CfgHero : BaseCfg
    {
        public string anim_path;
        public int height;
        public string name;
        // 阵营
        public ECamp camp = ECamp.An;
        // 职业
        public ECareer career = ECareer.Assassin;

        public int init_att = 0;
        public int init_def = 0;
        public int init_max_hp = 0;
        public int init_speed = 0; // 速度
        public int init_skill_damage_er = 0; // 技能伤害
        public int init_precise_per = 0; // 精准
        public int init_parry_per = 0; // 格挡
        public int init_critical_per = 0; // 暴击
        public int init_critical_damage_per = 0; // 暴击伤害
        public int init_armor_break_per = 0; // 破甲
        public int init_immunity_control_per = 0; // 免控率
        public int init_reduce_damage_per = 0; // 减伤率
        public int init_sacred_damage_per = 0; // 神圣伤害
        public int init_reduce_critical_per = 0; // 暴击抵抗
        public int init_inc_damage_per = 0; // 增伤率
        public int init_inc_control_per = 0; // 增控率

        public int lv_up_att = 0; // 每级增加攻击
        public int lv_up_def = 0; // 每级增加防御
        public int lv_up_max_hp = 0; // 每级增加血量

        public int init_star = 0;   // 初始星级
        public int max_star = 0;    // 最大星级

        public string skill_group_ids; // 技能组列表
        public int lottery_weight = 0; // 抽卡权重
    }
}