using System;

namespace kf
{
    public class KtByteConverter
    {
        public static string convertBytesToMB(long bytes)
        {
            const long oneMB = 1024 * 1024; // 1 MB in bytes
            double mb = bytes / oneMB;
            return mb.ToString("F2"); // 保留两位小数
        }
    }
}