using System;
using ak;
using kf;
using kf.anim;
using script.game.mgr;
using script.game.obj.config;
using script.game.ui.battle;
using UnityEngine;

namespace script.game.ui.hero.detail
{
    public class HeroAnim : MonoBehaviour
    {
        public enum EState
        {
            Idle,
            Attack,
        }
        
        private CfgHero m_cfg;
        private SpineAnim m_sa = null;
        private EState m_state;
        private bool m_playing = false;
        
        private const float ANIM_SIZE_SCALE = 8.0f;
        
        public void init(CfgHero cfg)
        {
            m_cfg = cfg;

            if (m_sa != null)
            {
                KtUnity.destory(m_sa.getObj());
            }
            
            new SpineAnim(false, m_cfg.anim_path, gameObject, SpineAnim.EType.UI, (sa) =>
            {
                m_sa = sa;
                m_sa.setScale(ANIM_SIZE_SCALE);
                setState(EState.Idle);
            });
        }

        public void setState(EState state)
        {
            m_state = state;

            switch (m_state)
            {
                case EState.Idle:
                    m_sa.playLoop(BattleHero.ANIM_NAME_IDLE);
                    break;
                case EState.Attack:
                    if (m_playing)
                    {
                        return;
                    }
                    
                    m_playing = true;
                    m_sa.playOnce(BattleHero.ANIM_NAME_ATT, @event =>
                    {
                        if (@event.e.Equals(SpineAnim.ECallBackEvent.Complete))
                        {
                            m_playing = false;
                            setState(EState.Idle);
                        }
                    });
                    break;
            }
        }

        public EState getState()
        {
            return m_state;
        }
    }
}