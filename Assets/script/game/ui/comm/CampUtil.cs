using script.game.enums;

namespace script.game.ui.comm
{
    public class CampUtil
    {
        private const string ICON_PREFIX_PATH = "Assets/res/img/ui/comm/camp/";
        private const string ICON_SUFFIX_PATH = ".png";

        
        public static string getCampIconPath(ECamp camp)
        {
            return ICON_PREFIX_PATH + (int)camp + ICON_SUFFIX_PATH;
        }
        
        public static string getCampName(ECamp camp)
        {
            switch (camp)
            {
                case ECamp.Sheng:
                    return "圣";
                case ECamp.Mu:
                    return "木";
                case ECamp.Huo:
                    return "火";
                case ECamp.Shui:
                    return "水";
                case ECamp.Guang:
                    return "光";
                case ECamp.An:
                    return "暗";
            }

            return "未知";
        }
        
    }
}